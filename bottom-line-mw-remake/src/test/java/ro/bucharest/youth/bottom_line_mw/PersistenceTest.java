package ro.bucharest.youth.bottom_line_mw;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.Person;
import ro.bucharest.youth.bottom_line_mw.entities.RoomType;
import ro.bucharest.youth.bottom_line_mw.entities.Utility;
import ro.bucharest.youth.bottom_line_mw.entities.UtilityDebtCalculus;
import ro.bucharest.youth.bottom_line_mw.entities.UtilityExpense;
import ro.bucharest.youth.bottom_line_mw.entities.UtilityExpensePayment;

@Ignore
public class PersistenceTest extends TestBase {

	private static Address address;

	@BeforeClass
	public static void init() {
		setupRooms();
		setupPersons();
		setupAddress();
		setupUtilities();
		setUtilitiesForAddress();
		addUtilitiesExpenses();
		addUtilitiesDebtPaymentFormulas();
	}

	private static void addUtilitiesDebtPaymentFormulas() {

		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		session.refresh(address);

		Person debtor = (Person) session.createQuery("from Person").list().get(0);
		Utility utility = address.getUtilities().get(0);
		UtilityDebtCalculus calculus = new UtilityDebtCalculus("{x/2}", null, address, debtor, utility);
		session.persist(calculus);
		session.getTransaction().commit();
		session.close();
	}

	private static void setupPersons() {
		Person fslev = new Person("Florin Slev");
		Person dvisan = new Person("Visan Drg");
		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		session.persist(fslev);
		session.persist(dvisan);
		session.getTransaction().commit();
		session.close();
	}

	private static void setupRooms() {
		RoomType bathroom = new RoomType("Bathroom");
		RoomType kitchen = new RoomType("Kitchen");
		RoomType livingRoom = new RoomType("Living room");
		RoomType dinningRoom = new RoomType("Dining room");
		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		session.persist(bathroom);
		session.persist(kitchen);
		session.persist(dinningRoom);
		session.persist(livingRoom);
		session.getTransaction().commit();
		session.close();
	}

	private static void setupUtilities() {
		Session session = sessionFactory.openSession();
		Utility rent = new Utility("Rent");
		Utility rdsRcs = new Utility("RDS-RCS");
		Utility energy = new Utility("Enel Muntenia");
		Utility overHeadCosts = new Utility("Intretinere");
		session.getTransaction().begin();
		session.persist(rent);
		session.persist(rdsRcs);
		session.persist(energy);
		session.persist(overHeadCosts);
		session.getTransaction().commit();
		session.close();
	}

	private static void setUtilitiesForAddress() {
		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		Utility RdsRcsUtility = (Utility) session.createQuery("from Utility where name='RDS-RCS'").uniqueResult();
		Utility EnelUtility = (Utility) session.createQuery("from Utility where name='Enel Muntenia'").uniqueResult();
		address.addUtility(EnelUtility);
		address.addUtility(RdsRcsUtility);
		session.merge(address);
		session.getTransaction().commit();
		session.close();
	}

	private static void setupAddress() {
		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		Address addr = new Address();
		addr.setCountry("Portugal");
		addr.setCity("Lisboa");
		addr.setStreet("Trestiana nr 9, bloc 11");

		// return available rooms
		RoomType kitchen = (RoomType) session.createQuery("select r from RoomType r where name='Kitchen'")
				.uniqueResult();
		RoomType bathroom = (RoomType) session.createQuery("select r from RoomType r where name='Bathroom'")
				.uniqueResult();
		// add rooms to the address
		addr.addRoom(kitchen);
		addr.addRoom(bathroom);

		// get and add all available persons to the address
		List<Person> persons = (List<Person>) session.createQuery("from Person").list();
		for (Person person : persons) {
			// addr.addPerson(person);
		}

		address = addr;
		session.persist(addr);
		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void testWaterMetering() throws InterruptedException {

		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		// get first available address
		Address addr = (Address) session.createQuery("from Address").list().get(0);
		// Get first room from address
		RoomType room = addr.getRooms().get(0);

		// add water metering
		session.getTransaction().commit();
		session.close();
	}

	public static void addUtilitiesExpenses() {

		Session session = sessionFactory.openSession();
		session.getTransaction().begin();
		// get first available address
		Address addr = (Address) session.createQuery("from Address").list().get(0);
		Utility energyUtil = (Utility) session.createQuery("select u from Utility u where name='Enel Muntenia'")
				.uniqueResult();
		Utility rdsRcsUtil = (Utility) session.createQuery("select u from Utility u where name='RDS-RCS'")
				.uniqueResult();
	}
}
