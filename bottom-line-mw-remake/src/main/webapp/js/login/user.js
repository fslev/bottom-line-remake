var app = angular.module('usersApp', [ 'ngRoute' ]);

app.controller('userCtrl', function($scope, $http) {

	$scope.incomplete = true;
	$scope.pwdsMatch = true;
	$scope.name = '';
	$scope.username = '';
	$scope.password = '';
	$scope.re_password = '';
	$scope.email = '';

	$scope.$watch('name', function() {
		$scope.complete();
	});

	$scope.$watch('username', function() {
		$scope.complete();
	});

	$scope.$watch('password', function() {
		$scope.complete();
	});

	$scope.$watch('re_password', function() {
		$scope.complete();
	});

	$scope.$watch('email', function() {
		$scope.complete();
	});

	$scope.complete = function() {
		$scope.incomplete = false;
		$scope.pwdsMatch = false;

		if ($scope.password == $scope.re_password) {
			$scope.pwdsMatch = true;
		}

		if (!$scope.name.length || !$scope.username.length
				|| !$scope.email.length || !$scope.password.length
				|| !$scope.pwdsMatch) {
			$scope.incomplete = true;
		}
	};

	$scope.create = function() {

		var createUserData = {
			name : $scope.username,
			username : $scope.username,
			password : $scope.password,
			email : $scope.email

		};
		console.debug("Create user: " + createUserData.username);

		$http({
			method : 'POST',
			url : apiTenants,
			data : createUserData
		}).success(function(response) {
			console.debug("Create user ok!");
			showAlertSuccess("User created and available for login");
		}).error(function(response, status) {
			console.error("Something bad happened. Status: " + status);
			showAlertWarning(response);
		});
	};

})
