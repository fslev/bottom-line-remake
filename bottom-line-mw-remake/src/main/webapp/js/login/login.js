var app = angular.module('loginApp', [ 'ngRoute' ]);

app.controller('loginCtrl', function($scope, $http) {

	$scope.incomplete = true;
	$scope.username = '';
	$scope.password = '';
	$scope.loading = false;

	$scope.$watch('username', function() {
		$scope.complete();
	});

	$scope.$watch('password', function() {
		$scope.complete();
	});

	$scope.complete = function() {
		$scope.incomplete = false;
		if (!$scope.username.length || !$scope.password.length) {
			$scope.incomplete = true;
		}
	};

	$scope.login = function() {

		$scope.loading = true;

		var loginData = {
			username : $scope.username,
			password : $scope.password
		};
		console.debug("Login user: " + loginData.username);

		$http({
			method : 'POST',
			url : apiLogin,
			data : loginData
		}).success(function(response) {
			console.debug("Login ok!");
			setCookie("auth", JSON.stringify(response));
			refreshAddressesForAuthenticatedUser(response['userId']);
			redirectToAdmin();
		}).error(function(response, status) {
			console.error("Something bad happened. Status: " + status);
			showAlertWarning("Cannot login. ");
			$scope.loading = false;
		});
	};

})
