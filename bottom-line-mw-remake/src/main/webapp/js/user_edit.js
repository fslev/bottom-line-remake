function editUser(scope) {

	scope.currUsername = getCurrentUserName();
	scope.incomplete = true;
	scope.password = "*****";

	scope.loadingProf = true;
	getUser(getCurrentUserId()).then(function(resp) {
		scope.loadingProf = false;
		if (resp.status == 200) {
			var user = JSON.parse(resp.responseText);
			scope.name = user.name;
			scope.username = user.username;
			scope.email = user.email;
			scope.pwd = user.password;
		}
		scope.$apply();
	});

	scope.$watch('name', function() {
		scope.complete();
	});

	scope.$watch('username', function() {
		scope.complete();
	});

	scope.$watch('email', function() {
		scope.complete();
	});

	scope.$watch('password', function() {
		scope.complete();
	});

	scope.$watch('pwd_unlock', function() {
		scope.complete();
	});

	scope.complete = function() {
		scope.incomplete = false;
		if (!scope.name || !scope.username || !scope.email || !scope.password
				|| !scope.pwd_unlock) {
			scope.incomplete = true;
		}
	};

	scope.update = function() {
		var user;
		if (scope.password != "*****") {
			user = {
				name : scope.name,
				username : scope.username,
				email : scope.email,
				password : scope.password
			};
		} else {
			user = {
				name : scope.name,
				username : scope.username,
				email : scope.email
			};
		}

		scope.updating = true;
		updateUser(getCurrentUserId(), JSON.stringify(user), scope.pwd_unlock)
				.then(
						function(resp) {
							scope.updating = false;
							if (resp.status == 200) {
								showAlertSuccess("Profile updated with success");
							} else {
								showAlertDanger("Cannot update user. Hint: Profile already exists or make sure password unlock is correct");
							}
							scope.$apply();
						});
	}
}