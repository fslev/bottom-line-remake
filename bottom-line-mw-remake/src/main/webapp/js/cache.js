function refreshAddressesForAuthenticatedUser(userId) {
	addresses = getAddressesForUser(userId);
	console.debug("Set local storage addresses");
	localStorage.setItem('addresses' + userId, addresses);
}

function getCachedAddresses(userId) {

	var addresses = localStorage.getItem("addresses" + userId);
	if (addresses == undefined || addresses == null) {
		return "[]";
	}
	return addresses;
}

function selectAddressId(userId, addressId) {
	localStorage.setItem('selectedAddrId' + userId, addressId);
}

function getSelectedAddress(userId) {
	var id = localStorage.getItem('selectedAddrId' + userId);
	var addresses = JSON.parse(getCachedAddresses(userId));

	if (id === undefined || id == null)
		return addresses[0];

	for (i in addresses) {
		if (addresses[i].id == id) {
			return addresses[i];
		}
	}

	return addresses[0];
}