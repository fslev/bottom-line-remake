var app = angular.module('bottomlineApp', [ 'ngRoute', 'highcharts-ng',
		'ngWebSocket' ]);

app.config(function($routeProvider) {
	$routeProvider.when('/water_consumption', {
		templateUrl : 'pages/water_consumption.html',
		controller : 'waterConsumptionCtrl'
	}).when('/heat_consumption', {
		templateUrl : 'pages/heat_consumption.html',
		controller : 'heatConsumptionCtrl'
	}).when('/energy_consumption', {
		templateUrl : 'pages/energy_consumption.html',
		controller : 'energyConsumptionCtrl'
	}).when('/utility_expense', {
		templateUrl : 'pages/utility_expense.html',
		controller : 'utilityExpenseCtrl'
	}).when('/chat', {
		templateUrl : 'pages/chat.html',
		controller : 'chatCtrl'
	}).otherwise({
		templateUrl : 'pages/welcome.html',
	});
});

app
		.controller(
				'navbarCtrl',
				function($rootScope, $scope, $websocket) {
					$scope.username = getCurrentUserName();
					$scope.logout = function() {
						logoutCurrentUser();
						redirectToLogin();
					}
					// Websocket
					$rootScope.ws = $websocket("ws://"
							+ "house-angrynerds.rhcloud.com:8000" + "/chat");
					$rootScope.ws.onOpen(function() {
						console.log('connection open');
						var clientRegistration = {
							type : "CLIENT_REGISTRATION",
							id : getCurrentUserId()
						};
						console
								.debug("WebSocket request: ",
										clientRegistration);
						$rootScope.ws.send(JSON.stringify(clientRegistration));
						$rootScope.connected = true;
					});

					$rootScope.newMsg = {};
					$rootScope.selectedUserId = '';
					$rootScope.currUserId = getCurrentUserId();
					$rootScope.maxMsgLines = 2000;
					var history = localStorage.getItem('msgs'
							+ $rootScope.currUserId);
					$rootScope.chatArea = (history == undefined || history == null) ? {}
							: JSON.parse(history);
					$rootScope.ws
							.onMessage(function(event) {
								try {
									var msg = JSON.parse(event.data);
								} catch (e) {
									console.error("Cannot parse message");
									return;
								}
								console.debug("Got message: ", msg);
								if (msg.type == "ONLINE_USERS") {
									$rootScope.onlineCount = msg.count;
								} else if (msg.type == "ONLINE_TENANTS") {
									$rootScope.onlineTenants = msg.tenants;
								} else if (msg.type == "MESSAGE") {
									if ($rootScope.chatArea[msg.srcUserId] == undefined) {
										$rootScope.chatArea[msg.srcUserId] = [];
									}
									// allow max lines
									if ($rootScope.chatArea[msg.srcUserId].length > $rootScope.maxMsgLines) {
										$rootScope.chatArea[msg.srcUserId]
												.shift();
									}
									$rootScope.chatArea[msg.srcUserId][$rootScope.chatArea[msg.srcUserId].length] = msg;
									if ($rootScope.selectedUserId != msg.srcUserId) {
										$rootScope.newMsg[msg.srcUserId] = true;
									}
									localStorage.setItem('msgs'
											+ $rootScope.currUserId, JSON
											.stringify($rootScope.chatArea));
									$("#app-chat").scrollTop(
											$("#app-chat")[0].scrollHeight);
								}
							});
				});

app.controller('appCtrl', function($scope, $route) {
	control_address_dropdown($scope, $route);
});

app.controller('waterConsumptionCtrl', function($scope) {
	waterConsumption($scope);
});

app.controller('heatConsumptionCtrl', function($scope) {
	heatConsumption($scope);
});

app.controller('energyConsumptionCtrl', function($scope) {
	energyConsumption($scope);
});

app.controller('utilityExpenseCtrl', function($scope, $q) {
	utilityExpense($scope, $q);
});

app.controller('chatCtrl', function($rootScope, $scope, $websocket) {
	chat($rootScope, $scope, $websocket);
});
