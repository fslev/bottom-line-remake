function heatConsumption(scope) {

	// variables init
	scope.hdate = '';
	scope.hindex = '';
	scope.hconsumption = '';
	scope.selectedRoom;
	scope.hdesc = '';
	scope.hid = '';

	scope.edit = false;
	scope.incomplete = false;
	scope.confirmedHConsumptionId = '';

	scope.limit = 10;
	scope.offset = 0;
	scope.count = '';
	scope.pageIndex = 0;
	scope.loading = false;

	scope.hPattern = '';

	scope.Math = Math;

	scope.getHeatCons = function(addressId, offset, limit, pattern) {
		scope.loading = true;
		var resp = getHeat(addressId, offset, limit, pattern);
		scope.loading = false;
		if (resp.status === 200) {
			scope.count = parseInt(resp.getResponseHeader("count"));
			return JSON.parse(resp.responseText);
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
			return null;
		} else {
			showAlertDanger(resp.responseText);
			return null;
		}
	};

	if (scope.selectedAddr == null || scope.selectedAddr === undefined) {
		return;
	}

	scope.heatList = scope.getHeatCons(scope.selectedAddr.id, scope.offset,
			scope.limit, scope.hPattern);

	scope.heatListFull = scope.getHeatCons(scope.selectedAddr.id, 0, 1000,
			scope.hPattern);
	generateHeatHighcharts(scope, scope.heatListFull);
	generateHeatPie(scope, scope.heatListFull);

	// PAGINATION
	scope.refreshPages = function() {
		scope.pagesCount = new Array(Math.ceil(scope.count / scope.limit));
		console.debug("Pages no: " + scope.pagesCount.length);
		scope.selectedPage = 0;
	}

	scope.refreshPages();

	scope.selectPage = function(index) {
		scope.selectedPage = index;
		scope.heatList = scope.getHeatCons(scope.selectedAddr.id, index
				* scope.limit, scope.limit, scope.hPattern);
	}
	scope.lastPage = function() {
		scope.selectedPage = scope.pagesCount.length - 1;
		scope.heatList = scope.getHeatCons(scope.selectedAddr.id, scope.limit
				* scope.selectedPage, scope.limit, scope.hPattern);
	}
	scope.firstPage = function() {
		scope.selectedPage = 0;
		scope.heatList = scope.getHeatCons(scope.selectedAddr.id, scope.limit
				* scope.selectedPage, scope.limit, scope.hPattern);
	}

	scope.selectRoom = function(room) {
		scope.selectedRoom = room;
		scope.complete();
		console.debug("Selected room: " + scope.selectedRoom);
	};

	scope.editHConsumption = function(hCons) {
		$('#modalHConsumption').modal('show');
		if (hCons == 'new') {
			scope.edit = false;
			scope.incomplete = true;
			scope.hdate = '';
			scope.hindex = '';
			scope.hconsumption = '';
			scope.hdesc = '';
			scope.selectedType = '';
			scope.selectedRoom = '';
			scope.hid = '';
		} else {
			scope.edit = true;
			$(".datepicker").datepicker("update", new Date(hCons.date));
			scope.hindex = hCons.index;
			scope.hconsumption = hCons.consumption != null ? hCons.consumption
					: null;
			scope.hdesc = hCons.description;
			scope.selectedRoom = hCons.room;
			scope.hid = hCons.id;
		}
	};

	scope.searchHeat = function(pattern) {
		console.debug("Search by pattern: " + pattern);

		var resp = getHeat(scope.selectedAddr.id, scope.offset, scope.limit,
				pattern);
		var resp1 = getHeat(scope.selectedAddr.id, 0, 1000, pattern);

		if (resp.status === 200 && resp1.status === 200) {
			scope.count = parseInt(resp.getResponseHeader("count"));
			scope.heatList = JSON.parse(resp.responseText);
			scope.heatListFull = JSON.parse(resp1.responseText);
			scope.refreshPages();
			generateHeatHighcharts(scope, scope.heatListFull);
			generateHeatPie(scope, scope.heatListFull);
		} else if (resp.status === 500 || resp1.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
			return null;
		} else {
			if (resp.status != 200 && resp.status != 500) {
				showAlertWarning(resp.responseText);
			}
			if (resp1.status != 200 && resp1.status != 500) {
				showAlertWarning(resp1.responseText);
			}
			return null;
		}
	};

	scope.confirmHConsumption = function(id) {
		scope.confirmedHConsumptionId = id;
		scope.modal_title = "Remove Heat Consumption";
		scope.modal_body = "You are about to remove a heat consumption entity. Proceed ?";
		console.debug("Remove heat consumption with id: " + id
				+ " confirmation.");
		$('#modalId').modal('show');
	};

	scope.confirmHeatRemoval = function(id) {
		scope.heatIdConfirmed = id;
		scope.modal_title = "Remove heat";
		scope.modal_body = "You are about to remove a heat consumption entry. Proceed ? :/";
		console.debug("Remove heat with id: " + id + " confirmation");
		$('#modalId').modal('show');
	};

	// remove heat consumption
	scope.modal_function = function() {
		var xhr = removeHeat(scope.heatIdConfirmed);
		if (xhr.status === 200) {
			scope.heatList = scope.getHeatCons(scope.selectedAddr.id,
					scope.offset, scope.limit, scope.hPattern);
			scope.refreshPages();
			showAlertSuccess(xhr.responseText);
		} else if (xhr.status === 500) {
			showAlertDanger(xhr.responseText);
		} else {
			showAlertWarning(xhr.responseText);
		}
	};

	scope.configureHConsumption = function() {

		if (!scope.edit) {
			var heat = {
				index : scope.hindex,
				consumption : scope.hconsumption,
				description : scope.hdesc,
				date : Date.parse(scope.hdate)
			};
			var xhr = postHeat(JSON.stringify(heat), scope.selectedAddr.id,
					scope.selectedRoom.id);
			if (xhr.status === 200) {
				showAlertSuccess("Heat consumption successfuly added");
				// refresh heat list
				scope.heatList = scope.getHeatCons(scope.selectedAddr.id,
						scope.offset, scope.limit, scope.hPattern);
				scope.refreshPages();
			} else if (xhr.status === 500) {
				showAlertDanger(xhr.responseText);
			} else {
				showAlertWarning(xhr.responseText);
			}
		} else {
			var heat = {
				index : scope.hindex,
				consumption : scope.hconsumption,
				description : scope.hdesc,
				date : Date.parse(scope.hdate)
			};
			var xhr = updateHeat(scope.hid, JSON.stringify(heat),
					scope.selectedRoom.id);
			if (xhr.status === 200) {
				showAlertSuccess("Heat consumption successfuly updated");
				// refresh heat list
				scope.heatList = scope.getHeatCons(scope.selectedAddr.id,
						scope.offset, scope.limit, scope.hPattern);
				scope.refreshPages();
			} else if (xhr.status === 500) {
				showAlertDanger(xhr.responseText);
			} else {
				showAlertWarning(xhr.responseText);
			}
		}
	};

	scope.$watch('hdate', function(newValue) {
		scope.hdate = newValue;
		scope.complete();
	});

	scope.$watch('hindex', function() {
		scope.complete();
	});

	scope.$watch('hconsumption', function() {
		scope.complete();
	});

	scope.complete = function() {
		scope.incomplete = false;
		if (!scope.hindex || !scope.selectedRoom || !scope.hdate
				|| isNaN(scope.hindex) || isNaN(scope.hconsumption)) {
			scope.incomplete = true;
		}
	};
};

