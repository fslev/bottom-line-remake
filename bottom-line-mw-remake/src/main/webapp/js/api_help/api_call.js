//Address

function getAddressesForUser(userId) {
	console.debug("Get addresses for user with id " + userId);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('GET', apiAddress + "/user/" + userId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send(null);
	if (xhr.status === 200) {
		return xhr.responseText;
	} else if (xhr.status === 401) {
		redirectToLogin();
	}
}

function postAddress(address) {
	console.debug("POST address");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('POST', apiAddress, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(address);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function putAddress(addressId, address) {
	console.debug("Update address for id " + addressId);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', apiAddress + "/" + addressId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(address);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function unassignUserFromAddress(addressId, userId) {
	console.debug("Unassign user from address");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiAddress + "/" + addressId + "/person/" + userId,
			false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(null);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function deleteAddress(addressId) {
	console.debug("Remove address with id " + addressId);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiAddress + "/" + addressId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function grantOwnershipForAddress(addressId, tenantId) {
	console.debug("Grant ownership");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', apiAddress + "/" + addressId + "/owner/" + tenantId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function revokeOwnershipFromAddress(addressId, tenantId) {
	console.debug("Remove ownership");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiAddress + "/" + addressId + "/owner/" + tenantId,
			false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function inviteUserToAddress(srcUserId, addressId, destUserId) {
	console.debug("Send address invitation to user " + destUserId);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('POST', apiInvitations + "/address/" + addressId + "/srcuser/"
			+ srcUserId + "/destuser/" + destUserId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send(null);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function getAddressInvitations(userId) {
	console.debug("Get invitations for user with id " + userId);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('GET', apiInvitations + "/destuser/" + userId + "/addresses",
			false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send(null);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function acceptAddressInvitation(addrInvId) {
	console.debug("Accept addr invitation " + addrInvId);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', apiInvitations + "/accept/" + addrInvId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send(null);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

// Tenants

function getTenants(addressId) {
	console.debug("Get tenants for address " + addressId);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('GET', apiTenants + "/address/" + addressId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send(null);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function getUsers(name) {
	console.debug("Get users by name " + name);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('GET', apiTenants + "?name=" + name, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send(null);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

// Rooms

function postRoom(room, addressId) {
	console.debug("POST room");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('POST', apiRooms + "/address/" + addressId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(room);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function updateRoom(room, roomId) {
	console.debug("Update room");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', apiRooms + "/" + roomId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(room);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function removeRoom(roomId) {
	console.debug("Remove room");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiRooms + "/" + roomId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(null);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

// Utilities

function postUtility(utility, addressId) {
	console.debug("POST utility");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('POST', apiUtility + "/address/" + addressId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(utility);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function updateUtility(utility, utilityId) {
	console.debug("Update utility");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', apiUtility + "/" + utilityId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(utility);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function removeUtility(utilityId) {
	console.debug("Remove utility");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiUtility + "/" + utilityId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(null);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

// Water

function getWater(addressId, offset, limit) {
	return getWater(addressId, offset, limit, null);
}

function getWater(addressId, offset, limit, pattern) {
	console.debug("GET water cons");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	if (pattern != null || pattern != '') {
		xhr.open('GET', apiWater + "?addressId=" + addressId + "&offset="
				+ offset + "&limit=" + limit + "&pattern=" + pattern, false);
	} else {
		xhr.open('GET', apiWater + "?addressId=" + addressId + "&offset="
				+ offset + "&limit=" + limit, false);
	}
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function postWater(water, addressId, roomId) {
	console.debug("Add water cons");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('POST', apiWater + "/address/" + addressId + "/room/" + roomId,
			false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(water);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function updateWater(waterId, water, roomId) {
	console.debug("Update water consumption: " + water);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', apiWater + "/" + waterId + "/room/" + roomId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(water);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function removeWater(waterId) {
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiWater + "/" + waterId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

// Heat

function getHeat(addressId, offset, limit) {
	return getHeat(addressId, offset, limit, null);
}

function getHeat(addressId, offset, limit, pattern) {
	console.debug("GET heat cons");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	if (pattern != null || pattern != '') {
		xhr.open('GET', apiHeat + "?addressId=" + addressId + "&offset="
				+ offset + "&limit=" + limit + "&pattern=" + pattern, false);
	} else {
		xhr.open('GET', apiHeat + "?addressId=" + addressId + "&offset="
				+ offset + "&limit=" + limit, false);
	}
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function postHeat(heat, addressId, roomId) {
	console.debug("Add heat cons");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('POST', apiHeat + "/address/" + addressId + "/room/" + roomId,
			false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(heat);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function updateHeat(heatId, heat, roomId) {
	console.debug("Update heat consumption: " + heat);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', apiHeat + "/" + heatId + "/room/" + roomId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(heat);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function removeHeat(heatId) {
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiHeat + "/" + heatId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

// ENERGY
function getEnergy(addressId, offset, limit) {
	return getEnergy(addressId, offset, limit, null);
}

function getEnergy(addressId, offset, limit, pattern) {
	console.debug("GET energy cons");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	if (pattern != null || pattern != '') {
		xhr.open('GET', apiEnergy + "?addressId=" + addressId + "&offset="
				+ offset + "&limit=" + limit + "&pattern=" + pattern, false);
	} else {
		xhr.open('GET', apiEnergy + "?addressId=" + addressId + "&offset="
				+ offset + "&limit=" + limit, false);
	}
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function postEnergy(energy, addressId) {
	console.debug("Add energy cons");
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('POST', apiEnergy + "/address/" + addressId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(energy);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function updateEnergy(energyId, energy) {
	console.debug("Update energy consumption: " + energy);
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', apiEnergy + "/" + energyId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send(energy);
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

function removeEnergy(energyId) {
	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiEnergy + "/" + energyId, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.send();
	if (xhr.status === 401) {
		redirectToLogin();
	} else {
		return xhr;
	}
}

// UTILITY EXPENSES
function getUtilityExpenses(addressId, start, end, offset, limit) {
	return getUtilityExpenses(addressId, start, end, offset, limit, null);
}

function getUtilityExpenses(addressId, start, end, offset, limit, pattern) {
	return new Promise(function(resolve, reject) {
		console.debug("GET utility expenses");
		var token = JSON.parse(getCookie("auth"))['uuid'];
		var xhr = new XMLHttpRequest();
		if (pattern != null || pattern != '') {
			xhr.open('GET', apiUtilityExp + "?addressId=" + addressId
					+ "&startDate=" + start + "&endDate=" + end + "&offset="
					+ offset + "&limit=" + limit + "&pattern=" + pattern);
		} else {
			xhr.open('GET', apiUtilityExp + "?addressId=" + addressId
					+ "&startDate=" + start + "&endDate=" + end + "&offset="
					+ offset + "&limit=" + limit);
		}
		xhr.onload = function() {
			if (this.status === 401) {
				redirectToLogin();
			} else {
				resolve(this);
			}
		};
		xhr.setRequestHeader("Authorization", token);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send();
	});
}

function postUtilityExpense(expense, utilityId) {
	return new Promise(function(resolve, reject) {
		console.debug("Add utility expense cons");
		var token = JSON.parse(getCookie("auth"))['uuid'];
		var xhr = new XMLHttpRequest();
		xhr.open('POST', apiUtilityExp + "/utility/" + utilityId);
		xhr.onload = function() {
			if (this.status === 401) {
				redirectToLogin();
			} else {
				resolve(this);
			}
		};
		xhr.setRequestHeader("Authorization", token);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(expense);
	});
}

function updateUtilityExpense(expenseId, expense) {
	return new Promise(function(resolve, reject) {
		console.debug("Update utility expense: " + expense);
		var token = JSON.parse(getCookie("auth"))['uuid'];
		var xhr = new XMLHttpRequest();
		xhr.open('PUT', apiUtilityExp + "/" + expenseId);
		xhr.onload = function() {
			if (this.status === 401) {
				redirectToLogin();
			} else {
				resolve(this);
			}
		};
		xhr.setRequestHeader("Authorization", token);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(expense);
	});
}

function removeUtilityExpense(expenseId) {
	return new Promise(function(resolve, reject) {
		var token = JSON.parse(getCookie("auth"))['uuid'];
		var xhr = new XMLHttpRequest();
		xhr.open('DELETE', apiUtilityExp + "/" + expenseId);
		xhr.onload = function() {
			if (this.status === 401) {
				redirectToLogin();
			} else {
				resolve(this);
			}
		};
		xhr.setRequestHeader("Authorization", token);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send();
	});
}

// Email
function postMail(mail) {
	return new Promise(function(resolve, reject) {
		console.debug("Sending email");
		var token = JSON.parse(getCookie("auth"))['uuid'];
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			if (this.status === 401) {
				redirectToLogin();
			} else {
				resolve(this);
			}
		};
		xhr.open('POST', apiMail, true);
		xhr.setRequestHeader("Authorization", token);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(mail);
	});
}

// User
function updateUser(userId, user, pwd) {
	return new Promise(function(resolve, reject) {
		console.debug("Update user: ", user);
		var token = JSON.parse(getCookie("auth"))['uuid'];
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			if (this.status === 401) {
				redirectToLogin();
			} else {
				resolve(this);
			}
		};
		xhr.open('PUT', apiTenants + "/" + userId + "?pwd=" + pwd);
		xhr.setRequestHeader("Authorization", token);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(user);
	});
}

function getUser(userId) {
	return new Promise(function(resolve, reject) {
		var token = JSON.parse(getCookie("auth"))['uuid'];
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			if (this.status === 401) {
				redirectToLogin();
			} else {
				resolve(this);
			}
		};
		xhr.open('GET', apiTenants + "/" + userId);
		xhr.setRequestHeader("Authorization", token);
		xhr.send(null);
	});
}

// Auth

function removeToken() {
	console.debug("Remove token");
	if (getCookie("auth") == 'null' || getCookie("auth") === undefined) {
		return;
	}

	var token = JSON.parse(getCookie("auth"))['uuid'];
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', apiLogout, false);
	xhr.setRequestHeader("Authorization", token);
	xhr.send(null);
	if (xhr.status === 200) {
		console.debug(xhr.responseText);
	} else {
		console.debug("Cannot remove token");
	}
}
