function chat(rootScope, scope) {

	if (scope.selectedAddr == undefined
			|| scope.selectedAddr.assignedPersons == undefined) {
		// No one to talk to...
		return;
	}

	scope.getTenantIds = function() {
		var ids = [];
		var tenants = scope.selectedAddr.assignedPersons;
		for (var i = 0; i < tenants.length; i++) {
			ids.push(tenants[i].id);
		}
		return ids;
	}

	scope.tenantIds = scope.getTenantIds();

	scope.getTenantName = function(id) {
		var tenants = scope.selectedAddr.assignedPersons;
		for (var i = 0; i < tenants.length; i++) {
			if (tenants[i].id == id) {
				return tenants[i].name;
			}
		}
		return null;
	}

	var tenantsRegistration = {
		type : "TENANTS_REGISTRATION",
		tenants : scope.getTenantIds(),
		userId : rootScope.currUserId
	};

	rootScope.$watch('connected', function() {
		console.debug("Websocket request: "
				+ JSON.stringify(tenantsRegistration));
		if (rootScope.connected) {
			rootScope.ws.send(JSON.stringify(tenantsRegistration));
		}
	});

	scope.isOnline = function(id) {
		for (i in rootScope.onlineTenants) {
			if (rootScope.onlineTenants[i] == id) {
				return true;
			}
		}
		return false;
	}

	scope.setDefaultSelectedUserId = function(id) {
		if (rootScope.selectedUserId == '') {
			rootScope.selectedUserId = id;
		}
	}

	scope.selectUser = function(id) {
		console.debug("Select user: ", id);
		if (id == rootScope.currUserId) {
			return;
		}
		rootScope.selectedUserId = id;
		rootScope.newMsg[rootScope.selectedUserId] = false;
		$("#app-chat").scrollTop($("#app-chat")[0].scrollHeight);
	}

	scope.clean = function() {
		console.debug("Clean messages");
		rootScope.chatArea[rootScope.selectedUserId] = [];
		localStorage.setItem('msgs'+rootScope.currUserId, JSON.stringify(rootScope.chatArea));
	}

	scope.sendMessage = function() {
		var msg = {
			type : "MESSAGE",
			srcUserId : rootScope.currUserId,
			destUserId : rootScope.selectedUserId,
			body : scope.inputMsg
		};
		console.debug(msg);
		if (rootScope.chatArea[rootScope.selectedUserId] == undefined) {
			rootScope.chatArea[rootScope.selectedUserId] = [];
		}
		rootScope.chatArea[rootScope.selectedUserId][rootScope.chatArea[rootScope.selectedUserId].length] = msg;
		rootScope.ws.send(JSON.stringify(msg));
		scope.inputMsg = '';
		// scroll down
		$("#app-chat").scrollTop($("#app-chat")[0].scrollHeight);
		// allow max lines
		if (rootScope.chatArea[rootScope.selectedUserId].length > rootScope.maxMsgLines) {
			rootScope.chatArea[rootScope.selectedUserId].shift();
		}
		localStorage.setItem('msgs'+rootScope.currUserId, JSON.stringify(rootScope.chatArea));
	}
}