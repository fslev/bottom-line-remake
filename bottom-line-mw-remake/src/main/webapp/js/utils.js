function refreshAddress(address, freshAddresses) {
	for (i in freshAddresses) {
		if (address['id'] == freshAddresses[i]['id']) {
			return freshAddresses[i];
		}
	}
	return address;
}

function getTenantsEmails(tenants) {
	var emails = [];
	for (var i = 0; i < tenants.length; i++) {
		emails.push(tenants[i].email);
	}
	return emails;
}

function removeFromArray(array, value) {
	for (var i = 0; i < array.length; i++) {
		if (array[i] == value) {
			array.splice(i, 1);
		}
	}
}

function addDataPoint(data, serieName, datapoint) {
	for (var i = 0; i < data.length; i++) {
		if (data[i].name == serieName) {
			reduceHcData(data[i].data, datapoint);
			return;
		}
	}
	newData = {};
	newData.name = serieName;
	newData.data = [];
	newData.data.push(datapoint);
	newData.visible = false;
	data.push(newData);
}

function getExpPie(organizedData) {
	pie = [];
	for (var i = 0; i < Object.keys(organizedData).length; i++) {
		var key = Object.keys(organizedData)[i];
		var pair = {};
		pair['name'] = key;
		pair['y'] = organizedData[key];
		pie.push(pair);
	}
	return pie;
}

function getWaterTypePie(organizedData) {
	pie = [];
	for (var i = 0; i < Object.keys(organizedData).length; i++) {
		var key = Object.keys(organizedData)[i];
		if (key == 'COLD' || key == 'HOT') {
			var pair = {};
			pair['name'] = key;
			pair['y'] = organizedData[key];
			pie.push(pair);
		}
	}
	return pie;
}

function getWaterRoomPie(organizedData) {
	pie = [];
	for (var i = 0; i < Object.keys(organizedData).length; i++) {
		var key = Object.keys(organizedData)[i];
		if (key != 'COLD' && key != 'HOT') {
			var pair = {};
			pair['name'] = key;
			pair['y'] = organizedData[key];
			pie.push(pair);
		}
	}
	return pie;
}

function getHeatRoomPie(organizedData) {
	pie = [];
	for (var i = 0; i < Object.keys(organizedData).length; i++) {
		var key = Object.keys(organizedData)[i];
		var pair = {};
		pair['name'] = key;
		pair['y'] = organizedData[key];
		pie.push(pair);
	}
	return pie;
}

function getTotalHeatCons(data) {
	res = {};
	for (var i = 0; i < data.length; i++) {
		if (res[data[i].room.name] === undefined) {
			res[data[i].room.name] = 0;
		}
		res[data[i].room.name] = res[data[i].room.name] + data[i].consumption;
	}
	return res;
}

function getTotalWaterCons(data) {
	res = {};
	for (var i = 0; i < data.length; i++) {
		if (res[data[i].type] === undefined) {
			res[data[i].type] = 0;
		}
		if (res[data[i].room.name] === undefined) {
			res[data[i].room.name] = 0;
		}
		res[data[i].type] = res[data[i].type] + data[i].consumption;
		res[data[i].room.name] = res[data[i].room.name] + data[i].consumption;
	}
	return res;
}

function getTotalExpenses(data) {
	res = {};
	for (var i = 0; i < data.length; i++) {
		if (res[data[i].utility.name] === undefined) {
			res[data[i].utility.name] = 0;
		}
		res[data[i].utility.name] = res[data[i].utility.name] + data[i].value;
	}
	return res;
}

function getMaxExpenses(data) {
	res = {};
	for (var i = 0; i < data.length; i++) {
		if (res[data[i].utility.name] === undefined) {
			res[data[i].utility.name] = 0;
		}
		if (res[data[i].utility.name] < data[i].value) {
			res[data[i].utility.name] = data[i].value;
		}
	}
	return res;
}

function makeVisibleOneSerie(hcData) {
	if (hcData.length > 0) {
		hcData[0].visible = true;
	}
}

function reduceHcData(list, data) {
	for (var i = 0; i < list.length; i++) {
		// if found element with same date,
		if (list[i][0] == data[0]) {
			// then add to its value
			list[i][1] = list[i][1] + data[1];
			return;
		}
	}
	// if not found, then push data
	list.push(data);
}

function sortHcData(hcData) {
	for (var i = 0; i < hcData.length; i++) {
		// sort by name
		hcData.sort(function(a, b) {
			return strcmp(a.name, b.name);
		});

		// sort data
		var data = hcData[i].data;
		data.sort(function(a, b) {
			return a[0] - b[0];
		});
	}
}

function strcmp(a, b) {
	a = a.toString(), b = b.toString();
	return (a < b ? -1 : (a > b ? 1 : 0));
}