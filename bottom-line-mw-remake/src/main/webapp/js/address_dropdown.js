function control_address_dropdown(scope, route) {

	// variables init
	var currUserId = getCurrentUserId();
	scope.loadAddr = false;

	scope.addresses = JSON.parse(getCachedAddresses(currUserId));
	if (scope.addresses == null || scope.addresses === undefined) {
		console.debug("No addresses. Exit");
		return;
	}

	scope.selectedAddr = getSelectedAddress(currUserId);

	scope.selectAddr = function(address) {
		selectAddressId(currUserId, address.id);
		scope.selectedAddr = address;
		route.reload();
	}
}