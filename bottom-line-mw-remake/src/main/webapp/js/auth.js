function isUserOwnerForAddress(userId, address) {
	var owners = address['owners'];
	for ( var i in owners) {
		if (owners[i]['id'] === userId) {
			return true;
		}
	}
	return false;
};

function logoutCurrentUser() {
	console.debug("Logout current user.")
	removeToken();
	setCookie("auth", null);
}

function getCurrentUserName() {
	return JSON.parse(getCookie("auth"))['username'];
}

function getCurrentUserId() {
	return JSON.parse(getCookie("auth"))['userId'];
}

function redirectToIndex() {
	window.location.href = '/index.html';
}

function redirectToAdmin() {
	window.location.href = '/admin.html';
}

function redirectToLogin() {
	window.location.href = '/login.html#/login';
}

function redirectAuthenticatedUser() {
	if (isUserAuthenticated()) {
		redirectToAdmin();
	}
}

function redirectNotAuthenticatedUser() {
	if (!isUserAuthenticated()) {
		redirectToLogin();
	}
}

function isUserAuthenticated() {
	console.debug("Check user is authenticated")
	try {
		var cookieAuth = getCookie("auth");
		if (cookieAuth === undefined)
			return false;
		var token = JSON.parse(getCookie("auth"))['uuid'];
		var request = new XMLHttpRequest();
		request.open('GET', apiToken, false);
		request.setRequestHeader("Authorization", token);
		request.send(null);
		if (request.status === 200) {
			return true;
		}
	} catch (e) {
		return false;
	}
	return false;
}