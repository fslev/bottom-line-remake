function configUtilities(scope) {

	// variables init
	var currUserId = getCurrentUserId();
	scope.loadAddr = true;
	scope.loadAddr = false;

	scope.util_name = '';
	scope.util_id = '';
	scope.edit = false;
	scope.incomplete = true;

	scope.addresses = JSON.parse(getCachedAddresses(currUserId));
	if (!scope.addresses.length) {
		console.debug("No addresses. Exit");
		return;
	}

	scope.selectedAddr = getSelectedAddress(currUserId);
	scope.utilities = scope.selectedAddr['utilities'];

	scope.$watch('util_name', function() {
		scope.complete();
	});

	scope.complete = function() {
		scope.incomplete = false;
		if (!scope.util_name.length) {
			scope.incomplete = true;
		}
	}

	scope.selectAddr = function(address) {
		selectAddressId(currUserId, address.id);
		scope.selectedAddr = address;
		scope.utilities = scope.selectedAddr['utilities'];
	}

	scope.isOwner = function(address) {
		return isUserOwnerForAddress(currUserId, address);
	}

	scope.editUtility = function(utility) {
		if (utility == 'new') {
			scope.edit = false;
			scope.incomplete = true;
			scope.util_name = '';
			scope.util_id = '';
		} else {
			scope.edit = true;
			scope.util_name = utility.name;
			scope.util_id = utility.id;
		}
	};

	scope.configureUtility = function() {
		var utility = {
			name : scope.util_name
		};
		// Add utility
		if (!scope.edit) {
			var resp = postUtility(JSON.stringify(utility),
					scope.selectedAddr['id']);
			if (resp.status === 200) {
				showAlertSuccess(resp.responseText);
				refreshAddressesForAuthenticatedUser(currUserId);
				scope.addresses = JSON.parse(getCachedAddresses(currUserId));
				scope.selectedAddr = refreshAddress(scope.selectedAddr,
						scope.addresses);
				scope.utilities = scope.selectedAddr['utilities'];
			} else if (resp.status === 500) {
				showAlertDanger("INTERNAL SERVER ERROR");
			} else {
				showAlertWarning(resp.responseText);
			}
		}
		// Update utility
		else {
			var resp = updateUtility(JSON.stringify(utility), scope.util_id);
			if (resp.status === 200) {
				showAlertSuccess(resp.responseText);
				refreshAddressesForAuthenticatedUser(currUserId);
				scope.addresses = JSON.parse(getCachedAddresses(currUserId));
				scope.selectedAddr = refreshAddress(scope.selectedAddr,
						scope.addresses);
				scope.utilities = scope.selectedAddr['utilities'];
			} else if (resp.status === 500) {
				showAlertDanger("INTERNAL SERVER ERROR");
			} else {
				showAlertWarning(resp.responseText);
			}
		}
	}

	scope.confirmUtil = function(id) {

		scope.utilId = id;
		scope.modal_title = "Remove utility";
		scope.modal_body = "You are about to remove a utility. Proceed ?";
		console.debug("Remove utility " + id + " confirmation");
		$('#modalId').modal('show');
	}

	scope.modal_function = function() {
		var resp = removeUtility(scope.utilId);
		if (resp.status === 200) {
			showAlertSuccess(resp.responseText);
			refreshAddressesForAuthenticatedUser(currUserId);
			scope.addresses = JSON.parse(getCachedAddresses(currUserId));
			scope.selectedAddr = refreshAddress(scope.selectedAddr,
					scope.addresses);
			scope.utilities = scope.selectedAddr['utilities'];
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
		} else {
			showAlertWarning(resp.responseText);
		}
	}
}
