function configRooms(scope) {

	// variables init
	var currUserId = getCurrentUserId();
	scope.loadAddr = true;
	scope.loadAddr = false;

	scope.room_type = '';
	scope.room_id = '';
	scope.edit = false;
	scope.incomplete = true;

	scope.addresses = JSON.parse(getCachedAddresses(currUserId));
	if (!scope.addresses.length) {
		console.debug("No addresses. Exit");
		return;
	}

	scope.selectedAddr = getSelectedAddress(currUserId);
	scope.rooms = scope.selectedAddr['rooms'];

	scope.$watch('room_type', function() {
		scope.complete();
	});

	scope.complete = function() {
		scope.incomplete = false;
		if (!scope.room_type.length) {
			scope.incomplete = true;
		}
	}

	scope.selectAddr = function(address) {
		selectAddressId(currUserId, address.id);
		scope.selectedAddr = address;
		scope.rooms = scope.selectedAddr['rooms'];
	}

	scope.isOwner = function(address) {
		return isUserOwnerForAddress(currUserId, address);
	}

	scope.editRoom = function(room) {
		if (room == 'new') {
			scope.edit = false;
			scope.incomplete = true;
			scope.room_type = '';
			scope.room_id = '';
		} else {
			scope.edit = true;
			scope.room_type = room.name;
			scope.room_id = room.id;
		}
	};

	scope.configureRoom = function() {
		var room = {
			name : scope.room_type
		};
		// Add room
		if (!scope.edit) {
			var resp = postRoom(JSON.stringify(room), scope.selectedAddr['id']);
			if (resp.status === 200) {
				showAlertSuccess("Room successfully added for selected address");
				refreshAddressesForAuthenticatedUser(currUserId);
				scope.addresses = JSON.parse(getCachedAddresses(currUserId));
				scope.selectedAddr = refreshAddress(scope.selectedAddr,
						scope.addresses);
				scope.rooms = scope.selectedAddr['rooms'];
			} else if (resp.status === 500) {
				showAlertDanger("INTERNAL SERVER ERROR");
			} else {
				showAlertWarning(resp.responseText);
			}
		}
		// Update room
		else {
			var resp = updateRoom(JSON.stringify(room), scope.room_id);
			if (resp.status === 200) {
				showAlertSuccess(resp.responseText);
				refreshAddressesForAuthenticatedUser(currUserId);
				scope.addresses = JSON.parse(getCachedAddresses(currUserId));
				scope.selectedAddr = refreshAddress(scope.selectedAddr,
						scope.addresses);
				scope.rooms = scope.selectedAddr['rooms'];
			} else if (resp.status === 500) {
				showAlertDanger("INTERNAL SERVER ERROR");
			} else {
				showAlertWarning(resp.responseText);
			}
		}
	}

	scope.confirmRoomType = function(id) {

		scope.roomId = id;
		scope.modal_title = "Remove room";
		scope.modal_body = "You are about to remove a room. Proceed ?";
		console.debug("Remove room " + id + " confirmation");
		$('#modalId').modal('show');
	}

	scope.modal_function = function() {
		var resp = removeRoom(scope.roomId);
		if (resp.status === 200) {
			showAlertSuccess("Room successfully removed from selected address");
			refreshAddressesForAuthenticatedUser(currUserId);
			scope.addresses = JSON.parse(getCachedAddresses(currUserId));
			scope.selectedAddr = refreshAddress(scope.selectedAddr,
					scope.addresses);
			scope.rooms = scope.selectedAddr['rooms'];
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
		} else {
			showAlertWarning(resp.responseText);
		}
	}
}

function refreshAddress(address, freshAddresses) {
	for (i in freshAddresses) {
		if (address['id'] == freshAddresses[i]['id']) {
			return freshAddresses[i];
		}
	}
	return address;
}
