function configMainAdmin(scope) {

	var currId = getCurrentUserId();

	scope.loading = true;
	scope.messages = getMesages(currId);
	scope.loading = false;

	scope.acceptAddrInvitation = function(addressInvitation) {
		var resp = acceptAddressInvitation(addressInvitation.id);
		if (resp.status === 200) {
			removeAddrInvitationFromMessages(scope.messages,
					addressInvitation.id);
			refreshAddressesForAuthenticatedUser(currId);
		}
	}
}

function getMesages(currId) {
	var messages = [];

	// Get address invitations
	var invitations = getAddressInvitations(currId);
	if (invitations.status != 200) {
		showAlertDanger("Cannot get messages. Something bad happened.");
	}
	var data = JSON.parse(invitations.responseText);
	for ( var i in data) {
		var message = data[i];
		if (message['accepted'] == false && message['active'] == true) {
			messages.push({
				type : "addrInvitation",
				content : data[i]
			});
		}
	}

	return messages;
}

function removeAddrInvitationFromMessages(messages, invitationId) {
	for ( var i in messages) {
		if (messages[i].type == "addrInvitation"
				&& messages[i].content.id == invitationId) {
			messages.splice(i, 1);
		}
	}
}