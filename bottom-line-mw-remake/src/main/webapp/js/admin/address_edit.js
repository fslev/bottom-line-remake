function editAddresses(scope) {

	var currUserId = getCurrentUserId();

	// variables init
	scope.country = '';
	scope.city = '';
	scope.street = '';
	scope.id = '';
	scope.edit = false;
	scope.incomplete = false;
	scope.confirmedAddressId = '';

	scope.addresses = JSON.parse(getCachedAddresses(currUserId));

	scope.editAddress = function(address) {
		if (address == 'new') {
			scope.edit = false;
			scope.incomplete = true;
			scope.country = '';
			scope.city = '';
			scope.street = '';
			scope.id = '';
		} else {
			scope.edit = true;
			scope.country = address.country;
			scope.city = address.city;
			scope.street = address.street;
			scope.id = address.id;
		}
	};

	scope.confirmAddress = function(id) {
		scope.addressIdConfirmed = id;
		scope.modal_title = "Remove address";
		scope.modal_body = "You are about to remove an address. Proceed ? :/";
		console.debug("Remove address with id: " + id + " confirmation");
		$('#modalId').modal('show');
	};

	scope.isOwner = function(address) {
		return isUserOwnerForAddress(currUserId, address);
	};

	// remove address
	scope.modal_function = function() {
		var xhr = deleteAddress(scope.addressIdConfirmed);
		if (xhr.status === 200) {
			refreshAddressesForAuthenticatedUser(currUserId);
			// Refresh addresses in FE
			scope.addresses = JSON.parse(getCachedAddresses(currUserId));
			showAlertSuccess(xhr.responseText);
		} else {
			showAlertDanger("INTERNAL SERVER ERROR");
		}
	};

	scope.configureAddress = function() {
		var address = {
			country : scope.country,
			city : scope.city,
			street : scope.street
		};
		if (!scope.edit) {
			// Add address
			var xhr = postAddress(JSON.stringify(address));
			if (xhr.status === 200) {
				refreshAddressesForAuthenticatedUser(currUserId);
				// Refresh addresses in FE
				scope.addresses = JSON.parse(getCachedAddresses(currUserId));
				showAlertSuccess(xhr.responseText);
			} else {
				showAlertDanger(xhr.responseText);
			}
		} else {
			// Update address
			var xhr = putAddress(scope.id, JSON.stringify(address));
			if (xhr.status === 200) {
				refreshAddressesForAuthenticatedUser(currUserId);
				// Refresh addresses in FE
				scope.addresses = JSON.parse(getCachedAddresses(currUserId));
				showAlertSuccess(xhr.responseText);
			} else if (xhr.status === 500) {
				showAlertDanger("INTERNAL SERVER ERROR");
			} else {
				showAlertDanger(xhr.responseText);
			}
		}
	};

	scope.unsubscribeAddress = function(id) {
		var resp = unassignUserFromAddress(id, currUserId);
		if (resp.status === 200) {
			showAlertSuccess(resp.responseText);
			refreshAddressesForAuthenticatedUser(currUserId);
			// Refresh addresses in FE
			scope.addresses = JSON.parse(getCachedAddresses(currUserId));
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
		} else {
			showAlertDanger(resp.responseText);
		}
	}

	scope.$watch('country', function() {
		scope.complete();
	});

	scope.$watch('city', function() {
		scope.complete();
	});
	scope.$watch('street', function() {
		scope.complete();
	});

	scope.complete = function() {
		scope.incomplete = false;
		if (!scope.country.length || !scope.city.length || !scope.street.length) {
			scope.incomplete = true;
		}
	};
};
