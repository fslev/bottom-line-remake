function configTenants(scope) {

	// variables init
	var currUserId = getCurrentUserId();
	scope.loadAddr = true;
	scope.loadAddr = false;
	scope.addresses = '';

	scope.addresses = JSON.parse(getCachedAddresses(currUserId));
	if (!scope.addresses.length) {
		console.debug("No addresses. Exit");
		return;
	}

	scope.selectedAddr = getSelectedAddress(currUserId);
	scope.tenants = scope.selectedAddr['assignedPersons'];

	scope.selectAddr = function(address) {
		selectAddressId(currUserId, address.id);
		scope.selectedAddr = address;
		scope.tenants = scope.selectedAddr['assignedPersons'];
	}

	scope.isOwner = function(address) {
		return isUserOwnerForAddress(currUserId, address);
	};

	scope.confirmTenant = function(id) {

		scope.tenantId = id;
		scope.modal_title = "Remove tenant";
		scope.modal_body = "You are about to remove a tenant. Proceed ?";
		console.debug("Remove tenant " + id + " confirmation");
		$('#modalId').modal('show');
	}

	scope.modal_function = function() {

		var resp = unassignUserFromAddress(scope.selectedAddr['id'],
				scope.tenantId);
		if (resp.status === 200) {
			showAlertSuccess("User successfully removed from selected address");
			refreshAddressesForAuthenticatedUser(currUserId);
			scope.addresses = JSON.parse(getCachedAddresses(currUserId));
			removeUser(scope.selectedAddr, scope.tenantId);
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
		} else {
			showAlertWarning(resp.responseText);
		}
	}

	scope.searchedUsers = '';

	scope.search = function(name) {
		var resp = getUsers(name);
		if (resp.status === 200) {
			scope.searchedUsers = JSON.parse(resp.responseText);
		} else if (resp.status === 500) {
			showAlertDanger("BAD SEARCH");
		} else {
			showAlertDanger(resp.responseText);
		}
	}

	scope.inviteToAddress = function(user) {
		var resp = inviteUserToAddress(currUserId, scope.selectedAddr['id'],
				user['id']);
		if (resp.status === 200) {
			showAlertSuccess("User " + user['username'] + " (" + user.name
					+ ") successfully invited to selected address");
			refreshAddressesForAuthenticatedUser(currUserId);
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
		} else {
			showAlertWarning(resp.responseText);
		}
	}

	scope.makeOwner = function(tenant, address) {
		var resp = grantOwnershipForAddress(address.id, tenant.id);
		if (resp.status === 200) {
			showAlertSuccess(resp.responseText);
			refreshAddressesForAuthenticatedUser(currUserId);
			scope.addresses = JSON.parse(getCachedAddresses(currUserId));
			scope.selectedAddr = refreshAddress(scope.selectedAddr,
					scope.addresses);
			scope.tenants = scope.selectedAddr['assignedPersons'];
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
		} else {
			showAlertWarning(resp.responseText);
		}
	}

	scope.unmakeOwner = function(tenant, address) {
		var resp = revokeOwnershipFromAddress(address.id, tenant.id);
		if (resp.status === 200) {
			showAlertSuccess(resp.responseText);
			refreshAddressesForAuthenticatedUser(currUserId);
			scope.addresses = JSON.parse(getCachedAddresses(currUserId));
			scope.selectedAddr = refreshAddress(scope.selectedAddr,
					scope.addresses);
			scope.tenants = scope.selectedAddr['assignedPersons'];
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
		} else {
			showAlertWarning(resp.responseText);
		}
	}

	scope.isTenantOwner = function(tenant, address) {
		console.debug("Check tenant is owner");
		var owners = address.owners;
		for (i in owners) {
			if (owners[i].id == tenant.id) {
				console.debug(true);
				return true;
			}
		}
		return false;
	}

}

function removeUser(address, userId) {
	var users = address['assignedPersons'];
	for (i in users) {
		if (users[i]['id'] == userId) {
			users.splice(i, 1);
		}
	}
}
