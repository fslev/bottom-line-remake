// HIGHCHARTS

function generateUtilityExpHighcharts(scope, utilityExpList) {

	if (!utilityExpList)
		return;

	hcData = [];

	for (var i = 0; i < utilityExpList.length; i++) {
		utilityExp = utilityExpList[i];
		addDataPoint(hcData, " Total Value", [ utilityExp.date,
				utilityExp.value ]);
		addDataPoint(hcData, "Utility: " + utilityExp.utility.name, [
				utilityExp.date, utilityExp.value ]);
	}

	sortHcData(hcData);
	makeVisibleOneSerie(hcData);

	scope.chartTypes = [ {
		"id" : "area",
		"title" : "Area"
	} ];

	scope.chartSeries = hcData;

	scope.chartStack = [ {
		"id" : '',
		"title" : "No"
	}, {
		"id" : "normal",
		"title" : "Normal"
	}, {
		"id" : "percent",
		"title" : "Percent"
	} ];

	Highcharts.setOptions({
		global : {
			useUTC : false
		}
	});

	scope.chartConfig = {
		options : {
			chart : {
				type : 'areaspline',
				height : 250,
				borderWidth : 1,
				borderRadius : 3,
				borderColor : '#EDEFF0',
				zoomType : 'xy'
			},
			plotOptions : {
				series : {
					stacking : ''
				}
			}
		},
		series : scope.chartSeries,
		title : {
			text : 'Utilities Expense Evolution (area-spline chart)'
		},
		xAxis : {
			title : {
				text : 'Date'
			},
			type : "datetime",
			dateTimeLabelFormats : {
				month : '%b %e, %Y'
			}
		},
		yAxis : {
			title : {
				text : 'Cost'
			}
		},
		credits : {
			enabled : true
		},
		loading : false,
		size : {}
	};

	scope.reflow = function() {
		scope.$broadcast('highchartsng.reflow');
	};
}

// HIGHCHARTS

function generateExpensesPie(scope, utilityExpList) {

	if (!utilityExpList)
		return;

	hcData = getExpPie(getTotalExpenses(utilityExpList));
	hcData1 = getExpPie(getMaxExpenses(utilityExpList));

	// Make monochrome colors and set them as default for all pies
	Highcharts.getOptions().plotOptions.pie.colors = (function() {
		var colors = [], base = '#00FFF2', i;

		for (i = 0; i < 10; i += 1) {
			// Start out with a darkened base color (negative brighten), and end
			// up with a much brighter color
			colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
		}
		return colors;
	}());

	scope.chartConfig1 = {
		options : {
			chart : {
				type : 'pie',
				height : 230,
				width : 465,
				borderWidth : 1,
				borderRadius : 3,
				borderColor : '#EDEFF0'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : true,
						format : '<b>{point.name}</b>: {point.percentage:.1f} %',
						style : {
							color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
						},
						connectorColor : 'silver'
					}
				}
			}
		},
		series : [ {
			name : 'Total',
			data : hcData
		} ],
		title : {
			text : 'Total utilities expense (overall %)'
		},
		credits : {
			enabled : true
		},
		loading : false,
		size : {}
	};

	scope.chartConfig2 = {
		options : {
			chart : {
				type : 'pie',
				height : 230,
				width : 465,
				borderWidth : 1,
				borderRadius : 3,
				borderColor : '#EDEFF0'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : true,
						format : '<b>{point.name}</b>: {point.percentage:.1f} %',
						style : {
							color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
						},
						connectorColor : 'silver'
					}
				}
			}
		},
		series : [ {
			name : 'Max',
			data : hcData1
		} ],
		title : {
			text : 'Maximum utilities expense (overall %)'
		},
		credits : {
			enabled : true
		},
		loading : false,
		size : {}
	};

	scope.reflow = function() {
		scope.$broadcast('highchartsng.reflow');
	};
}