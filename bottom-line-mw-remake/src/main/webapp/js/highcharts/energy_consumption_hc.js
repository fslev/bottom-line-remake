// HIGHCHARTS

function generateEnergyHighcharts(scope, list) {

	if (!list)
		return;

	hcData = [];

	for (var i = 0; i < list.length; i++) {
		energy = list[i];
		addDataPoint(hcData, " Total Consumption", [ energy.date,
				energy.consumption ]);
	}

	sortHcData(hcData);
	makeVisibleOneSerie(hcData);
	// console.debug(JSON.stringify(hcData));

	scope.chartTypes = [ {
		"id" : "area",
		"title" : "Area"
	} ];

	scope.chartSeries = hcData;

	scope.chartStack = [ {
		"id" : '',
		"title" : "No"
	}, {
		"id" : "normal",
		"title" : "Normal"
	}, {
		"id" : "percent",
		"title" : "Percent"
	} ];

	Highcharts.setOptions({
		global : {
			useUTC : false
		}
	});

	scope.chartConfig = {
		options : {
			chart : {
				type : 'areaspline',
				height : 250,
				borderWidth : 1,
				borderRadius : 3,
				borderColor : '#EDEFF0',
				zoomType : 'xy'
			},
			plotOptions : {
				series : {
					stacking : ''
				}
			}
		},
		series : scope.chartSeries,
		title : {
			text : 'Energy Consumption Evolution (area-spline chart)'
		},
		xAxis : {
			title : {
				text : 'Date'
			},
			type : "datetime",
			dateTimeLabelFormats : {
				month : '%b %e, %Y'
			}
		},
		yAxis : {
			title : {
				text : 'Consumption'
			}
		},
		credits : {
			enabled : true
		},
		loading : false,
		size : {}
	};

	scope.reflow = function() {
		scope.$broadcast('highchartsng.reflow');
	};
}