// HIGHCHARTS

function generateWaterHighcharts(scope, waterList) {

	if (!waterList)
		return;

	hcData = [];

	for (var i = 0; i < waterList.length; i++) {
		water = waterList[i];
		addDataPoint(hcData, " Total Consumption", [ water.date,
				water.consumption ]);
		addDataPoint(hcData, "Type: " + water.type, [ water.date,
				water.consumption ]);
		addDataPoint(hcData, "Room: " + water.room.name, [ water.date,
				water.consumption ]);
		addDataPoint(hcData, "Type: " + water.type + " & Room: "
				+ water.room.name, [ water.date, water.consumption ]);
	}

	sortHcData(hcData);
	makeVisibleOneSerie(hcData);
	// console.debug(JSON.stringify(hcData));

	scope.chartTypes = [ {
		"id" : "area",
		"title" : "Area"
	} ];

	scope.chartSeries = hcData;

	scope.chartStack = [ {
		"id" : '',
		"title" : "No"
	}, {
		"id" : "normal",
		"title" : "Normal"
	}, {
		"id" : "percent",
		"title" : "Percent"
	} ];

	Highcharts.setOptions({
		global : {
			useUTC : false
		}
	});

	scope.chartConfig = {
		options : {
			chart : {
				type : 'areaspline',
				height : 250,
				borderWidth : 1,
				borderRadius : 3,
				borderColor : '#EDEFF0',
				zoomType : 'xy'
			},
			plotOptions : {
				series : {
					stacking : ''
				}
			}
		},
		series : scope.chartSeries,
		title : {
			text : 'Water Consumption Evolution (area-spline chart)'
		},
		xAxis : {
			title : {
				text : 'Date'
			},
			type : "datetime",
			dateTimeLabelFormats : {
				month : '%b %e, %Y'
			}
		},
		yAxis : {
			title : {
				text : 'Consumption'
			}
		},
		credits : {
			enabled : true
		},
		loading : false,
		size : {}
	};

	scope.reflow = function() {
		scope.$broadcast('highchartsng.reflow');
	};
}

// HIGHCHARTS

function generateWaterPies(scope, waterList) {

	if (!waterList)
		return;

	var totalWaterCons = getTotalWaterCons(waterList);
	hcDataType = getWaterTypePie(totalWaterCons);
	hcDataRoom = getWaterRoomPie(totalWaterCons);

	// Make monochrome colors and set them as default for all pies
	Highcharts.getOptions().plotOptions.pie.colors = (function() {
		var colors = [], base = '#70D3FA', i;

		for (i = 0; i < 10; i += 1) {
			// Start out with a darkened base color (negative brighten), and end
			// up with a much brighter color
			colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
		}
		return colors;
	}());

	scope.chartConfig2 = {
		options : {
			chart : {
				type : 'pie',
				height : 200,
				width : 400,
				borderWidth : 1,
				borderRadius : 3,
				borderColor : '#EDEFF0'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : true,
						format : '<b>{point.name}</b>: {point.percentage:.1f} %',
						style : {
							color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
						},
						connectorColor : 'silver'
					}
				}
			}
		},
		series : [ {
			name : 'Consumption',
			data : hcDataType
		} ],
		title : {
			text : 'Hot & cold water cons. (overall)'
		},
		credits : {
			enabled : true
		},
		loading : false,
		size : {}
	};

	scope.chartConfig3 = {
		options : {
			chart : {
				type : 'pie',
				height : 200,
				width : 500,
				borderWidth : 1,
				borderRadius : 3,
				borderColor : '#EDEFF0'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : true,
						format : '<b>{point.name}</b>: {point.percentage:.1f} %',
						style : {
							color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
						},
						connectorColor : 'silver'
					}
				}
			}
		},
		series : [ {
			name : 'Consumption',
			data : hcDataRoom
		} ],
		title : {
			text : 'Room water cons. (overall)'
		},
		credits : {
			enabled : true
		},
		loading : false,
		size : {}
	};

	scope.reflow = function() {
		scope.$broadcast('highchartsng.reflow');
	};
}