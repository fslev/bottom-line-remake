function waterConsumption(scope) {

	// variables init
	scope.wdate = '';
	scope.windex = '';
	scope.wconsumption = '';
	scope.selectedType;
	scope.selectedRoom;
	scope.wdesc = '';
	scope.wid = '';

	scope.edit = false;
	scope.incomplete = false;
	scope.confirmedWConsumptionId = '';

	scope.limit = 10;
	scope.offset = 0;
	scope.count = '';
	scope.pageIndex = 0;
	scope.loading = false;

	scope.wPattern = '';

	scope.Math = Math;

	scope.getWaterCons = function(addressId, offset, limit, pattern) {
		scope.loading = true;
		var resp = getWater(addressId, offset, limit, pattern);
		scope.loading = false;
		if (resp.status === 200) {
			scope.count = parseInt(resp.getResponseHeader("count"));
			return JSON.parse(resp.responseText);
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
			return null;
		} else {
			showAlertDanger(resp.responseText);
			return null;
		}
	};

	if (scope.selectedAddr == null || scope.selectedAddr === undefined) {
		return;
	}

	scope.waterList = scope.getWaterCons(scope.selectedAddr.id, scope.offset,
			scope.limit, scope.wPattern);

	scope.waterListFull = scope.getWaterCons(scope.selectedAddr.id, 0, 1000,
			scope.wPattern);
	generateWaterHighcharts(scope, scope.waterListFull);
	generateWaterPies(scope, scope.waterListFull);

	// PAGINATION
	scope.refreshPages = function() {
		scope.pagesCount = new Array(Math.ceil(scope.count / scope.limit));
		console.debug("Pages no: " + scope.pagesCount.length);
		scope.selectedPage = 0;
	}

	scope.refreshPages();

	scope.selectPage = function(index) {
		scope.selectedPage = index;
		scope.waterList = scope.getWaterCons(scope.selectedAddr.id, index
				* scope.limit, scope.limit, scope.wPattern);
	}
	scope.lastPage = function() {
		scope.selectedPage = scope.pagesCount.length - 1;
		scope.waterList = scope.getWaterCons(scope.selectedAddr.id, scope.limit
				* scope.selectedPage, scope.limit, scope.wPattern);
	}
	scope.firstPage = function() {
		scope.selectedPage = 0;
		scope.waterList = scope.getWaterCons(scope.selectedAddr.id, scope.limit
				* scope.selectedPage, scope.limit, scope.wPattern);
	}

	scope.selectType = function(type) {
		scope.selectedType = type;
		scope.complete();
		console.debug("Selected type: " + scope.selectedType);
	};

	scope.selectRoom = function(room) {
		scope.selectedRoom = room;
		scope.complete();
		console.debug("Selected room: " + scope.selectedRoom);
	};

	scope.editWConsumption = function(wCons) {
		$('#modalWConsumption').modal('show');
		if (wCons == 'new') {
			scope.edit = false;
			scope.incomplete = true;
			scope.wdate = '';
			scope.windex = '';
			scope.wconsumption = '';
			scope.wdesc = '';
			scope.selectedType = '';
			scope.selectedRoom = '';
			scope.wid = '';
		} else {
			scope.edit = true;
			$(".datepicker").datepicker("update", new Date(wCons.date));
			scope.windex = wCons.index;
			scope.wconsumption = wCons.consumption != null ? wCons.consumption
					: null;
			scope.wdesc = wCons.description;
			scope.selectedType = wCons.type;
			scope.selectedRoom = wCons.room;
			scope.wid = wCons.id;
		}
	};

	scope.searchWater = function(pattern) {
		console.debug("Search by pattern: " + pattern);

		var resp = getWater(scope.selectedAddr.id, scope.offset, scope.limit,
				pattern);
		var resp1 = getWater(scope.selectedAddr.id, 0, 1000, pattern);

		if (resp.status === 200 && resp1.status === 200) {
			scope.count = parseInt(resp.getResponseHeader("count"));
			scope.waterList = JSON.parse(resp.responseText);
			scope.waterListFull = JSON.parse(resp1.responseText);
			scope.refreshPages();
			generateWaterHighcharts(scope, scope.waterListFull);
			generateWaterPies(scope, scope.waterListFull);
		} else if (resp.status === 500 || resp1.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
			return null;
		} else {
			if (resp.status != 200 && resp.status != 500) {
				showAlertWarning(resp.responseText);
			}
			if (resp1.status != 200 && resp1.status != 500) {
				showAlertWarning(resp1.responseText);
			}
			return null;
		}
	};

	scope.confirmWConsumption = function(id) {
		scope.confirmedWConsumptionId = id;
		scope.modal_title = "Remove Water Consumption";
		scope.modal_body = "You are about to remove a water consumption entity. Proceed ?";
		console.debug("Remove water consumption with id: " + id
				+ " confirmation.");
		$('#modalId').modal('show');
	};

	scope.confirmWaterRemoval = function(id) {
		scope.waterIdConfirmed = id;
		scope.modal_title = "Remove water";
		scope.modal_body = "You are about to remove a water consumption entry. Proceed ? :/";
		console.debug("Remove water with id: " + id + " confirmation");
		$('#modalId').modal('show');
	};

	// remove water consumption
	scope.modal_function = function() {
		var xhr = removeWater(scope.waterIdConfirmed);
		if (xhr.status === 200) {
			scope.waterList = scope.getWaterCons(scope.selectedAddr.id,
					scope.offset, scope.limit, scope.wPattern);
			scope.refreshPages();
			showAlertSuccess(xhr.responseText);
		} else if (xhr.status === 500) {
			showAlertDanger(xhr.responseText);
		} else {
			showAlertWarning(xhr.responseText);
		}
	};

	scope.configureWConsumption = function() {

		if (!scope.edit) {
			var water = {
				index : scope.windex,
				consumption : scope.wconsumption,
				description : scope.wdesc,
				date : Date.parse(scope.wdate),
				type : scope.selectedType
			};
			var xhr = postWater(JSON.stringify(water), scope.selectedAddr.id,
					scope.selectedRoom.id);
			if (xhr.status === 200) {
				showAlertSuccess("Water consumption successfuly added");
				// refresh water list
				scope.waterList = scope.getWaterCons(scope.selectedAddr.id,
						scope.offset, scope.limit, scope.wPattern);
				scope.refreshPages();
			} else if (xhr.status === 500) {
				showAlertDanger(xhr.responseText);
			} else {
				showAlertWarning(xhr.responseText);
			}
		} else {
			var water = {
				index : scope.windex,
				consumption : scope.wconsumption,
				description : scope.wdesc,
				date : Date.parse(scope.wdate),
				type : scope.selectedType
			};
			var xhr = updateWater(scope.wid, JSON.stringify(water),
					scope.selectedRoom.id);
			if (xhr.status === 200) {
				showAlertSuccess("Water consumption successfuly updated");
				// refresh water list
				scope.waterList = scope.getWaterCons(scope.selectedAddr.id,
						scope.offset, scope.limit, scope.wPattern);
				scope.refreshPages();
			} else if (xhr.status === 500) {
				showAlertDanger(xhr.responseText);
			} else {
				showAlertWarning(xhr.responseText);
			}
		}
	};

	scope.$watch('wdate', function(newValue) {
		scope.wdate = newValue;
		scope.complete();
	});

	scope.$watch('windex', function() {
		scope.complete();
	});

	scope.$watch('wconsumption', function() {
		scope.complete();
	});

	scope.complete = function() {
		scope.incomplete = false;
		if (!scope.windex || !scope.selectedType || !scope.selectedRoom
				|| !scope.wdate || isNaN(scope.windex)
				|| isNaN(scope.wconsumption)) {
			scope.incomplete = true;
		}
	};
};

