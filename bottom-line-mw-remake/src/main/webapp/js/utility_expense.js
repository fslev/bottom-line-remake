var startDate = new Date().getTime() - 31536000000;// one year ago...
var startDateH = new Date(startDate).getDate() + "/"
		+ (new Date(startDate).getMonth() + 1) + "/"
		+ +new Date(startDate).getFullYear();
var endDate = new Date().getTime();
var endDateH = new Date(endDate).getDate() + "/"
		+ (new Date(endDate).getMonth() + 1) + "/"
		+ +new Date(endDate).getFullYear();

function initPopovers() {

	document.getElementById("startDateBtn").innerHTML = startDateH;
	document.getElementById("endDateBtn").innerHTML = endDateH;

	$(function() {
		$('[data-toggle="popover"]').popover();
	});
	$('.datepicker').datepicker();
	$('.datepicker').datepicker({
		format : 'mm/dd/yyyy',
		startDate : '0d',
		autoClose : true
	});
	$('#startDateBtn').datepicker().on(
			"changeDate",
			function(e) {
				startDateH = e.date.getDate() + "/" + (e.date.getMonth() + 1)
						+ "/" + +e.date.getFullYear();
				startDate = e.date.getTime();
				document.getElementById("startDateBtn").innerHTML = startDateH;
			});
	$('#endDateBtn').datepicker().on(
			"changeDate",
			function(e) {
				endDateH = e.date.getDate() + "/" + (e.date.getMonth() + 1)
						+ "/" + +e.date.getFullYear();
				endDate = e.date.getTime();
				document.getElementById("endDateBtn").innerHTML = endDateH;
			});
}

function utilityExpense(scope) {

	// variables init
	scope.ueDate = '';
	scope.ueValue = '';
	scope.selectedUtility;
	scope.ueDesc = '';
	scope.ueId = '';
	scope.test = '';

	scope.edit = false;
	scope.incomplete = false;
	scope.isMailInProgress = false;
	scope.confirmedUeConsumptionId = '';

	scope.limit = 10;
	scope.offset = 0;
	scope.count = '';
	scope.pageIndex = 0;
	scope.loading = false;

	scope.uePattern = '';

	scope.Math = Math;

	scope.getUtilityExp = function(addressId, startDate, endDate, offset,
			limit, pattern) {
		return new Promise(function(resolve, reject) {
			getUtilityExpenses(addressId, startDate, endDate, offset, limit,
					pattern).then(function(resp) {
				if (resp.status === 200) {
					scope.count = parseInt(resp.getResponseHeader("count"));
					scope.$apply();
					resolve(JSON.parse(resp.responseText));
				} else if (resp.status === 500) {
					showAlertDanger("INTERNAL SERVER ERROR");
					resolve(null);
				} else {
					showAlertDanger(resp.responseText);
					resolve(null);
				}
				scope.$apply();
			});
		});
	};

	if (scope.selectedAddr == null || scope.selectedAddr === undefined) {
		return;
	}

	scope.loading = true;
	scope.getUtilityExp(scope.selectedAddr.id, startDate, endDate,
			scope.offset, scope.limit, scope.uePattern).then(function(promise) {
		scope.loading = false;
		scope.utilityExpList = promise;
		scope.$apply();
		scope.refreshPages();
		scope.$apply();
	});

	scope.loadingHC = true;
	scope.getUtilityExp(scope.selectedAddr.id, startDate, endDate, 0,
			1000000000, scope.uePattern).then(function(promise) {
		scope.loadingHC = false;
		scope.utilityExpListFull = promise;
		scope.$apply();
		generateUtilityExpHighcharts(scope, scope.utilityExpListFull);
		generateExpensesPie(scope, scope.utilityExpListFull);
		scope.$apply();
	});

	// PAGINATION
	scope.refreshPages = function() {
		scope.pagesCount = new Array(Math.ceil(scope.count / scope.limit));
		console.debug("Pages no: " + scope.pagesCount.length);
		scope.selectedPage = 0;
	}

	scope.selectPage = function(index) {
		scope.selectedPage = index;
		scope.loading = true;
		scope.getUtilityExp(scope.selectedAddr.id, startDate, endDate,
				index * scope.limit, scope.limit, scope.uePattern).then(
				function(promise) {
					scope.loading = false;
					scope.utilityExpList = promise;
					scope.$apply();
				});
	}

	scope.lastPage = function() {
		scope.selectedPage = scope.pagesCount.length - 1;
		scope.loading = true;
		scope.getUtilityExp(scope.selectedAddr.id, startDate, endDate,
				scope.limit * scope.selectedPage, scope.limit, scope.uePattern)
				.then(function(promise) {
					scope.loading = false;
					scope.utilityExpList = promise;
					scope.$apply();
				});
	}

	scope.firstPage = function() {
		scope.selectedPage = 0;
		scope.loading = true;
		scope.getUtilityExp(scope.selectedAddr.id, startDate, endDate,
				scope.limit * scope.selectedPage, scope.limit, scope.uePattern)
				.then(function(promise) {
					scope.loading = false;
					scope.utilityExpList = promise;
					scope.$apply();
				});
	}

	scope.selectUtility = function(utility) {
		scope.selectedUtility = utility;
		scope.complete();
		console.debug("Selected utility");
	};

	scope.editUtilityExpense = function(utilityExp) {
		$('#modalUtilityExpense').modal('show');
		if (utilityExp == 'new') {
			scope.edit = false;
			scope.incomplete = true;
			scope.ueDate = '';
			scope.ueValue = '';
			scope.ueDesc = '';
			scope.selectedUtility = '';
			scope.ueId = '';
		} else {
			scope.edit = true;
			$(".datepicker").datepicker("update", new Date(utilityExp.date));
			scope.ueValue = parseFloat(utilityExp.value);
			scope.ueDesc = utilityExp.description;
			scope.selectedUtility = utilityExp.utility;
			scope.ueId = utilityExp.id;
		}
	};

	scope.searchUtilityExp = function(pattern) {
		console.debug("Search by pattern: " + pattern);

		scope.loading = true;
		getUtilityExpenses(scope.selectedAddr.id, startDate, endDate,
				scope.offset, scope.limit, pattern).then(function(promise) {
			scope.loading = false;
			if (promise.status === 200) {
				scope.utilityExpList = JSON.parse(promise.responseText);
				scope.count = parseInt(promise.getResponseHeader("count"));
				scope.$apply();
				scope.refreshPages();
				scope.$apply();
			} else if (promise.status === 500) {
				showAlertDanger("INTERNAL SERVER ERROR");
			} else {
				if (promise.status != 200 && promise.status != 500) {
					showAlertWarning(promise.responseText);
					scope.$apply();
				}
			}
		});

		scope.loadingHC = true;
		getUtilityExpenses(scope.selectedAddr.id, startDate, endDate, 0,
				1000000, pattern).then(function(promise) {
			scope.loadingHC = false;
			if (promise.status === 200) {
				scope.utilityExpListFull = JSON.parse(promise.responseText);
				generateUtilityExpHighcharts(scope, scope.utilityExpListFull);
				generateExpensesPie(scope, scope.utilityExpListFull);
				scope.$apply();
			} else if (promise.status === 500) {
				showAlertDanger("INTERNAL SERVER ERROR");
			} else {
				if (promise.status != 200 && promise.status != 500) {
					showAlertWarning(promise.responseText);
				}
			}
		});
	}

	scope.confirmUtilityExpRemoval = function(id) {
		scope.ueIdConfirmed = id;
		scope.modal_title = "Remove expense";
		scope.modal_body = "You are about to remove an expense entry. Proceed ? :/";
		console.debug("Remove expense with id: " + id + " confirmation");
		$('#modalId').modal('show');
	};

	// remove utility expense
	scope.modal_function = function() {
		removeUtilityExpense(scope.ueIdConfirmed)
				.then(
						function(promise) {
							if (promise.status === 200) {
								// Notify tenants about your modification
								var mail = {
									from : "angry.nerds.house@gmail.com",
									recipients : getTenantsEmails(scope.selectedAddr.assignedPersons),
									subject : getCurrentUserName()
											+ " removed a utility expense",
									msg : "Utility expense removed ("
											+ scope.ueIdConfirmed
											+ "):"
											+ "\n\nBy user: "
											+ getCurrentUserName()
											+ "\n\nFor more details, access your account: http://house-angrynerds.rhcloud.com"
								};
								console.debug("Sending mail to tenants: ", JSON
										.stringify(mail));
								scope.isMailInProgress = true;
								postMail(JSON.stringify(mail))
										.then(
												function(resp) {
													scope.isMailInProgress = false;
													scope.$apply();
													if (resp.status != 200) {
														showAlertDanger("Cannot send email to tenants");
													}
													console
															.debug(
																	"eMail sent with success",
																	scope.isMailInProgress);
												});
								// refresh pages
								scope.loading = true;
								scope.getUtilityExp(scope.selectedAddr.id,
										startDate, endDate, scope.offset,
										scope.limit, scope.uePattern).then(
										function(promise) {
											scope.loading = false;
											scope.utilityExpList = promise;
											scope.refreshPages();
											scope.$apply();
										});

								// refresh highcharts
								scope.loadingHC = true;
								scope.getUtilityExp(scope.selectedAddr.id,
										startDate, endDate, 0, 1000000,
										scope.uePattern).then(
										function(promise) {
											scope.loadingHC = false;
											scope.utilityExpListFull = promise;
											scope.$apply();
											generateUtilityExpHighcharts(scope,
													scope.utilityExpListFull);
											generateExpensesPie(scope,
													scope.utilityExpListFull);
											scope.$apply();
										});

								showAlertSuccess(promise.responseText);
							} else if (promise.status === 500) {
								showAlertDanger(promise.responseText);
							} else {
								showAlertWarning(promise.responseText);
							}
						});
	};

	scope.configureUtilityExpense = function() {
		if (!scope.edit) {
			var expense = {
				value : parseFloat(scope.ueValue),
				description : scope.ueDesc,
				date : Date.parse(scope.ueDate)
			};
			postUtilityExpense(JSON.stringify(expense),
					scope.selectedUtility.id)
					.then(
							function(promise) {
								if (promise.status === 200) {
									showAlertSuccess("Utility expense successfuly added");
									// Notify tenants about your modification
									var mail = {
										from : "angry.nerds.house@gmail.com",
										recipients : getTenantsEmails(scope.selectedAddr.assignedPersons),
										subject : getCurrentUserName()
												+ " added new expense: "
												+ scope.selectedUtility.name
												+ " (val. " + scope.ueValue
												+ ")",
										msg : "New utility expense added: "
												+ scope.selectedUtility.name
												+ "\n\nBy user: "
												+ getCurrentUserName()
												+ "\nValue: "
												+ scope.ueValue
												+ "\nDate: "
												+ scope.ueDate
												+ "\nDescription: "
												+ scope.ueDesc
												+ "\n\nFor more details, access your account: http://house-angrynerds.rhcloud.com"
									};
									console.debug("Sending mail to tenants: ",
											JSON.stringify(mail));
									scope.isMailInProgress = true;
									postMail(JSON.stringify(mail))
											.then(
													function(resp) {
														scope.isMailInProgress = false;
														scope.$apply();
														if (resp.status != 200) {
															showAlertDanger("Cannot send email to tenants");
														}
														console
																.debug(
																		"eMail sent with success",
																		scope.isMailInProgress);
													});
									// refresh utilities expense list
									scope.loading = true;
									scope.getUtilityExp(scope.selectedAddr.id,
											startDate, endDate, scope.offset,
											scope.limit, scope.uePattern).then(
											function(promise) {
												scope.loading = false;
												scope.utilityExpList = promise;
												scope.$apply();
												scope.refreshPages();
												scope.$apply();
											});

									// refresh highcharts
									scope.loadingHC = true;
									scope
											.getUtilityExp(
													scope.selectedAddr.id,
													startDate, endDate, 0,
													1000000, scope.uePattern)
											.then(
													function(promise) {
														scope.loadingHC = false;
														scope.utilityExpListFull = promise;
														scope.$apply();
														generateUtilityExpHighcharts(
																scope,
																scope.utilityExpListFull);
														generateExpensesPie(
																scope,
																scope.utilityExpListFull);
														scope.$apply();
													});
								} else if (promise.status === 500) {
									showAlertDanger(promise.responseText);
								} else {
									showAlertWarning(promise.responseText);
								}
							});
		} else {
			var expense = {
				value : parseFloat(scope.ueValue),
				description : scope.ueDesc,
				date : Date.parse(scope.ueDate),
				utility : {
					name : scope.selectedUtility.name,
					id : scope.selectedUtility.id
				}
			};

			updateUtilityExpense(scope.ueId, JSON.stringify(expense))
					.then(
							function(promise) {
								if (promise.status === 200) {
									showAlertSuccess("Utility expense successfuly updated");
									// Notify tenants about your modification
									var mail = {
										from : "angry.nerds.house@gmail.com",
										recipients : getTenantsEmails(scope.selectedAddr.assignedPersons),
										subject : getCurrentUserName()
												+ " updated expense: "
												+ scope.selectedUtility.name
												+ " (val. " + scope.ueValue
												+ ")",
										msg : "Utility expense updated: "
												+ scope.selectedUtility.name
												+ "\n\nBy user: "
												+ getCurrentUserName()
												+ "\nValue: "
												+ scope.ueValue
												+ "\nDate: "
												+ scope.ueDate
												+ "\nDescription: "
												+ scope.ueDesc
												+ "\n\nFor more details, access your account: http://house-angrynerds.rhcloud.com"
									};
									console.debug("Sending mail to tenants: ",
											JSON.stringify(mail));
									scope.isMailInProgress = true;
									postMail(JSON.stringify(mail))
											.then(
													function(resp) {
														scope.isMailInProgress = false;
														scope.$apply();
														if (resp.status != 200) {
															showAlertDanger("Cannot send email to tenants");
														}
														console
																.debug(
																		"eMail sent with success",
																		scope.isMailInProgress);
													});
									scope.loading = true;
									// refresh expenses list
									scope.getUtilityExp(scope.selectedAddr.id,
											startDate, endDate, scope.offset,
											scope.limit, scope.uePattern).then(
											function(promise) {
												scope.loading = false;
												scope.utilityExpList = promise;
												scope.$apply();
												scope.refreshPages();
												scope.$apply();
											});
									// refresh highcharts
									scope.loadingHC = true;
									scope
											.getUtilityExp(
													scope.selectedAddr.id,
													startDate, endDate, 0,
													1000000, scope.uePattern)
											.then(
													function(promise) {
														scope.loadingHC = false;
														scope.utilityExpListFull = promise;
														scope.$apply();
														generateUtilityExpHighcharts(
																scope,
																scope.utilityExpListFull);
														generateExpensesPie(
																scope,
																scope.utilityExpListFull);
														scope.$apply();
													});
								} else if (promise.status === 500) {
									showAlertDanger(promise.responseText);
								} else {
									showAlertWarning(promise.responseText);
								}
							});
		}
	};

	scope.$watch('ueDate', function(newValue) {
		scope.ueDate = newValue;
		scope.complete();
	});

	scope.$watch('ueValue', function() {
		scope.complete();
	});

	scope.complete = function() {
		scope.incomplete = false;
		if (!scope.ueValue || !scope.selectedUtility || !scope.ueDate
				|| isNaN(scope.ueValue)) {
			scope.incomplete = true;
		}
	};
};

