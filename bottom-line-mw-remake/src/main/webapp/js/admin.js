var adminApp = angular.module('bottomlineAdmin', [ 'ngRoute', 'ngWebSocket' ]);

// configure our routes
adminApp.config(function($routeProvider) {
	$routeProvider.when('/address', {
		templateUrl : 'pages/admin/address_edit.html'
	}).when('/tenants', {
		templateUrl : 'pages/admin/tenants_config.html'
	}).when('/rooms', {
		templateUrl : 'pages/admin/rooms_config.html'
	}).when('/utilities', {
		templateUrl : 'pages/admin/utilities_config.html',
		controller : 'adminUtilitiesCtrl'
	}).when('/user_edit', {
		templateUrl : 'pages/admin/user_edit.html',
		controller : 'userEditCtrl'
	})
	// otherwise, welcome!
	.otherwise({
		templateUrl : 'pages/admin/welcome.html'
	});
});

adminApp.controller('navbarCtrl', function($rootScope, $scope, $websocket) {
	$scope.username = getCurrentUserName();
	$scope.logout = function() {
		logoutCurrentUser();
		redirectToLogin();
	}

	// Websocket
	$rootScope.ws = $websocket("ws://" + "house-angrynerds.rhcloud.com:8000"
			+ "/chat");
	$rootScope.ws.onOpen(function() {
		console.log('connection open');
		var clientRegistration = {
			type : "CLIENT_REGISTRATION",
			id : getCurrentUserId()
		};
		$rootScope.ws.send(JSON.stringify(clientRegistration));
	});

	$rootScope.ws.onMessage(function(event) {
		console.log('message: ', event.data);
		var msg = JSON.parse(event.data);
		if (msg.type == "ONLINE_USERS") {
			$rootScope.onlineCount = msg.count;
		}
	});
});

adminApp.controller('adminAddressEditCtrl', function($scope) {
	editAddresses($scope);
});

adminApp.controller('adminTenantsCtrl', function($scope) {
	configTenants($scope);
});

adminApp.controller('adminRoomsCtrl', function($scope) {
	configRooms($scope);
});

adminApp.controller('adminUtilitiesCtrl', function($scope) {
	configUtilities($scope);
});

adminApp.controller('mainAdminCtrl', function($scope) {
	configMainAdmin($scope);
});

adminApp.controller('userEditCtrl', function($scope) {
	editUser($scope);
});