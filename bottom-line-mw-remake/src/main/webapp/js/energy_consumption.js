function energyConsumption(scope) {

	// variables init
	scope.edate = '';
	scope.eindex = '';
	scope.econsumption = '';
	scope.edesc = '';
	scope.eid = '';

	scope.edit = false;
	scope.incomplete = false;
	scope.confirmedEConsumptionId = '';

	scope.limit = 10;
	scope.offset = 0;
	scope.count = '';
	scope.pageIndex = 0;
	scope.loading = false;

	scope.ePattern = '';

	scope.Math = Math;

	scope.getEnergyCons = function(addressId, offset, limit, pattern) {
		scope.loading = true;
		var resp = getEnergy(addressId, offset, limit, pattern);
		scope.loading = false;
		if (resp.status === 200) {
			scope.count = parseInt(resp.getResponseHeader("count"));
			return JSON.parse(resp.responseText);
		} else if (resp.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
			return null;
		} else {
			showAlertDanger(resp.responseText);
			return null;
		}
	};

	if (scope.selectedAddr == null || scope.selectedAddr === undefined) {
		return;
	}

	scope.energyList = scope.getEnergyCons(scope.selectedAddr.id, scope.offset,
			scope.limit, scope.ePattern);

	scope.energyListFull = scope.getEnergyCons(scope.selectedAddr.id, 0, 1000,
			scope.ePattern);
	generateEnergyHighcharts(scope, scope.energyListFull);

	// PAGINATION
	scope.refreshPages = function() {
		scope.pagesCount = new Array(Math.ceil(scope.count / scope.limit));
		console.debug("Pages no: " + scope.pagesCount.length);
		scope.selectedPage = 0;
	}

	scope.refreshPages();

	scope.selectPage = function(index) {
		scope.selectedPage = index;
		scope.energyList = scope.getEnergyCons(scope.selectedAddr.id, index
				* scope.limit, scope.limit, scope.ePattern);
	}
	scope.lastPage = function() {
		scope.selectedPage = scope.pagesCount.length - 1;
		scope.energyList = scope.getEnergyCons(scope.selectedAddr.id,
				scope.limit * scope.selectedPage, scope.limit, scope.ePattern);
	}
	scope.firstPage = function() {
		scope.selectedPage = 0;
		scope.energyList = scope.getEnergyCons(scope.selectedAddr.id,
				scope.limit * scope.selectedPage, scope.limit, scope.ePattern);
	}

	scope.editEConsumption = function(eCons) {
		$('#modalEConsumption').modal('show');
		if (eCons == 'new') {
			scope.edit = false;
			scope.incomplete = true;
			scope.edate = '';
			scope.eindex = '';
			scope.econsumption = '';
			scope.edesc = '';
			scope.hid = '';
		} else {
			scope.edit = true;
			$(".datepicker").datepicker("update", new Date(eCons.date));
			scope.eindex = eCons.index;
			scope.econsumption = eCons.consumption != null ? eCons.consumption
					: null;
			scope.edesc = eCons.description;
			scope.eid = eCons.id;
		}
	};

	scope.searchEnergy = function(pattern) {
		console.debug("Search by pattern: " + pattern);

		var resp = getEnergy(scope.selectedAddr.id, scope.offset, scope.limit,
				pattern);
		var resp1 = getEnergy(scope.selectedAddr.id, 0, 1000, pattern);

		if (resp.status === 200 && resp1.status === 200) {
			scope.count = parseInt(resp.getResponseHeader("count"));
			scope.energyList = JSON.parse(resp.responseText);
			scope.energyListFull = JSON.parse(resp1.responseText);
			scope.refreshPages();
			generateEnergyHighcharts(scope, scope.energyListFull);
		} else if (resp.status === 500 || resp1.status === 500) {
			showAlertDanger("INTERNAL SERVER ERROR");
			return null;
		} else {
			if (resp.status != 200 && resp.status != 500) {
				showAlertWarning(resp.responseText);
			}
			if (resp1.status != 200 && resp1.status != 500) {
				showAlertWarning(resp1.responseText);
			}
			return null;
		}
	};

	scope.confirmEnergyRemoval = function(id) {
		scope.energyIdConfirmed = id;
		scope.modal_title = "Remove energy";
		scope.modal_body = "You are about to remove a energy consumption entry. Proceed ? :/";
		console.debug("Remove energy with id: " + id + " confirmation");
		$('#modalId').modal('show');
	};

	// remove energy consumption
	scope.modal_function = function() {
		var xhr = removeEnergy(scope.energyIdConfirmed);
		if (xhr.status === 200) {
			scope.energyList = scope.getEnergyCons(scope.selectedAddr.id,
					scope.offset, scope.limit, scope.ePattern);
			scope.refreshPages();
			showAlertSuccess(xhr.responseText);
		} else if (xhr.status === 500) {
			showAlertDanger(xhr.responseText);
		} else {
			showAlertWarning(xhr.responseText);
		}
	};

	scope.configureEConsumption = function() {

		if (!scope.edit) {
			var energy = {
				index : scope.eindex,
				consumption : scope.econsumption,
				description : scope.edesc,
				date : Date.parse(scope.edate)
			};
			var xhr = postEnergy(JSON.stringify(energy), scope.selectedAddr.id);
			if (xhr.status === 200) {
				showAlertSuccess("Energy consumption successfuly added");
				// refresh energy list
				scope.energyList = scope.getEnergyCons(scope.selectedAddr.id,
						scope.offset, scope.limit, scope.ePattern);
				scope.refreshPages();
			} else if (xhr.status === 500) {
				showAlertDanger(xhr.responseText);
			} else {
				showAlertWarning(xhr.responseText);
			}
		} else {
			var energy = {
				index : scope.eindex,
				consumption : scope.econsumption,
				description : scope.edesc,
				date : Date.parse(scope.edate)
			};
			var xhr = updateEnergy(scope.eid, JSON.stringify(energy));
			if (xhr.status === 200) {
				showAlertSuccess("Energy consumption successfuly updated");
				// refresh energy list
				scope.energyList = scope.getEnergyCons(scope.selectedAddr.id,
						scope.offset, scope.limit, scope.ePattern);
				scope.refreshPages();
			} else if (xhr.status === 500) {
				showAlertDanger(xhr.responseText);
			} else {
				showAlertWarning(xhr.responseText);
			}
		}
	};

	scope.$watch('edate', function(newValue) {
		scope.edate = newValue;
		scope.complete();
	});

	scope.$watch('eindex', function() {
		scope.complete();
	});

	scope.$watch('econsumption', function() {
		scope.complete();
	});

	scope.complete = function() {
		scope.incomplete = false;
		if (!scope.eindex || !scope.edate || isNaN(scope.eindex)
				|| isNaN(scope.econsumption)) {
			scope.incomplete = true;
		}
	};
};

