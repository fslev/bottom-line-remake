SET foreign_key_checks = 0;
-- MySQL dump 10.14  Distrib 5.5.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: nexus
-- ------------------------------------------------------
-- Server version	5.5.44-MariaDB-1ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (13,'Montecarotto','Algeria','754-5521 Non, Rd.'),(11,'Laino Castello','Chad','974-3359 Dolor Road'),(10,'Jackson','Christmas Island','P.O. Box 623, 8722 Nunc St.'),(3,'Tolentino','Ecuador','Ap #874-4241 Montes, Rd.'),(4,'Dollard-des-Ormeaux','Gambia','5356 Aliquet Road'),(9,'Prince Albert','Guam','705-6903 Risus Road'),(1,'Bouwel','Ireland','Ap #937-4436 Vestibulum, Road'),(7,'Clauzetto','Laos','P.O. Box 827, 4240 Nisl. Avenue'),(14,'North Vancouver','Liberia','488-8817 Mi Road'),(2,'Veldwezelt','Morocco','968-2987 Velit. Avenue'),(18,'Bucuresti','Romania','Splaiul Independentei nr. 290, Bloc P16, Ap.218'),(16,'Bucuresti','Romania','Strada Splaiul Independentei nr. 290, Bloc P16, Ap. 206'),(15,'Bucuresti','Romania','Strada Trestiana nr. 9, Bloc 11, Ap. 58'),(17,'Bucuresti','Romania','Trestiana nr. 9, Bloc 11, Sc.B, Ap.58'),(5,'Papasidero','Singapore','Ap #915-3711 Maecenas St.'),(12,'Rae-Edzo','Sweden','Ap #884-9090 Tempor Rd.'),(8,'Castor','Turkmenistan','P.O. Box 957, 9423 Neque St.'),(6,'Koln','Turkmenistan','P.O. Box 173, 3519 Tempor Street');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `address_invitation`
--

LOCK TABLES `address_invitation` WRITE;
/*!40000 ALTER TABLE `address_invitation` DISABLE KEYS */;
INSERT INTO `address_invitation` VALUES (1,'','\0',13,1,2),(2,'','\0',15,3,2),(3,'','\0',13,3,2),(4,'','\0',17,2,4),(5,'','\0',17,3,4),(6,'','\0',17,1,4),(7,'','\0',17,5,4);
/*!40000 ALTER TABLE `address_invitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `address_person`
--

LOCK TABLES `address_person` WRITE;
/*!40000 ALTER TABLE `address_person` DISABLE KEYS */;
INSERT INTO `address_person` VALUES (1,'',12,1),(2,'',13,2),(9,'\0',15,2),(10,'\0',13,1),(11,'\0',15,3),(12,'\0',13,3),(13,'',17,4),(14,'',18,4),(15,'\0',17,5),(16,'\0',17,3),(17,'\0',17,2),(18,'\0',17,1);
/*!40000 ALTER TABLE `address_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `debt`
--

LOCK TABLES `debt` WRITE;
/*!40000 ALTER TABLE `debt` DISABLE KEYS */;
/*!40000 ALTER TABLE `debt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `energy_consumption`
--

LOCK TABLES `energy_consumption` WRITE;
/*!40000 ALTER TABLE `energy_consumption` DISABLE KEYS */;
INSERT INTO `energy_consumption` VALUES (1,NULL,1414706400000,'',4377,17),(2,113,1417298400000,'',4490,17),(3,67,1419026400000,'',4557,17),(4,43,1420581600000,'',4600,17),(5,69,1422655200000,'',4669,17),(6,100,1425506400000,'',4769,17),(7,116,1427922000000,'',4885,17),(8,85,1430427600000,'',4970,17),(9,111,1433106000000,'',5081,17),(10,119,1436130000000,'',5200,17),(11,186,1438635600000,'',5386,17),(12,141,1441314000000,'',5527,17),(13,109,1443646800000,'',5636,17);
/*!40000 ALTER TABLE `energy_consumption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `heat_consumption`
--

LOCK TABLES `heat_consumption` WRITE;
/*!40000 ALTER TABLE `heat_consumption` DISABLE KEYS */;
INSERT INTO `heat_consumption` VALUES (1,NULL,1417384800000,'',159,17,7),(2,NULL,1417384800000,'',237,17,8),(3,83,1419026400000,'',242,17,7),(4,0,1417384800000,'',0,17,6),(5,0,1417384800000,'',0,17,5),(6,25,1419026400000,'',262,17,8),(7,0,1419026400000,'',0,17,6),(8,0,1419026400000,'',0,17,5),(9,3,1419976800000,'',245,17,7),(10,6,1419976800000,'',268,17,8),(11,0,1419976800000,'',0,17,6),(12,0,1419976800000,'',0,17,5),(13,108,1422655200000,'',108,17,7),(14,55,1422655200000,'',55,17,8),(15,0,1422655200000,'',0,17,6),(16,0,1422655200000,'',0,17,5),(17,64,1425420000000,'',172,17,7),(18,19,1425420000000,'',74,17,8),(19,0,1425420000000,'',0,17,5),(20,0,1425420000000,'',0,17,6),(21,9,1427922000000,'',181,17,7),(22,11,1427922000000,'',85,17,8),(23,0,1427922000000,'',0,17,5),(24,0,1427922000000,'',0,17,6),(25,0,1433106000000,'',181,17,7),(26,1,1433106000000,'',86,17,8),(27,0,1433106000000,'',0,17,5),(28,0,1433106000000,'',0,17,6);
/*!40000 ALTER TABLE `heat_consumption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'slevoaca.florin@gmail.com','Florin Slevoaca','�u���1Ù�iw&a','fslevoaca'),(2,'iulia.neacsu@gmail.com','Iulia Neacsu','�u���1Ù�iw&a','ineacsu'),(3,'matei.ornea@1and1.org','Matei Ornea','�u���1Ù�iw&a','mornea'),(4,'tuser@gmail.com','tuser','�u���1Ù�iw&a','tuser'),(5,'vis@gmail.com','guest','�u���1Ù�iw&a','guest');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `room_type`
--

LOCK TABLES `room_type` WRITE;
/*!40000 ALTER TABLE `room_type` DISABLE KEYS */;
INSERT INTO `room_type` VALUES (2,'Bathroom',13),(4,'Bedroom',13),(5,'Bucatarie',17),(6,'Baie',17),(7,'Dormitor',17),(8,'Sufragerie',17);
/*!40000 ALTER TABLE `room_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `utilities_debt_audit`
--

LOCK TABLES `utilities_debt_audit` WRITE;
/*!40000 ALTER TABLE `utilities_debt_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `utilities_debt_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `utilities_expense`
--

LOCK TABLES `utilities_expense` WRITE;
/*!40000 ALTER TABLE `utilities_expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `utilities_expense` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `utilities_expense_payment`
--

LOCK TABLES `utilities_expense_payment` WRITE;
/*!40000 ALTER TABLE `utilities_expense_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `utilities_expense_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `utility`
--

LOCK TABLES `utility` WRITE;
/*!40000 ALTER TABLE `utility` DISABLE KEYS */;
INSERT INTO `utility` VALUES (1,'RCS-RDS',13),(2,'Enel Moldova',13),(3,'Rent payment',13),(4,'Enel Muntenia',17),(5,'RDS-RCS',17),(6,'Intretinere',17),(7,'Chirie',17);
/*!40000 ALTER TABLE `utility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `utility_debt_calculus`
--

LOCK TABLES `utility_debt_calculus` WRITE;
/*!40000 ALTER TABLE `utility_debt_calculus` DISABLE KEYS */;
/*!40000 ALTER TABLE `utility_debt_calculus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `water_consumption`
--

LOCK TABLES `water_consumption` WRITE;
/*!40000 ALTER TABLE `water_consumption` DISABLE KEYS */;
INSERT INTO `water_consumption` VALUES (3,6,1441227600000,'',8,'HOT',13,2),(4,8,1441141200000,'dfs',7,'COLD',13,2),(5,6,1442437200000,'',5,'COLD',13,2),(6,3,1441918800000,'dfas',43,'COLD',13,2),(7,2,1441227600000,'aaaa',3,'COLD',13,4),(8,2,1441400400000,'',9,'HOT',13,2),(9,3,1442005200000,'fdsfsd',7,'COLD',13,2),(10,34,1441832400000,'',13,'HOT',13,2),(11,3,1440968400000,'',4,'COLD',13,2),(12,3,1438981200000,'',3,'COLD',13,2),(13,1,1439067600000,'',1,'HOT',13,4),(14,1,1442178000000,'',1,'COLD',13,4),(15,3,1441832400000,'',21,'COLD',13,2),(16,3,1442437200000,'',3,'COLD',13,4),(17,1,1442523600000,'',1,'COLD',13,4),(18,1,1441141200000,'',1,'COLD',13,4),(19,1,1442869200000,'',1,'HOT',13,2),(20,3,1443128400000,'',1,'HOT',13,2),(21,1,1443214800000,'',1,'HOT',13,4),(22,4,1442437200000,'',3,'HOT',13,2),(23,1,1444338000000,'',1,'HOT',13,2),(24,1,1446156000000,'',1,'HOT',13,2),(25,1,1446760800000,'',1,'COLD',13,2),(26,NULL,1414706400000,'',103,'COLD',17,5),(27,NULL,1414706400000,'',105,'HOT',17,5),(28,NULL,1414706400000,'',469,'COLD',17,6),(29,NULL,1414706400000,'',188,'HOT',17,6),(30,0,1417298400000,'',103,'COLD',17,5),(31,1,1417298400000,'',106,'HOT',17,5),(32,1,1417298400000,'',470,'COLD',17,6),(33,2,1417298400000,'',190,'HOT',17,6),(34,1,1419026400000,'',104,'COLD',17,5),(35,0,1419026400000,'',106,'HOT',17,5),(36,0,1419026400000,'',470,'COLD',17,6),(37,1,1419026400000,'',191,'HOT',17,6),(38,0,1420581600000,'',104,'COLD',17,5),(39,0,1420581600000,'',106,'HOT',17,5),(40,1,1420581600000,'',471,'COLD',17,6),(41,0,1420581600000,'',191,'HOT',17,6),(42,0,1422655200000,'',104,'COLD',17,5),(43,1,1422655200000,'',107,'HOT',17,5),(44,0,1422655200000,'',471,'COLD',17,6),(45,2,1422655200000,'',193,'HOT',17,6),(46,1,1425506400000,'',105,'COLD',17,5),(47,1,1425506400000,'',108,'HOT',17,5),(48,1,1425506400000,'',472,'COLD',17,6),(49,3,1425506400000,'',196,'HOT',17,6),(50,1,1427922000000,'',106,'COLD',17,5),(51,0,1427922000000,'',108,'HOT',17,5),(52,1,1427922000000,'',473,'COLD',17,6),(53,2,1427922000000,'',198,'HOT',17,6),(54,1,1430427600000,'',107,'COLD',17,5),(55,0,1430427600000,'',108,'HOT',17,5),(56,1,1430427600000,'',474,'COLD',17,6),(57,1,1430427600000,'',199,'HOT',17,6),(58,1,1433106000000,'',108,'COLD',17,5),(59,1,1433106000000,'',109,'HOT',17,5),(60,1,1433106000000,'',475,'COLD',17,6),(61,1,1433106000000,'',200,'HOT',17,6),(62,1,1436130000000,'',109,'COLD',17,5),(63,0,1436130000000,'',109,'HOT',17,5),(64,3,1436130000000,'',478,'COLD',17,6),(65,1,1436130000000,'',201,'HOT',17,6),(66,1,1438635600000,'',110,'COLD',17,5),(67,0,1438635600000,'',109,'HOT',17,5),(68,3,1438635600000,'',481,'COLD',17,6),(69,1,1438635600000,'',202,'HOT',17,6),(70,2,1441314000000,'',112,'COLD',17,5),(71,0,1441314000000,'',109,'HOT',17,5),(72,4,1441314000000,'',485,'COLD',17,6),(73,2,1441314000000,'',204,'HOT',17,6),(74,1,1443646800000,'',113,'COLD',17,5),(75,0,1443646800000,'',109,'HOT',17,5),(76,3,1443646800000,'',488,'COLD',17,6),(77,1,1443646800000,'',205,'HOT',17,6);
/*!40000 ALTER TABLE `water_consumption` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-03 16:07:34
SET foreign_key_checks = 1;