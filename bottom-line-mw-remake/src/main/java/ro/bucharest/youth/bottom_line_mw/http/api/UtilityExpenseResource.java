package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.Utility;
import ro.bucharest.youth.bottom_line_mw.entities.UtilityExpense;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;
import ro.bucharest.youth.bottom_line_mw.utils.PatternProcessor.InvalidStringCriteria;
import ro.bucharest.youth.bottom_line_mw.utils.QueryBuilder;

@Path("utilityexp")
@AuthorizationRequestFilter
public class UtilityExpenseResource {

	private static final Logger log = LogManager.getLogger(UtilityExpenseResource.class);

	@Context
	private SecurityContext secContext;

	@POST
	@JsonObjectRequestFilter
	@Path("utility/{utilityId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addUtilityExpense(final UtilityExpense utilityExp, @PathParam("utilityId") final int utilityId) {
		log.debug("Received request to add utility expense {} for utility: {}", utilityExp.getId(), utilityId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {

				validateFields(utilityExp);
				Utility utility = UtilityResource.getUtilityNotNull(session, utilityId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				AddressResource.checkUserIsAssignedToAddress(userId, utility.getAddress());

				UtilityExpense result = (UtilityExpense) session
						.createQuery("from UtilityExpense ue where ue.date = :date and ue.utility.id= :utilityId")
						.setParameter("date", utilityExp.getDate()).setParameter("utilityId", utilityId).uniqueResult();
				if (result != null) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Utility expense already exists").build());
				} else {
					utilityExp.setAddress(utility.getAddress());
					utilityExp.setUtility(utility);
					session.persist(utilityExp);
				}
			}
		}.execute();
		log.debug("Utility expense persisted");

		return Response.status(Status.OK).entity("Utility expense was successfully added").build();
	}

	@GET
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response getUtilitiesExpenses(@QueryParam("addressId") final int addressId,
			@QueryParam("startDate") final long startDate, @QueryParam("endDate") final long endDate,
			@QueryParam(value = "offset") final Integer offset, @QueryParam(value = "limit") final Integer limit,
			@QueryParam(value = "pattern") final String pattern) {

		log.debug("Get all utility expenses for addressId {} and offset {} and limit {} and pattern {}", addressId,
				offset, limit, pattern);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		final QueryBuilder queryBuilder = new QueryBuilder(pattern);
		queryBuilder.acceptField("utility", "utility.name").acceptField("date", "date").acceptField("value", "value");

		List<UtilityExpense> list = new HibernateTransaction(App.sessionFactory).new WithValue<List<UtilityExpense>>() {
			@Override
			public List<UtilityExpense> run(Session session) {

				Address address = AddressResource.getAddressNotNull(session, addressId);
				AddressResource.checkUserIsAssignedToAddress(userId, address);

				StringBuilder queryString = new StringBuilder(
						"from UtilityExpense ue where ue.address.id = ? and ue.date>=? and ue.date<=?");

				final String queryPattern;
				try {
					queryPattern = queryBuilder.build(queryString).toString();
				} catch (InvalidStringCriteria e) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
				}

				log.debug("RUNNING special query: {}", queryPattern);
				Query query = session.createQuery(queryPattern + " order by ue.date desc").setInteger(0, addressId)
						.setLong(1, startDate).setLong(2, endDate);
				for (int i = 0; i < queryBuilder.getCriteriaList().size(); i++) {
					query.setString(i + 3, queryBuilder.getCriteriaList().get(i).value);
				}

				if (limit != null)
					query.setMaxResults(limit);
				if (offset != null)
					query.setFirstResult(offset);

				return query.list();
			}
		}.execute();

		// Get query count for pagination
		long count = new HibernateTransaction(App.sessionFactory).new WithValue<Long>() {
			@Override
			public Long run(Session session) {

				StringBuilder queryString = new StringBuilder(
						"select count(ue) from UtilityExpense ue where ue.address.id = ? and ue.date>=? and ue.date<=?");

				final String queryPattern;
				try {
					queryPattern = queryBuilder.build(queryString).toString();
				} catch (InvalidStringCriteria e) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
				}

				log.debug("RUNNING special query: {}", queryPattern);
				Query query = session.createQuery(queryPattern).setInteger(0, addressId).setLong(1, startDate)
						.setLong(2, endDate);
				for (int i = 0; i < queryBuilder.getCriteriaList().size(); i++) {
					query.setString(i + 3, queryBuilder.getCriteriaList().get(i).value);
				}
				return (Long) query.uniqueResult();
			}
		}.execute();

		return Response.status(Status.OK).entity(list).header("count", count).build();

		// for (UtilityExpense expense : address.getUtilitiesExpenses()) {
		// expenses.add(expense);
		// }
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Removes Utility expense
	 */
	public Response delete(@PathParam("id") final int utilityExpId) {
		log.debug("Remove utility expense {}", utilityExpId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				UtilityExpense utilityExp = getUtilityExpNotNull(session, utilityExpId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				Address address = utilityExp.getAddress();
				AddressResource.checkUserIsAssignedToAddress(userId, address);
				session.delete(utilityExp);
			}
		}.execute();
		return Response.status(Status.OK).entity("Utility expense successfully removed").build();
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@JsonObjectRequestFilter
	public Response update(@PathParam("id") final int utilityExpId, final UtilityExpense utilityExp) {
		log.debug("Update utility expense {} with {}", utilityExpId, utilityExp);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// get utility expense from database
				UtilityExpense destExpUtility = getUtilityExpNotNull(session, utilityExpId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				AddressResource.checkUserIsAssignedToAddress(userId, destExpUtility.getAddress());
				UtilityExpense result = (UtilityExpense) session
						.createQuery(
								"from UtilityExpense ue where ue.date = :date and ue.utility.id= :utilityId and ue.id!=:id")
						.setParameter("date", utilityExp.getDate())
						.setParameter("utilityId", utilityExp.getUtility().getId()).setParameter("id", utilityExpId)
						.uniqueResult();
				if (result != null) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Utility expense already exists").build());
				}
				// update utility expense
				destExpUtility.setDate(utilityExp.getDate());
				destExpUtility.setValue(utilityExp.getValue());
				destExpUtility.setDescription(utilityExp.getDescription());
				if (utilityExp.getUtility() != null) {
					Utility utility = UtilityResource.getUtilityNotNull(session, utilityExp.getUtility().getId());
					destExpUtility.setUtility(utility);
				}
				session.merge(destExpUtility);
			}
		}.execute();

		return Response.status(Status.OK).entity("Utility expense successfully updated").build();
	}

	private static void validateFields(UtilityExpense expense) {
		if (expense.getDate() < 1 || expense.getValue() < 0) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_INVALID_FIELDS).build());
		}
	}

	public static UtilityExpense getUtilityExpNotNull(Session session, int utilityExpId) {
		UtilityExpense utilityExp = (UtilityExpense) session.get(UtilityExpense.class, utilityExpId);
		if (utilityExp == null) {
			log.warn("Utility expense with id {} doesn't exist.", utilityExp);
			throw new WebApplicationException(
					Response.status(Status.NOT_FOUND).entity("Utility expense does not exist").build());
		}
		return utilityExp;
	}
}