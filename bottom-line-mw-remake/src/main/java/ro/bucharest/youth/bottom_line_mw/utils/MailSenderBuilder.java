package ro.bucharest.youth.bottom_line_mw.utils;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ro.bucharest.youth.bottom_line_mw.AppProperties;

public class MailSenderBuilder {

	private static final Logger log = LogManager.getLogger(MailSenderBuilder.class);

	private Properties properties;
	private String from;
	private String subject;
	private Set<String> recipients = new HashSet<>();

	public MailSenderBuilder() {
		properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.host", AppProperties.getSmtpHost());
		properties.setProperty("mail.smtp.port", AppProperties.getSmtpPort());
	}

	public MailSenderBuilder recipient(String recipient) {
		this.recipients.add(recipient);
		return this;
	}

	public MailSenderBuilder subject(String subject) {
		this.subject = subject;
		return this;
	}

	public MailSenderBuilder from(String from) {
		this.from = from;
		return this;
	}

	public MailSenderBuilder build() {
		return this;
	}

	public boolean send(String msg) {

		Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(AppProperties.getSmtpUser(), AppProperties.getSmtpPwd());
			}
		});

		try {
			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(from));

			for (String recipient : recipients) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
			}

			message.setSubject(subject);
			message.setText(msg);

			// Send message
			Transport.send(message);
			log.info("Sent mail successfully to recipients {}", recipients);
			return true;
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		return false;
	}

	// public static void main(String[] args) {
	// new
	// MailSenderBuilder().from("dea@gmail.com").recipient("slevoaca.florin@gmail.com")
	// .recipient("angry.nderds.house@gmail.com").recipient("florynu2004@yahoo.com").subject("test
	// me")
	// .send("bah");
	// }
}
