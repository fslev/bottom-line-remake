package ro.bucharest.youth.bottom_line_mw.security;

import java.security.Principal;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import ro.bucharest.youth.bottom_line_mw.utils.Messages;

public class LoggedUserResourceManager {

	private SecurityContext secContext;

	public LoggedUserResourceManager(SecurityContext secContext) {
		this.secContext = secContext;
	}

	public int getUserId() {
		Principal princpal = secContext.getUserPrincipal();
		if (princpal == null) {
			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED)
					.entity(Messages.WARNING_PRINCIPAL_NOT_FOUND).build());
		}
		return Integer.valueOf(princpal.getName());
	}
}
