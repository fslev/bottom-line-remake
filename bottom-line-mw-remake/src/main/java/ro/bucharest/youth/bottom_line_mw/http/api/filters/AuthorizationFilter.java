package ro.bucharest.youth.bottom_line_mw.http.api.filters;

import java.security.Principal;

import javax.annotation.Priority;
import javax.naming.AuthenticationException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.Token;
import ro.bucharest.youth.bottom_line_mw.security.TokenResourceManager;

@Provider
@Priority(Priorities.AUTHORIZATION)
@AuthorizationRequestFilter
public class AuthorizationFilter implements ContainerRequestFilter {

	private static final Logger log = LogManager.getLogger(AuthorizationFilter.class);

	@Override
	public void filter(ContainerRequestContext request) {

		User user;
		try {
			user = authorize(request);
			request.setSecurityContext(new Authorizer(user));
		} catch (AuthenticationException e) {
			request.abortWith(Response.status(Status.UNAUTHORIZED).entity(e.getMessage()).build());
		}

	}

	private User authorize(ContainerRequestContext request) throws AuthenticationException {

		Token token = new TokenResourceManager(request).extractToken();

		User user = new User(String.valueOf(token.getUserId()));
		log.debug("Authorization successful");
		return user;
	}

	public class Authorizer implements SecurityContext {

		private User user;
		private Principal principal;

		public Authorizer(final User user) {
			this.user = user;
			this.principal = new Principal() {

				public String getName() {
					return user.username;
				}
			};
		}

		public Principal getUserPrincipal() {
			return this.principal;
		}

		public boolean isUserInRole(String role) {
			return false;
		}

		public boolean isSecure() {
			return false;
		}

		public String getAuthenticationScheme() {
			return SecurityContext.BASIC_AUTH;
		}
	}

	public class User {
		public String username;

		public User(String username) {
			this.username = username;
		}
	}
}