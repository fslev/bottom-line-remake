package ro.bucharest.youth.bottom_line_mw.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Class containing math formulae definitions for debt calculus for a person
 * 
 * @author raf
 *
 */
@Entity(name = "utility_debt_calculus")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "address_id", "debtor_id",
		"utility_id" }))
public class UtilityDebtCalculus {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, insertable = false)
	private int id;

	@Column(name = "formula", length = 2048, nullable = false)
	private String formula;

	@Column(name = "comment", length = 300)
	private String comment;

	@ManyToOne(optional = false)
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	private Address address;

	@ManyToOne(optional = false)
	@JoinColumn(name = "debtor_id", referencedColumnName = "id")
	private Person debtor;

	@ManyToOne(optional = false)
	@JoinColumn(name = "utility_id", referencedColumnName = "id")
	private Utility utility;

	public UtilityDebtCalculus() {

	}

	public UtilityDebtCalculus(String formula, String comment, Address address, Person debtor,
			Utility utility) {
		super();
		this.formula = formula;
		this.comment = comment;
		this.address = address;
		this.debtor = debtor;
		this.utility = utility;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Person getDebtor() {
		return debtor;
	}

	public void setDebtor(Person debtor) {
		this.debtor = debtor;
	}

	public Utility getUtility() {
		return utility;
	}

	public void setUtility(Utility utility) {
		this.utility = utility;
	}

	public int getId() {
		return id;
	}
}
