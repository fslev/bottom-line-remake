package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.AddressInvitation;
import ro.bucharest.youth.bottom_line_mw.entities.AddressPerson;
import ro.bucharest.youth.bottom_line_mw.entities.Person;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;

@Path("invitations")
@AuthorizationRequestFilter
public class AddressInvitationResource {

	private static final Logger log = LogManager.getLogger(AddressInvitationResource.class);

	@Context
	private SecurityContext secContext;

	@POST
	@Path("/address/{addressId}/srcuser/{srcUserId}/destuser/{destUserId}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response inviteUserToAddress(@PathParam("addressId") final int addressId,
			@PathParam("srcUserId") final int srcUserId, @PathParam("destUserId") final int destUserId) {
		log.debug("Received request from user {} to assign address {} to user {}", addressId, srcUserId, destUserId);

		if (srcUserId == destUserId) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity("You cannot invite yourself. It's rude").build());
		}

		int loggedUserId = new LoggedUserResourceManager(secContext).getUserId();
		if (loggedUserId != srcUserId)
			throw new WebApplicationException(
					Response.status(Status.FORBIDDEN).entity(Messages.WARNING_USER_HAS_NO_RIGHTS).build());

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check address exists inside db
				Address address = AddressResource.getAddressNotNull(session, addressId);
				AddressResource.checkUserIsOwnerForAddress(srcUserId, address);
				Person srcPerson = PersonResource.getPersonNotNull(session, srcUserId);
				Person destPerson = PersonResource.getPersonNotNull(session, destUserId);

				// Check if person is already assigned to address
				if (isPersonAssignedToAddress(address, destPerson)) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Person already assigned to address").build());
				}
				// Check if another address invitation is already placed
				AddressInvitation invitation = (AddressInvitation) session
						.createQuery(
								"from AddressInvitation ai where ai.address.id= :addressId and ai.srcPerson.id= :srcUserId and ai.destPerson.id= :destUserId and ai.active=true")
						.setParameter("addressId", addressId).setParameter("destUserId", destUserId)
						.setParameter("srcUserId", srcUserId).uniqueResult();
				if (invitation != null) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Invitation already placed").build());
				}
				invitation = new AddressInvitation(address, srcPerson, destPerson);
				session.persist(invitation);
			}
		}.execute();
		log.debug("Approval persisted.");

		return Response.status(Status.OK).entity("User has been asked for address assignment approval").build();
	}

	@GET
	@Path("/destuser/{destUserId}/addresses")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AddressInvitation> getAddressInvitations(@PathParam("destUserId") final int destUserId) {
		log.debug("Get address invites for user {}", destUserId);

		int loggedUserId = new LoggedUserResourceManager(secContext).getUserId();
		if (loggedUserId != destUserId)
			throw new WebApplicationException(
					Response.status(Status.FORBIDDEN).entity(Messages.WARNING_USER_HAS_NO_RIGHTS).build());

		return new HibernateTransaction(App.sessionFactory).new WithValue<List<AddressInvitation>>() {
			@Override
			public List<AddressInvitation> run(Session session) {
				return session.createQuery("from AddressInvitation ai where ai.destPerson.id= :destUserId")
						.setParameter("destUserId", destUserId).list();
			}
		}.execute();
	}

	@PUT
	@Path("/accept/{addrInvitationId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response acceptAddressInvitation(@PathParam("addrInvitationId") final int addrInvId) {

		log.debug("Accept address invitation {}", addrInvId);
		final int loggedUserId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {

			@Override
			public void run(Session session) {
				AddressInvitation invitation = getAddressInvitationNotNull(session, addrInvId);
				if (loggedUserId != invitation.getDestPerson().getId()) {
					throw new WebApplicationException(
							Response.status(Status.FORBIDDEN).entity(Messages.WARNING_USER_HAS_NO_RIGHTS).build());
				}

				Address address = invitation.getAddress();
				Person person = invitation.getDestPerson();
				address.getAddressesPersons().add(new AddressPerson(address, person, false));
				invitation.setActive(false);
				invitation.setAccepted(true);
				session.merge(address);
				session.merge(invitation);
				log.debug("Assign user to address");
			}
		}.execute();

		return Response.status(Status.OK).entity(Messages.SUCCESS_ADDRESS_INVITATION_ACCEPTED).build();
	}

	private boolean isPersonAssignedToAddress(Address address, Person person) {
		for (AddressPerson addrPers : address.getAddressesPersons()) {
			if (addrPers.getPerson().getId() == person.getId()) {
				return true;
			}
		}
		return false;
	}

	public static AddressInvitation getAddressInvitationNotNull(Session session, int addrInvId) {
		AddressInvitation invitation = (AddressInvitation) session.get(AddressInvitation.class, addrInvId);
		if (invitation == null) {
			log.warn("Address invitation with id {} doesn't exist.", addrInvId);
			throw new WebApplicationException(Response.status(Status.NOT_FOUND)
					.entity(Messages.WARNING_ADDRESS_INVITATION_DOES_NOT_EXIST).build());
		}
		return invitation;
	}
}
