package ro.bucharest.youth.bottom_line_mw.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "heat_consumption")
public class HeatConsumption extends AbstractUtilitiesConsumption {

	private static final long serialVersionUID = -8258488709468782504L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "room_type_id", referencedColumnName = "id")
	private RoomType room;

	public HeatConsumption() {
	};

	public RoomType getRoom() {
		return room;
	}

	public void setRoom(RoomType room) {
		this.room = room;
	}

	@Override
	public String toString() {
		return "HeatConsumption [room=" + room + ", id=" + id + ", index=" + index
				+ ", consumption=" + consumption + ", description=" + description + ", date="
				+ date + "]";
	}
}
