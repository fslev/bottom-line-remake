package ro.bucharest.youth.bottom_line_mw.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class HibernateTransaction {

	private static final Logger log = LogManager.getLogger(HibernateTransaction.class);

	private final SessionFactory sessionFactory;

	public HibernateTransaction(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public abstract class WithValue<T> {

		public final Session session = sessionFactory.openSession();

		public abstract T run(Session session);

		public T execute() {
			try {
				session.getTransaction().begin();
				T result = run(session);
				session.getTransaction().commit();
				return result;
			} catch (Exception e) {
				log.error(e);
				session.getTransaction().rollback();
				throw e;
			} finally {
				session.close();
			}
		}
	}

	public abstract class NoValue {

		public final Session session = sessionFactory.openSession();

		public abstract void run(Session session);

		public void execute() {
			try {
				session.getTransaction().begin();
				run(session);
				session.getTransaction().commit();
			} catch (Exception e) {
				log.error(e);
				session.getTransaction().rollback();
				throw e;
			} finally {
				session.close();
			}
		}
	}
}
