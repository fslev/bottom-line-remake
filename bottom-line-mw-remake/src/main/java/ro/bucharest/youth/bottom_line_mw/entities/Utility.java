package ro.bucharest.youth.bottom_line_mw.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "utility")
@XmlRootElement
public class Utility implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false, nullable = false)
	private int id;

	@Column(name = "name", nullable = false)
	private String name;

	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	private Address address;

	@JsonIgnore
	@OneToMany(mappedBy = "utility", cascade = CascadeType.ALL)
	private List<UtilityExpense> utilitiesExpenses = new ArrayList<UtilityExpense>();

	public Utility() {

	}

	public Utility(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public List<UtilityExpense> getUtilitiesExpenses() {
		return utilitiesExpenses;
	}

	public void addUtilitiesExpense(UtilityExpense utilitiesExpense) {
		this.utilitiesExpenses.add(utilitiesExpense);
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Utility other = (Utility) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Utility [id=" + id + ", name=" + name + ", utilitiesExpenses=" + utilitiesExpenses + "]";
	}
}
