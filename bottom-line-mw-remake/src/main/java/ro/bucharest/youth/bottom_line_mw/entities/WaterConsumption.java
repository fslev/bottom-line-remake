package ro.bucharest.youth.bottom_line_mw.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "water_consumption")
public class WaterConsumption extends AbstractUtilitiesConsumption {

	private static final long serialVersionUID = 4130525820877412491L;

	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false, length = 4)
	private Type type;

	@ManyToOne(optional = false)
	@JoinColumn(name = "room_type_id", referencedColumnName = "id")
	private RoomType room;

	public static enum Type {
		COLD, HOT
	}

	public WaterConsumption() {

	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public RoomType getRoom() {
		return room;
	}

	public void setRoom(RoomType room) {
		this.room = room;
	}

	@Override
	public String toString() {
		return "WaterConsumption [type=" + type + ", room=" + room + ", index=" + index
				+ ", description=" + description + ", date=" + date + ", consumption="
				+ consumption + "]";
	}
}
