package ro.bucharest.youth.bottom_line_mw.http.api.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.ws.rs.NameBinding;

@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonArrayRequestFilter {
}
