package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.Utility;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;

@Path("utility")
@AuthorizationRequestFilter
public class UtilityResource {

	private static final Logger log = LogManager.getLogger(UtilityResource.class);

	@Context
	private SecurityContext secContext;

	@POST
	@JsonObjectRequestFilter
	@Path("address/{addressId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addUtility(final Utility utility, @PathParam("addressId") final int addressId) {
		log.debug("Received request to add utility {} for address: {}", utility.getId(), addressId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {

				Address address = AddressResource.getAddressNotNull(session, addressId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				AddressResource.checkUserIsOwnerForAddress(userId, address);

				Utility result = (Utility) session
						.createQuery("from Utility u where u.name = :name and u.address.id= :addressId")
						.setParameter("name", utility.getName()).setParameter("addressId", addressId).uniqueResult();
				if (result != null) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Utility already assigned to address").build());
				} else {
					utility.setAddress(address);
					address.getUtilities().add(utility);
					session.persist(utility);
				}
			}
		}.execute();
		log.debug("Utility persisted");

		return Response.status(Status.OK).entity("Utility was successfully added").build();
	}

	@GET
	@Path("address/{addressId}")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public List<Utility> getUtilities(@PathParam("addressId") final int addressId) {
		log.debug("Get utilities for address {}", addressId);

		return new HibernateTransaction(App.sessionFactory).new WithValue<List<Utility>>() {
			@Override
			public List<Utility> run(Session session) {
				// check address exists
				Address address = AddressResource.getAddressNotNull(session, addressId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				AddressResource.checkUserIsAssignedToAddress(userId, address);
				return address.getUtilities();
			}
		}.execute();
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Removes Utility
	 */
	public Response delete(@PathParam("id") final short utilityId) {
		log.debug("Remove utility {}", utilityId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				Utility utility = getUtilityNotNull(session, utilityId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				Address address = utility.getAddress();
				AddressResource.checkUserIsOwnerForAddress(userId, address);
				// remove utility from address also
				address.getUtilities().remove(utility);
				session.merge(address);
			}
		}.execute();
		return Response.status(Status.OK).entity(Messages.SUCCESS_UTILITY_REMOVED).build();
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@JsonObjectRequestFilter
	public Response update(@PathParam("id") final short utilityId, final Utility utility) {
		log.debug("Update utility {}", utilityId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// get utility from database
				Utility destUtility = getUtilityNotNull(session, utilityId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				AddressResource.checkUserIsOwnerForAddress(userId, destUtility.getAddress());

				// Check utility is unique for address
				Utility result = (Utility) session
						.createQuery("from Utility u where u.name= :name and u.address.id= :addressId")
						.setParameter("name", utility.getName())
						.setParameter("addressId", destUtility.getAddress().getId()).uniqueResult();
				if (result != null) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Utility already exists").build());
				}

				// update utility
				destUtility.setName(utility.getName());
				session.merge(destUtility);
			}
		}.execute();

		return Response.status(Status.OK).entity(Messages.SUCCESS_UTILITY_UPDATED).build();
	}

	public static Utility getUtilityNotNull(Session session, int utilityId) {
		Utility utility = (Utility) session.get(Utility.class, utilityId);
		if (utility == null) {
			log.warn("Utility with id {} doesn't exist.", utility);
			throw new WebApplicationException(
					Response.status(Status.NOT_FOUND).entity(Messages.WARNING_UTILITY_DOES_NOT_EXIST).build());
		}
		return utility;
	}
}
