package ro.bucharest.youth.bottom_line_mw.ws.msg;

public enum Type {
	CLIENT_REGISTRATION, TENANTS_REGISTRATION, ONLINE_USERS, ONLINE_TENANTS, MESSAGE;
}
