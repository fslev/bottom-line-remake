package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.HeatConsumption;
import ro.bucharest.youth.bottom_line_mw.entities.RoomType;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;
import ro.bucharest.youth.bottom_line_mw.utils.PatternProcessor.InvalidStringCriteria;
import ro.bucharest.youth.bottom_line_mw.utils.QueryBuilder;

@Path("hconsumption")
@AuthorizationRequestFilter
public class HeatConsumptionResource {

	private static final Logger log = LogManager.getLogger(HeatConsumptionResource.class);

	@Context
	SecurityContext secContext;

	@Path("/address/{addressId}/room/{roomId}")
	@POST
	@JsonObjectRequestFilter
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Adds a heat consumption instance inside db
	 */
	public Response addHeatConsumption(final HeatConsumption hconsumption, @PathParam("addressId") final int addressId,
			@PathParam("roomId") final int roomId) {
		log.debug("Received request to add heat consumption: {}", hconsumption);

		validateFields(hconsumption);
		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check address exists
				Address address = AddressResource.getAddressNotNull(session, addressId);
				AddressResource.checkUserIsAssignedToAddress(userId, address);
				// check room exists
				RoomType room = RoomResource.getRoomNotNull(session, roomId);

				// check heat consumption exists inside db
				HeatConsumption result = (HeatConsumption) session
						.createQuery(
								"from HeatConsumption hc where hc.date = :date and  hc.room.id = :room_type_id and hc.address.id = :address_id")
						.setParameter("date", hconsumption.getDate()).setParameter("room_type_id", roomId)
						.setParameter("address_id", addressId).uniqueResult();
				if (result != null) {
					log.warn("HeatConsumption already exists inside database: {}", hconsumption);
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Heat consumption already exists").build());
				}

				// If consumption is not defined then get previous index
				// measurement for same address, room and
				// type and calculate it
				if (hconsumption.getConsumption() == null) {
					HeatConsumption prevResult = (HeatConsumption) session
							.createQuery(
									"from HeatConsumption hc where hc.room.id = :room_type_id and hc.address.id = :address_id and hc.date < :date order by hc.date desc")
							.setParameter("date", hconsumption.getDate()).setParameter("room_type_id", roomId)
							.setParameter("address_id", addressId).setMaxResults(1).uniqueResult();
					hconsumption.setConsumption(
							prevResult != null ? hconsumption.getIndex() - prevResult.getIndex() : null);
				}

				hconsumption.setAddress(address);
				hconsumption.setRoom(room);
				session.persist(hconsumption);
			}
		}.execute();
		log.debug("Heat consumption entry persisted");

		return Response.status(Status.OK).entity("Heat consumption successfully persisted").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHConsumptionList(@QueryParam(value = "addressId") final int addressId,
			@QueryParam(value = "offset") final Integer offset, @QueryParam(value = "limit") final Integer limit,
			@QueryParam(value = "pattern") final String pattern) {

		log.debug("Get all heat consumption for addressId {} and offset {} and limit {} and pattern {}", addressId,
				offset, limit, pattern);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		final QueryBuilder queryBuilder = new QueryBuilder(pattern);
		queryBuilder.acceptField("room", "room.name").acceptField("date", "date").acceptField("index", "index_val")
				.acceptField("consumption", "consumption");

		List<HeatConsumption> list = new HibernateTransaction(
				App.sessionFactory).new WithValue<List<HeatConsumption>>() {
			@Override
			public List<HeatConsumption> run(Session session) {

				Address address = AddressResource.getAddressNotNull(session, addressId);
				AddressResource.checkUserIsAssignedToAddress(userId, address);

				StringBuilder queryString = new StringBuilder("from HeatConsumption hc where hc.address.id = ?");

				final String queryPattern;
				try {
					queryPattern = queryBuilder.build(queryString).toString();
				} catch (InvalidStringCriteria e) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
				}

				log.debug("RUNNING special query: {}", queryPattern);
				Query query = session.createQuery(queryPattern + " order by hc.date desc").setInteger(0, addressId);
				for (int i = 0; i < queryBuilder.getCriteriaList().size(); i++) {
					query.setString(i + 1, queryBuilder.getCriteriaList().get(i).value);
				}

				if (limit != null)
					query.setMaxResults(limit);
				if (offset != null)
					query.setFirstResult(offset);

				return query.list();
			}
		}.execute();

		// Get query count for pagination
		long count = new HibernateTransaction(App.sessionFactory).new WithValue<Long>() {
			@Override
			public Long run(Session session) {

				StringBuilder queryString = new StringBuilder(
						"select count(hc) from HeatConsumption hc where hc.address.id = ?");

				final String queryPattern;
				try {
					queryPattern = queryBuilder.build(queryString).toString();
				} catch (InvalidStringCriteria e) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
				}

				log.debug("RUNNING special query: {}", queryPattern);
				Query query = session.createQuery(queryPattern).setInteger(0, addressId);
				for (int i = 0; i < queryBuilder.getCriteriaList().size(); i++) {
					query.setString(i + 1, queryBuilder.getCriteriaList().get(i).value);
				}
				return (Long) query.uniqueResult();
			}
		}.execute();

		return Response.status(Status.OK).entity(list).header("count", count).build();
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Removes HeatConsumption
	 */
	public Response delete(@PathParam("id") final int hConsId) {
		log.debug("Remove heat consumption with id: {}", hConsId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				HeatConsumption hCons = getHConsumptionNotNull(session, hConsId);
				AddressResource.checkUserIsAssignedToAddress(userId, hCons.getAddress());
				session.delete(hCons);
			}
		}.execute();

		return Response.status(Status.OK).entity("Heat consumption successfully removed").build();
	}

	@PUT
	@Path("{id}/room/{roomId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@JsonObjectRequestFilter
	public Response update(@PathParam("id") final int hConsId, final HeatConsumption hCons,
			@PathParam("roomId") final int roomId) {
		log.debug("Update heat consumption with id {} ", hConsId);
		System.out.println(hCons);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// get heat consumption from database
				HeatConsumption srcHCons = getHConsumptionNotNull(session, hConsId);
				RoomType room = RoomResource.getRoomNotNull(session, roomId);
				AddressResource.checkUserIsAssignedToAddress(userId, srcHCons.getAddress());
				// update heat consumption
				srcHCons.setDate(hCons.getDate());
				srcHCons.setIndex(hCons.getIndex());
				srcHCons.setRoom(room);
				if (hCons.getConsumption() == null) {
					HeatConsumption result = (HeatConsumption) session
							.createQuery(
									"from HeatConsumption hc where hc.room.id = :room_type_id and hc.address.id = :address_id and hc.date < :date order by hc.date desc")
							.setParameter("date", hCons.getDate()).setParameter("room_type_id", roomId)
							.setParameter("address_id", srcHCons.getAddress().getId()).setMaxResults(1).uniqueResult();
					srcHCons.setConsumption(result != null ? hCons.getIndex() - result.getIndex() : null);
				} else {
					srcHCons.setConsumption(hCons.getConsumption());
				}
				srcHCons.setDescription(hCons.getDescription());
				session.merge(srcHCons);
			}
		}.execute();

		return Response.status(Status.OK).entity("Heat consumption successfully removed").build();
	}

	public static HeatConsumption getHConsumptionNotNull(Session session, int hConsumptionId) {
		HeatConsumption hconsumption = (HeatConsumption) session.get(HeatConsumption.class, hConsumptionId);
		if (hconsumption == null) {
			log.warn("Heat consumption with id {} doesn't exist.", hconsumption);
			throw new WebApplicationException(
					Response.status(Status.NOT_FOUND).entity("Heat consumption does not exist").build());
		}
		return hconsumption;
	}

	public static void removeHeatConsForRoom(RoomType room) {
		Iterator<HeatConsumption> iter = room.getAddress().getHeatConsumptionList().iterator();
		while (iter.hasNext()) {
			if (iter.next().getRoom().getId() == room.getId()) {
				iter.remove();
			}
		}
	}

	public static void validateFields(HeatConsumption hconsumption) {
		if (hconsumption.getDate() == null || hconsumption.getIndex() == null) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_INVALID_FIELDS).build());
		}
	}
}
