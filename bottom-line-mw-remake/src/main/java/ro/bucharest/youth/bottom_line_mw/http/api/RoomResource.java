package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.RoomType;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;

@Path("room")
@AuthorizationRequestFilter
public class RoomResource {

	private static final Logger log = LogManager.getLogger(RoomResource.class);

	@Context
	private SecurityContext secContext;

	@POST
	@Path("address/{addressId}")
	@JsonObjectRequestFilter
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addRoom(final RoomType room, @PathParam("addressId") final int addressId) {
		log.debug("Received request to add room: {} for addressId: {}", room, addressId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {

				Address address = AddressResource.getAddressNotNull(session, addressId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				AddressResource.checkUserIsOwnerForAddress(userId, address);

				// check room is already added for address
				RoomType roomResult = (RoomType) session
						.createQuery("from RoomType r where r.name = :name and r.address.id= :addressId")
						.setParameter("name", room.getName()).setParameter("addressId", addressId).uniqueResult();
				if (roomResult != null) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Room already assigned to address").build());
				} else {
					room.setAddress(address);
					address.getRooms().add(room);
					session.persist(room);
				}
			}
		}.execute();
		log.debug("Room persisted");

		return Response.status(Status.OK).entity("Room was successfully added").build();
	}

	@GET
	@Path("address/{addressId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<RoomType> getRooms(@PathParam("addressId") final int addressId) {
		log.debug("Get all rooms for address {}", addressId);

		return new HibernateTransaction(App.sessionFactory).new WithValue<List<RoomType>>() {

			@Override
			public List<RoomType> run(Session session) {
				// check address exists
				Address address = AddressResource.getAddressNotNull(session, addressId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				AddressResource.checkUserIsAssignedToAddress(userId, address);
				return address.getRooms();
			}
		}.execute();
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Removes room from address
	 */
	public Response deleteRoomFromAddress(@PathParam("id") final short roomId) {
		log.debug("Remove room {}", roomId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {

				RoomType room = getRoomNotNull(session, roomId);
				int userId = new LoggedUserResourceManager(secContext).getUserId();
				Address address = room.getAddress();
				AddressResource.checkUserIsOwnerForAddress(userId, address);

				// Remove water consumption
				WaterConsumptionResource.removeWaterConsForRoom(room);
				session.flush();
				// remove room from address also
				address.getRooms().remove(room);
				session.delete(room);
			}
		}.execute();
		return Response.status(Status.OK).entity(Messages.SUCCESS_ROOM_REMOVED).build();
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@JsonObjectRequestFilter
	public Response update(@PathParam("id") final short roomId, final RoomType room) {
		log.debug("Update room with id: " + roomId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// get room from database
				RoomType destRoom = getRoomNotNull(session, roomId);

				int userId = new LoggedUserResourceManager(secContext).getUserId();
				AddressResource.checkUserIsOwnerForAddress(userId, destRoom.getAddress());

				// Check room is unique for address
				RoomType result = (RoomType) session
						.createQuery("from RoomType r where r.name= :name and r.address.id= :addressId")
						.setParameter("name", room.getName()).setParameter("addressId", destRoom.getAddress().getId())
						.uniqueResult();
				if (result != null) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Room already exists").build());
				}
				// update room
				destRoom.setName(room.getName());
				session.merge(destRoom);
			}
		}.execute();

		return Response.status(Status.OK).entity(Messages.SUCCESS_ROOM_UPDATED).build();
	}

	public static RoomType getRoomNotNull(Session session, int roomId) {
		RoomType room = (RoomType) session.get(RoomType.class, roomId);
		if (room == null) {
			log.warn("Room with id {} doesn't exist.", room);
			throw new WebApplicationException(
					Response.status(Status.NOT_FOUND).entity(Messages.WARNING_ROOM_DOES_NOT_EXIST).build());
		}
		return room;
	}
}
