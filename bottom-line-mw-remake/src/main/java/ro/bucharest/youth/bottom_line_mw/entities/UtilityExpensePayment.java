package ro.bucharest.youth.bottom_line_mw.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "utilities_expense_payment")
@XmlRootElement
public class UtilityExpensePayment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "comment")
	@Length(max = 2048)
	private String comment;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date")
	private Date date;

	@OneToOne(optional = false)
	@JoinColumn(name = "utility_expense_id", referencedColumnName = "id", unique = true)
	private UtilityExpense utilityExpense;

	@ManyToOne(optional = false)
	@JoinColumn(name = "payer_id", referencedColumnName = "id")
	private Person payer;

	public UtilityExpensePayment() {
	};

	public UtilityExpensePayment(UtilityExpense utilityExpense, Person payer, String comment) {
		this.utilityExpense = utilityExpense;
		this.payer = payer;
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public UtilityExpense getUtilityExpense() {
		return utilityExpense;
	}

	public void setUtilityExpense(UtilityExpense utilityExpense) {
		this.utilityExpense = utilityExpense;
	}

	public Person getPayer() {
		return payer;
	}

	public void setPayer(Person payer) {
		this.payer = payer;
	}

	public int getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
