package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.UUID;

import javax.naming.AuthenticationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Person;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.Login;
import ro.bucharest.youth.bottom_line_mw.security.Token;
import ro.bucharest.youth.bottom_line_mw.security.TokenManager;
import ro.bucharest.youth.bottom_line_mw.security.TokenResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;
import ro.bucharest.youth.bottom_line_mw.utils.PasswordCypher;

@Path("auth")
public class AuthenticationResource {

	private static final Logger log = LogManager.getLogger(AuthenticationResource.class);

	@POST
	@Path("login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
	@JsonObjectRequestFilter
	public Token login(final Login login) {

		log.debug("Login with username: {}", login.getUsername());

		validateFields(login);

		Person person = new HibernateTransaction(App.sessionFactory).new WithValue<Person>() {
			@Override
			public Person run(Session session) {
				Person person = (Person) session
						.createQuery("from person p where p.username= :username and p.password= :password")
						.setParameter("username", login.getUsername())
						.setParameter("password", PasswordCypher.encrypt(login.getPassword())).uniqueResult();

				if (person == null) {
					log.debug("Login failed");
					throw new WebApplicationException(Response.status(Status.UNAUTHORIZED)
							.entity(Messages.WARNING_AUTHENTICATION_FAILED).entity("Login failed").build());
				}
				log.info("Login successfull {}", login.getUsername());
				return person;
			}
		}.execute();

		// Create token
		String tokenId = UUID.randomUUID().toString();
		Token token = new Token(tokenId, person.getId(), person.getUsername(),
				System.currentTimeMillis() + TokenManager.EXP_PERIOD);
		App.tokenManager.addToken(token);
		log.debug("Generated token");
		return token;
	}

	@GET
	@Path("user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Token getUserInfo(@Context ContainerRequestContext ctx) {
		Token token;
		try {
			token = new TokenResourceManager(ctx).extractToken();
		} catch (AuthenticationException e) {
			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).entity(e.getMessage()).build());
		}
		return token;
	}

	private static void validateFields(Login login) {
		if (login.getUsername() == null || login.getPassword() == null) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
					.entity(Messages.WARNING_MISSING_USERNAME + " or " + Messages.WARNING_MISSING_PASSWORD).build());
		}
	}

	@GET
	@Path("token")
	@AuthorizationRequestFilter
	public Response checkToken() {
		return Response.status(Status.OK).entity("Token valid").build();
	}

	@DELETE
	@Path("logout")
	@AuthorizationRequestFilter
	public Response logout(@Context ContainerRequestContext ctx) {
		try {
			new TokenResourceManager(ctx).removeToken();
		} catch (AuthenticationException e) {
			throw new WebApplicationException(Response.status(Status.UNAUTHORIZED).entity(e.getMessage()).build());
		}
		return Response.status(Status.OK).entity("Token removed").build();
	}
}
