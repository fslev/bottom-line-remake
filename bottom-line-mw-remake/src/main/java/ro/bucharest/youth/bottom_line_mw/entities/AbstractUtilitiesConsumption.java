package ro.bucharest.youth.bottom_line_mw.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

@XmlRootElement
@MappedSuperclass
public abstract class AbstractUtilitiesConsumption implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	protected int id;

	@Column(name = "index_val", nullable = false)
	protected Integer index;

	@Column(name = "consumption")
	protected Integer consumption = 0;

	@Column(name = "description")
	@Length(max = 2048)
	protected String description;

	@Column(name = "date", nullable = false)
	protected Long date;

	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	protected Address address;

	public int getId() {
		return id;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Integer getConsumption() {
		return consumption;
	}

	public void setConsumption(Integer consumption) {
		this.consumption = consumption;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "AbstractUtilitiesConsumption [id=" + id + ", index=" + index + ", consumption=" + consumption
				+ ", description=" + description + ", date=" + date + "]";
	}

}
