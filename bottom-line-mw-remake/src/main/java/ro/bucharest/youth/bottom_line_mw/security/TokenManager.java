package ro.bucharest.youth.bottom_line_mw.security;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TokenManager {

	private static final Logger log = LogManager.getLogger(TokenManager.class);

	public static final long EXP_PERIOD = TimeUnit.HOURS.toMillis(24);

	private Set<Token> tokens = new HashSet<Token>();

	/**
	 * Looks for a valid (not-expired) token
	 */
	public Token getToken(String tokenUUID) {

		log.debug("Looking for token id {} through a list of tokens of size: {}", tokenUUID, tokens.size());

		for (Token token : tokens) {
			if (token.getUuid().equals(tokenUUID)) {
				if (token.getExpDate() < System.currentTimeMillis()) {
					log.warn("Token expired");
					return null;
				}
				log.debug("Success. Found token");
				return token;
			}
		}
		log.debug("No token found");
		return null;
	}

	/**
	 * Looks for a valid (not-expired) token for user id
	 */
	public Token getToken(int userId) {
		log.debug("Looking for token by user id {}", userId);
		for (Token token : tokens) {
			if (token.getUserId() == userId) {
				return token;
			}
		}
		log.debug("No token found");
		return null;
	}

	public void addToken(Token token) {
		this.tokens.remove(token);
		this.tokens.add(token);
	}

	public void removeToken(Token token) {
		this.tokens.remove(token);
	}

	public Set<Token> getTokens() {
		return this.tokens;
	}
}
