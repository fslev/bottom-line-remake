package ro.bucharest.youth.bottom_line_mw.ws.msg;

public class SimpleMessage {
	private Type type = Type.MESSAGE;
	private int srcUserId;
	private int destUserId;
	private String body;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getSrcUserId() {
		return srcUserId;
	}

	public void setSrcUserId(int srcUserId) {
		this.srcUserId = srcUserId;
	}

	public int getDestUserId() {
		return destUserId;
	}

	public void setDestUserId(int destUserId) {
		this.destUserId = destUserId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
