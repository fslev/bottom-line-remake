package ro.bucharest.youth.bottom_line_mw.security;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Token {
	private String uuid;
	private int userId;
	private String username;
	private long expDate;

	public Token() {

	}

	public Token(String uuid, int userId, String username, long expDate) {
		this.uuid = uuid;
		this.userId = userId;
		this.username = username;
		this.expDate = expDate;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getExpDate() {
		return expDate;
	}

	public void setExpDate(long expDate) {
		this.expDate = expDate;
	}

	@Override
	public int hashCode() {
		return userId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Token other = (Token) obj;
		if (userId != other.userId)
			return false;
		return true;
	}

}
