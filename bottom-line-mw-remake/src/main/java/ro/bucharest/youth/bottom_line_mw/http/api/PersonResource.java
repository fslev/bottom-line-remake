package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.Request;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Person;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;
import ro.bucharest.youth.bottom_line_mw.utils.PasswordCypher;

@Path("person")
public class PersonResource {

	private static final Logger log = LogManager.getLogger(PersonResource.class);
	private static ConcurrentMap<String, Long> clientRequests = new ConcurrentHashMap<>();

	@Context
	private SecurityContext secContext;
	@Context
	Request req;

	@POST
	@JsonObjectRequestFilter
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addPerson(final Person person) {
		// Some punks once tried to take advantage by this call, by power
		// overwhelming it
		// TO DO: move it to a filter.Some other calls may need this
		String host = req.getRemoteHost();
		long currTime = System.currentTimeMillis();
		if (iAmRobot(host, currTime)) {
			throw new WebApplicationException(Response.status(Status.NOT_ACCEPTABLE)
					.entity("What are you doing human ?! Are you trying to power overwhelm me ?").build());
		}
		clientRequests.put(host, currTime);
		log.debug("Received request to add person: {} from", person, host);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {

				validateFields(person);

				// check person exists inside db
				Person result = (Person) session
						.createQuery("from person p where p.username = :username or p.email = :email")
						.setParameter("username", person.getUsername()).setParameter("email", person.getEmail())
						.uniqueResult();
				if (result != null) {
					log.warn("Person already exists inside database: {}", person);
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_PERSON_ALREADY_EXISTS).build());
				}
				// Encrypt password
				person.setPassword(PasswordCypher.encrypt(person.getPassword()));
				session.persist(person);
			}
		}.execute();
		log.debug("Person persisted");

		return Response.status(Status.OK).entity("Person was successfully added").build();
	}

	@GET
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	@AuthorizationRequestFilter
	public List<Person> getPersons(@QueryParam("name") final String name) {
		log.debug("Get persons with name like {}", name);

		return new HibernateTransaction(App.sessionFactory).new WithValue<List<Person>>() {
			@Override
			public List<Person> run(Session session) {
				return session.createQuery("from person p where p.name like ? or p.username like ?")
						.setParameter(0, "%" + name + "%").setParameter(1, "%" + name + "%").setMaxResults(50).list();
			}
		}.execute();
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	@AuthorizationRequestFilter
	public Person getPerson(@PathParam("id") final int personId) {
		log.debug("Get person by id:" + personId);

		return new HibernateTransaction(App.sessionFactory).new WithValue<Person>() {
			@Override
			public Person run(Session session) {
				return getPersonNotNull(session, personId);
			}
		}.execute();
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Remove person
	 */
	@AuthorizationRequestFilter
	public Response delete(@PathParam("id") final short personId) {
		log.debug("Remove person with id: {}", personId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				Person person = getPersonNotNull(session, personId);
				// first, remove person from all addresses
				// for (Address address : person.getAddresses()) {
				// address.removePerson(person);
				// }
				// now that the person is "free", you can remove it
				session.delete(person);
			}
		}.execute();

		return Response.status(Status.OK).entity(Messages.SUCCESS_PERSON_REMOVED).build();
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@JsonObjectRequestFilter
	@AuthorizationRequestFilter
	public Response update(@PathParam("id") final short personId, final Person person,
			@QueryParam(value = "pwd") final String currPassword) {
		log.debug("Update person with id: " + personId);

		// Users can edit only themselves
		final int userId = new LoggedUserResourceManager(secContext).getUserId();
		if (userId != personId) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
					.entity("You are not allowed to edit users other than yourself").build());
		}

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {

				// get person from database
				Person srcPerson = (Person) session
						.createQuery("from person p where p.id= :id and p.password= :password")
						.setParameter("id", userId).setParameter("password", PasswordCypher.encrypt(currPassword))
						.uniqueResult();

				if (srcPerson == null) {
					throw new WebApplicationException(
							Response.status(Status.FORBIDDEN).entity("User password not matched").build());
				}

				// update person
				srcPerson.setName(person.getName());
				srcPerson.setUsername(person.getUsername());
				srcPerson.setEmail(person.getEmail());
				if (person.getPassword() != null) {
					srcPerson.setPassword(PasswordCypher.encrypt(person.getPassword()));
				} else {
					srcPerson.setPassword(PasswordCypher.encrypt(currPassword));
				}
				session.merge(srcPerson);
			}
		}.execute();

		return Response.status(Status.OK).entity(Messages.SUCCESS_PERSON_UPDATED).build();

	}

	public static Person getPersonNotNull(Session session, int personId) {
		Person person = (Person) session.get(Person.class, personId);
		if (person == null) {
			log.warn("Person with id {} doesn't exist.", personId);
			throw new WebApplicationException(
					Response.status(Status.NOT_FOUND).entity(Messages.WARNING_PERSON_DOES_NOT_EXIST).build());
		}
		return person;
	}

	private static void validateFields(Person person) {
		// validate name
		if (person.getName() == null) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_MISSING_NAME).build());
		}
		// validate username
		if (person.getUsername() == null) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_MISSING_USERNAME).build());
		}

		// validate password
		if (person.getPassword() == null) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_MISSING_PASSWORD).build());
		}

		// validate email
		if (person.getEmail() == null || !isValidEmail(person.getEmail())) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_INVALID_EMAIL).build());
		}
	}

	public boolean iAmRobot(String host, Long time) {
		Long prevTime = clientRequests.get(host);
		if (prevTime != null && time - prevTime < TimeUnit.SECONDS.toMillis(10)) {
			return true;
		}
		return false;
	}

	public static boolean isValidEmail(String email) {
		String pattern = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
		return email.matches(pattern);
	}
}
