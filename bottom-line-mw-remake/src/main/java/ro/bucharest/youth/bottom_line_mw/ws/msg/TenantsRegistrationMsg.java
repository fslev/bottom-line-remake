package ro.bucharest.youth.bottom_line_mw.ws.msg;

import java.util.List;

public class TenantsRegistrationMsg {
	private Type type;
	private int userId;
	private List<Integer> tenants;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<Integer> getTenants() {
		return tenants;
	}

	public void setTenants(List<Integer> tenants) {
		this.tenants = tenants;
	}

}