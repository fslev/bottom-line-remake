package ro.bucharest.youth.bottom_line_mw.http.api.filters;

import java.io.IOException;

import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonArrayRequestFilter;

@Provider
@JsonArrayRequestFilter
public class ResourceJsonArrayRequestFilter extends ResourceJsonRequestFilter {

	@Override
	protected boolean isEntityValidJSON() {
		try {
			new ObjectMapper().readValue(this.entity, ArrayNode.class);
		} catch (JsonParseException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	@Override
	protected String getJSONErrorMessage() {
		return "Invalid JSON ARRAY !";
	}
}
