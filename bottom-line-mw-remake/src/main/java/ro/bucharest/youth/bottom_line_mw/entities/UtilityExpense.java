package ro.bucharest.youth.bottom_line_mw.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "utilities_expense", uniqueConstraints = @UniqueConstraint(columnNames = { "date", "address_id",
		"utility_id" }) )
@XmlRootElement
public class UtilityExpense implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false)
	private int id;

	@Column(name = "value", nullable = false, scale = 3, precision = 2)
	private double value;

	@Column(name = "date", nullable = false)
	private long date;

	@Column(name = "description")
	@Length(max = 2048)
	protected String description;

	@ManyToOne(optional = false)
	@JoinColumn(name = "utility_id", referencedColumnName = "id")
	private Utility utility;

	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(name = "address_id", referencedColumnName = "id")
	private Address address;

	public UtilityExpense() {
	};

	public int getId() {
		return id;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Utility getUtility() {
		return utility;
	}

	public void setUtility(Utility utility) {
		this.utility = utility;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

	@Override
	public String toString() {
		return "UtilityExpense [value=" + value + ", date=" + date + ", description=" + description + ", utility="
				+ utility + "]";
	}
}
