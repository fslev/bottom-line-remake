package ro.bucharest.youth.bottom_line_mw.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "address", uniqueConstraints = @UniqueConstraint(columnNames = { "country", "city", "street" }) )
@XmlRootElement
public class Address implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, insertable = false)
	public int id;

	@Column(name = "country", nullable = false)
	@Length(max = 100)
	private String country;

	@Column(name = "city", nullable = false)
	@Length(max = 100)
	private String city;

	@Column(name = "street", nullable = false)
	@Length(max = 255)
	private String street;

	@OneToMany(mappedBy = "address", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
	@Fetch(FetchMode.SELECT)
	private List<RoomType> rooms = new ArrayList<RoomType>();

	@JsonIgnore
	@OneToMany(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@Fetch(FetchMode.SELECT)
	private List<AddressPerson> addressesPersons = new ArrayList<AddressPerson>();

	@OneToMany(mappedBy = "address", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER, orphanRemoval = true)
	@Fetch(FetchMode.SELECT)
	private List<Utility> utilities = new ArrayList<Utility>();

	@JsonIgnore
	@OneToMany(mappedBy = "address", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<WaterConsumption> waterConsumptionList = new ArrayList<WaterConsumption>();

	@JsonIgnore
	@OneToMany(mappedBy = "address", cascade = CascadeType.REMOVE)
	private List<EnergyConsumption> energyConsumptionList = new ArrayList<EnergyConsumption>();

	@JsonIgnore
	@OneToMany(mappedBy = "address", cascade = CascadeType.ALL)
	private List<HeatConsumption> heatConsumptionList = new ArrayList<HeatConsumption>();

	@JsonIgnore
	@OneToMany(mappedBy = "address", cascade = CascadeType.ALL)
	private List<UtilityExpense> utilitiesExpenses = new ArrayList<UtilityExpense>();

	public Address() {

	}

	public Address(String country, String city, String street) {
		this.country = country;
		this.city = city;
		this.street = street;
	}

	public int getId() {
		return id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public List<RoomType> getRooms() {
		return rooms;
	}

	public void setRooms(List<RoomType> rooms) {
		this.rooms = rooms;
	}

	public void addRoom(RoomType room) {
		this.rooms.add(room);
	}

	public void removeRoom(RoomType room) {
		Iterator<RoomType> it = rooms.iterator();
		while (it.hasNext()) {
			RoomType r = it.next();
			if (r.equals(room))
				it.remove();
		}
	}

	public List<AddressPerson> getAddressesPersons() {
		return addressesPersons;
	}

	@JsonProperty("assignedPersons")
	public Set<Person> getAssignedPersons() {
		Set<Person> persons = new HashSet<Person>();
		for (AddressPerson addrPers : addressesPersons) {
			persons.add(addrPers.getPerson());
		}
		return persons;
	}

	@JsonProperty("owners")
	public Set<Person> getOwners() {
		Set<Person> owners = new HashSet<Person>();
		for (AddressPerson addrPers : addressesPersons) {
			if (addrPers.isOwner()) {
				owners.add(addrPers.getPerson());
			}
		}
		return owners;
	}

	public void setAddressesPersons(List<AddressPerson> addressesPersons) {
		this.addressesPersons = addressesPersons;
	}

	public List<Utility> getUtilities() {
		return utilities;
	}

	public void setUtilities(List<Utility> utilities) {
		this.utilities = utilities;
	}

	public void addUtility(Utility utility) {
		this.utilities.add(utility);
	}

	public void removeUtility(Utility utility) {
		Iterator<Utility> it = utilities.iterator();
		while (it.hasNext()) {
			Utility u = it.next();
			if (u.equals(utility))
				it.remove();
		}
	}

	public List<WaterConsumption> getWaterConsumptionList() {
		return waterConsumptionList;
	}

	public void addWaterConsumption(WaterConsumption waterConsumption) {
		this.waterConsumptionList.add(waterConsumption);
	}

	public List<HeatConsumption> getHeatConsumptionList() {
		return heatConsumptionList;
	}

	public void addHeatConsumption(HeatConsumption heatConsumption) {
		this.heatConsumptionList.add(heatConsumption);
	}

	public List<EnergyConsumption> getEnergyConsumptionList() {
		return energyConsumptionList;
	}

	public void addEnergyConsumption(EnergyConsumption energyConsumption) {
		this.energyConsumptionList.add(energyConsumption);
	}

	public List<UtilityExpense> getUtilitiesExpenses() {
		return utilitiesExpenses;
	}

	public void setUtilitiesExpenses(List<UtilityExpense> utilitiesExpenses) {
		this.utilitiesExpenses = utilitiesExpenses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", country=" + country + ", city=" + city + ", street=" + street + "]";
	}

}
