package ro.bucharest.youth.bottom_line_mw.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ro.bucharest.youth.bottom_line_mw.utils.PatternProcessor.Criteria;
import ro.bucharest.youth.bottom_line_mw.utils.PatternProcessor.InvalidStringCriteria;

public class QueryBuilder {

	private List<Criteria> criteriaList = new ArrayList<Criteria>();
	private Map<String, String> acceptedFields = new HashMap<String, String>();
	private String pattern;

	public QueryBuilder(String pattern) {
		this.pattern = pattern;
	}

	public QueryBuilder acceptField(String alias, String field) {
		this.acceptedFields.put(alias, field);
		return this;
	}

	private void processPattern() throws InvalidStringCriteria {
		if (pattern != null && !pattern.isEmpty()) {
			PatternProcessor pp = new PatternProcessor();
			this.criteriaList = pp.process(pattern);
		}
	}

	private void validateCriteriaList() throws InvalidStringCriteria {
		for (Criteria criteria : criteriaList) {
			if (!isFieldAccepted(criteria.key)) {
				throw new PatternProcessor.InvalidStringCriteria("Field is not accepted");
			}
		}
	}

	private boolean isFieldAccepted(String alias) {
		for (Entry<String, String> entry : acceptedFields.entrySet()) {
			if (alias.equals(entry.getKey())) {
				return true;
			}
		}
		return false;
	}

	public List<Criteria> getCriteriaList() {
		return this.criteriaList;
	}

	public String build(StringBuilder query) throws InvalidStringCriteria {

		processPattern();
		validateCriteriaList();

		if (criteriaList.isEmpty())
			return query.toString();

		query.append(" and ");
		for (Criteria criteria : criteriaList) {
			if (criteria.conjuction != null) {
				query.append(criteria.conjuction).append(" ");
			}
			query.append(acceptedFields.get(criteria.key)).append(" ");
			query.append(criteria.operator).append(" ");
			query.append("?").append(" ");
		}
		return query.toString();
	}

	public static void main(String[] args) throws InvalidStringCriteria {
		System.out
				.println(new QueryBuilder(
						"type= COLD or type=HOT and room =Bucatarie and date > 142")
						.build(new StringBuilder(
								"from WaterConsumption wc where wc.address.id= :addressId")));
	}
}
