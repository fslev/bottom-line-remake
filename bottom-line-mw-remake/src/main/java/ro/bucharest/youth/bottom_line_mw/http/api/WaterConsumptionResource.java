package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.RoomType;
import ro.bucharest.youth.bottom_line_mw.entities.WaterConsumption;
import ro.bucharest.youth.bottom_line_mw.entities.WaterConsumption.Type;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;
import ro.bucharest.youth.bottom_line_mw.utils.PatternProcessor.InvalidStringCriteria;
import ro.bucharest.youth.bottom_line_mw.utils.QueryBuilder;

@Path("wconsumption")
@AuthorizationRequestFilter
public class WaterConsumptionResource {

	private static final Logger log = LogManager.getLogger(WaterConsumptionResource.class);

	@Context
	SecurityContext secContext;

	@Path("/address/{addressId}/room/{roomId}")
	@POST
	@JsonObjectRequestFilter
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Adds a water consumption instance inside db
	 */
	public Response addWaterConsumption(final WaterConsumption wconsumption,
			@PathParam("addressId") final int addressId, @PathParam("roomId") final int roomId) {
		log.debug("Received request to add water consumption: {}", wconsumption);

		validateFields(wconsumption);
		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check address exists
				Address address = AddressResource.getAddressNotNull(session, addressId);
				AddressResource.checkUserIsAssignedToAddress(userId, address);
				// check room exists
				RoomType room = RoomResource.getRoomNotNull(session, roomId);

				// check water consumption exists inside db
				WaterConsumption result = (WaterConsumption) session
						.createQuery(
								"from WaterConsumption wc where wc.date = :date and wc.type = :type and wc.room.id = :room_type_id and wc.address.id = :address_id")
						.setParameter("date", wconsumption.getDate()).setParameter("type", wconsumption.getType())
						.setParameter("room_type_id", roomId).setParameter("address_id", addressId).uniqueResult();
				if (result != null) {
					log.warn("WaterConsumption already exists inside database: {}", wconsumption);
					throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
							.entity(Messages.WARNING_WCONSUMPTION_ALREADY_EXISTS).build());
				}

				// If consumption is not defined then get previous index
				// measurement for same address, room and
				// type and calculate it
				if (wconsumption.getConsumption() == null) {
					WaterConsumption prevResult = (WaterConsumption) session
							.createQuery(
									"from WaterConsumption wc where wc.type = :type and wc.room.id = :room_type_id and wc.address.id = :address_id and wc.date < :date order by wc.date desc")
							.setParameter("date", wconsumption.getDate()).setParameter("type", wconsumption.getType())
							.setParameter("room_type_id", roomId).setParameter("address_id", addressId).setMaxResults(1)
							.uniqueResult();
					wconsumption.setConsumption(
							prevResult != null ? wconsumption.getIndex() - prevResult.getIndex() : null);
				}

				wconsumption.setAddress(address);
				wconsumption.setRoom(room);
				session.persist(wconsumption);
			}
		}.execute();
		log.debug("Water consumption entry persisted");

		return Response.status(Status.OK).entity(Messages.SUCCESS_WCONSUMPTION_PERSIST).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWConsumptionList(@QueryParam(value = "addressId") final int addressId,
			@QueryParam(value = "offset") final Integer offset, @QueryParam(value = "limit") final Integer limit,
			@QueryParam(value = "pattern") final String pattern) {

		log.debug("Get all water consumption for addressId {} and offset {} and limit {} and pattern {}", addressId,
				offset, limit, pattern);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		final QueryBuilder queryBuilder = new QueryBuilder(pattern);
		queryBuilder.acceptField("type", "type").acceptField("room", "room.name").acceptField("date", "date")
				.acceptField("index", "index_val").acceptField("consumption", "consumption");

		List<WaterConsumption> list = new HibernateTransaction(
				App.sessionFactory).new WithValue<List<WaterConsumption>>() {
			@Override
			public List<WaterConsumption> run(Session session) {

				Address address = AddressResource.getAddressNotNull(session, addressId);
				AddressResource.checkUserIsAssignedToAddress(userId, address);

				StringBuilder queryString = new StringBuilder("from WaterConsumption wc where wc.address.id = ?");

				final String queryPattern;
				try {
					queryPattern = queryBuilder.build(queryString).toString();
				} catch (InvalidStringCriteria e) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
				}

				log.debug("RUNNING special query: {}", queryPattern);
				Query query = session.createQuery(queryPattern + " order by wc.date desc").setInteger(0, addressId);
				for (int i = 0; i < queryBuilder.getCriteriaList().size(); i++) {
					query.setString(i + 1, queryBuilder.getCriteriaList().get(i).value);
				}

				if (limit != null)
					query.setMaxResults(limit);
				if (offset != null)
					query.setFirstResult(offset);

				return query.list();
			}
		}.execute();

		// Get query count for pagination
		long count = new HibernateTransaction(App.sessionFactory).new WithValue<Long>() {
			@Override
			public Long run(Session session) {

				StringBuilder queryString = new StringBuilder(
						"select count(wc) from WaterConsumption wc where wc.address.id = ?");

				final String queryPattern;
				try {
					queryPattern = queryBuilder.build(queryString).toString();
				} catch (InvalidStringCriteria e) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
				}

				log.debug("RUNNING special query: {}", queryPattern);
				Query query = session.createQuery(queryPattern).setInteger(0, addressId);
				for (int i = 0; i < queryBuilder.getCriteriaList().size(); i++) {
					query.setString(i + 1, queryBuilder.getCriteriaList().get(i).value);
				}
				return (Long) query.uniqueResult();
			}
		}.execute();

		return Response.status(Status.OK).entity(list).header("count", count).build();
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Removes WaterConsumption
	 */
	public Response delete(@PathParam("id") final int wConsId) {
		log.debug("Remove water consumption with id: {}", wConsId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				WaterConsumption wCons = getWConsumptionNotNull(session, wConsId);
				AddressResource.checkUserIsAssignedToAddress(userId, wCons.getAddress());
				session.delete(wCons);
			}
		}.execute();

		return Response.status(Status.OK).entity(Messages.SUCCESS_WCONSUMPTION_REMOVED).build();
	}

	@PUT
	@Path("{id}/room/{roomId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@JsonObjectRequestFilter
	public Response update(@PathParam("id") final int wConsId, final WaterConsumption wCons,
			@PathParam("roomId") final int roomId) {
		log.debug("Update water consumption with id {} ", wConsId);
		System.out.println(wCons);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// get water consumption from database
				WaterConsumption srcWCons = getWConsumptionNotNull(session, wConsId);
				RoomType room = RoomResource.getRoomNotNull(session, roomId);
				AddressResource.checkUserIsAssignedToAddress(userId, srcWCons.getAddress());
				// update water consumption
				srcWCons.setDate(wCons.getDate());
				srcWCons.setIndex(wCons.getIndex());
				srcWCons.setRoom(room);
				srcWCons.setType(wCons.getType());

				if (wCons.getConsumption() == null) {
					WaterConsumption result = (WaterConsumption) session
							.createQuery(
									"from WaterConsumption wc where wc.type = :type and wc.room.id = :room_type_id and wc.address.id = :address_id and wc.date < :date order by wc.date desc")
							.setParameter("date", wCons.getDate()).setParameter("type", wCons.getType())
							.setParameter("room_type_id", roomId)
							.setParameter("address_id", srcWCons.getAddress().getId()).setMaxResults(1).uniqueResult();
					srcWCons.setConsumption(result != null ? wCons.getIndex() - result.getIndex() : null);
				} else {
					srcWCons.setConsumption(wCons.getConsumption());
				}

				srcWCons.setDescription(wCons.getDescription());
				session.merge(srcWCons);
			}
		}.execute();

		return Response.status(Status.OK).entity(Messages.SUCCESS_WCONSUMPTION_UPDATED).build();
	}

	public static WaterConsumption getWConsumptionNotNull(Session session, int wConsumptionId) {
		WaterConsumption wconsumption = (WaterConsumption) session.get(WaterConsumption.class, wConsumptionId);
		if (wconsumption == null) {
			log.warn("Water consumption with id {} doesn't exist.", wconsumption);
			throw new WebApplicationException(
					Response.status(Status.NOT_FOUND).entity(Messages.WARNING_WCONSUMPTION_DOES_NOT_EXIST).build());
		}
		return wconsumption;
	}

	public static void removeWaterConsForRoom(RoomType room) {
		Iterator<WaterConsumption> iter = room.getAddress().getWaterConsumptionList().iterator();
		while (iter.hasNext()) {
			if (iter.next().getRoom().getId() == room.getId()) {
				iter.remove();
			}
		}
	}

	public static void validateFields(WaterConsumption wconsumption) {
		if ((wconsumption.getDate() == null || wconsumption.getIndex() == null || wconsumption.getType() == null)
				|| (!wconsumption.getType().equals(Type.HOT) && !wconsumption.getType().equals(Type.COLD))) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_INVALID_FIELDS).build());
		}
	}
}
