package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.AddressPerson;
import ro.bucharest.youth.bottom_line_mw.entities.Person;
import ro.bucharest.youth.bottom_line_mw.entities.RoomType;
import ro.bucharest.youth.bottom_line_mw.entities.Utility;
import ro.bucharest.youth.bottom_line_mw.entities.UtilityExpense;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonArrayRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;

@Path("address")
@AuthorizationRequestFilter
public class AddressResource {

	private static final Logger log = LogManager.getLogger(AddressResource.class);

	@Context
	private SecurityContext secContext;

	@POST
	@JsonObjectRequestFilter
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response addAddress(final Address address) {
		log.debug("Received request to add address: {}", address);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check address exists inside db
				Address result = (Address) session
						.createQuery("from Address a where a.country = ? and a.city = ? and a.street =?")
						.setParameter(0, address.getCountry()).setParameter(1, address.getCity())
						.setParameter(2, address.getStreet()).uniqueResult();
				if (result != null) {
					log.warn("Address already exists inside database: " + address);
					throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
							.entity(Messages.WARNING_ADDRESS_ALREADY_EXISTS).build());
				}

				// Current logged user is also the owner of the address he
				// persisted
				Person owner = PersonResource.getPersonNotNull(session, userId);
				AddressPerson addressPerson = new AddressPerson(address, owner, true);
				address.getAddressesPersons().add(addressPerson);
				session.persist(address);
			}
		}.execute();
		log.debug("Address persisted");

		return Response.status(Status.OK).entity("Address was successfully added").build();
	}

	@PUT
	@Path("{id}/owner/{ownerId}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response addOwnerForAddress(@PathParam("id") final int addressId, @PathParam("ownerId") final int personId) {
		log.debug("Received request to add ownerId: {} for addressId: {}", personId, addressId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check address exists inside db
				Address address = getAddressNotNull(session, addressId);
				checkUserIsOwnerForAddress(userId, address);
				if (isUserOwnerForAddress(personId, address)) {
					log.warn("Person with id {} is already owner for address with id {}: ", personId, addressId);
					throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
							.entity(Messages.WARNING_USER_ALREADY_OWNER_FOR_ADDRESS).build());
				}
				Person person = PersonResource.getPersonNotNull(session, personId);
				makeUserOwnerForAddress(person, address);
				session.merge(address);
			}
		}.execute();
		log.debug("Owner set for address");

		return Response.status(Status.OK).entity("Owner was successfully added for address").build();
	}

	@DELETE
	@Path("{id}/owner/{ownerId}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response removeOwnerFromAddress(@PathParam("id") final int addressId,
			@PathParam("ownerId") final int personId) {
		log.debug("Received request to remove owner {} for address {}", personId, addressId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check address exists inside db
				Address address = getAddressNotNull(session, addressId);
				checkUserIsOwnerForAddress(userId, address);
				Person person = PersonResource.getPersonNotNull(session, personId);
				if (!removeUserOwnerFromAddress(person, address)) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("User is not owner for address").build());
				}
				session.merge(address);
			}
		}.execute();
		log.debug("Owner removed from address");

		return Response.status(Status.OK).entity("Owner was successfully removed from address").build();
	}

	@GET
	@Path("{id}")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Address getAddress(@PathParam("id") final int addressId) {
		log.debug("Get address by id: {}", addressId);

		return new HibernateTransaction(App.sessionFactory).new WithValue<Address>() {
			@Override
			public Address run(Session session) {
				return getAddressNotNull(session, addressId);
			}
		}.execute();
	}

	@GET
	@Path("/user/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Address> getAddresses(@PathParam("userId") final int userId) {

		log.debug("Get all addresses for user {}", userId);
		int loggedUserId = new LoggedUserResourceManager(secContext).getUserId();
		log.debug("Current logged user id: " + loggedUserId);

		if (loggedUserId != userId)
			throw new WebApplicationException(
					Response.status(Status.UNAUTHORIZED).entity(Messages.WARNING_USER_HAS_NO_RIGHTS).build());

		return new HibernateTransaction(App.sessionFactory).new WithValue<List<Address>>() {
			@Override
			public List<Address> run(Session session) {
				return session
						.createQuery(
								"select addr from Address addr inner join addr.addressesPersons prs where prs.person.id = :personId")
						.setParameter("personId", userId).list();
			}
		}.execute();
	}

	@GET
	@Path("{id}/expenses")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public List<UtilityExpense> getUtilitiesExpenses(@PathParam("id") final int addressId) {
		log.debug("Received request to retrieve all utilities expenses for address with id {}", addressId);

		return new HibernateTransaction(App.sessionFactory).new WithValue<List<UtilityExpense>>() {
			@Override
			public List<UtilityExpense> run(Session session) {
				Address address = getAddressNotNull(session, addressId);
				List<UtilityExpense> expenses = new ArrayList<UtilityExpense>();
				for (UtilityExpense u : address.getUtilitiesExpenses()) {
					expenses.add(u);
				}
				return expenses;
			}
		}.execute();
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response delete(@PathParam("id") final int addressId) {
		log.debug("Remove address with id: {}", addressId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {

				Address address = getAddressNotNull(session, addressId);
				checkUserIsOwnerForAddress(userId, address);
				// session.createQuery("delete from AddressPerson ap where
				// ap.address.id= :addressId")
				// .setParameter("addressId", addressId).executeUpdate();
				// session.createQuery(
				// "delete from AddressInvitation ai where ai.address.id=
				// :addressId")
				// .setParameter("addressId", addressId).executeUpdate();
				// session.createQuery("delete from RoomType r where
				// r.address.id= :addressId")
				// .setParameter("addressId", addressId).executeUpdate();
				// session.createQuery("delete from Utility u where
				// u.address.id= :addressId")
				// .setParameter("addressId", addressId).executeUpdate();
				// session.createQuery("delete from Address a where a.id=
				// :addressId")
				// .setParameter("addressId", addressId).executeUpdate();
				session.createQuery("delete from AddressInvitation ai where ai.address.id= :addressId")
						.setParameter("addressId", addressId).executeUpdate();
				session.delete(address);
			}
		}.execute();

		log.debug("Remove address with id: {} successfully removed", addressId);
		return Response.status(Status.OK).entity(Messages.SUCCESS_ADDRES_REMOVED).build();
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@JsonObjectRequestFilter
	public Response update(@PathParam("id") final int addressId, final Address address) {
		log.debug("Update address with id {}", addressId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// get address from database
				Address srcAddress = getAddressNotNull(session, addressId);
				checkUserIsOwnerForAddress(userId, srcAddress);
				// Check for duplicates
				Address duplicate = (Address) session
						.createQuery("from Address a where a.country= :country and a.city= :city and a.street= :street")
						.setParameter("country", address.getCountry()).setParameter("city", address.getCity())
						.setParameter("street", address.getStreet()).uniqueResult();
				if (duplicate != null) {
					throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
							.entity(Messages.WARNING_ADDRESS_ALREADY_EXISTS).build());
				}
				// update address
				srcAddress.setCountry(address.getCountry());
				srcAddress.setCity(address.getCity());
				srcAddress.setStreet(address.getStreet());
				session.merge(srcAddress);
			}
		}.execute();

		return Response.status(Status.OK).entity(Messages.SUCCESS_ADDRESS_UPDATED).build();
	}

	@DELETE
	@Path("{id}/person/{personId}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response unassignPersonFromAddress(@PathParam("id") final int addressId,
			@PathParam("personId") final int personId) {
		log.debug("Received request to unassign person {} from address {}", personId, addressId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		// if (userId == personId) {
		// throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
		// .entity("What is yours is yours").build());
		// }

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				Address address = getAddressNotNull(session, addressId);
				if (userId != personId) {
					checkUserIsOwnerForAddress(userId, address);
				}
				removeUserFromAddress(personId, address);
				session.merge(address);
			}
		}.execute();
		log.info("Person {} unassigned from address {}", personId, addressId);
		return Response.status(Status.OK).entity(Messages.SUCCESS_PERSONS_UNASSIGNED_FROM_ADDRESS).build();
	}

	@POST
	@Path("{id}/rooms")
	@Produces(MediaType.TEXT_PLAIN)
	@JsonArrayRequestFilter
	public Response assignRoomsToAddress(@PathParam("id") final int addressId, final List<Short> roomIds) {
		log.debug("Received request to assign rooms with ids: {} to address with id: {}", roomIds, addressId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check rooms exists inside db
				List<RoomType> roomsToAssign = new ArrayList<RoomType>();
				for (Short roomId : roomIds) {
					roomsToAssign.add(RoomResource.getRoomNotNull(session, roomId));
				}
				// check address exists inside db
				Address address = AddressResource.getAddressNotNull(session, addressId);
				// assign rooms
				address.setRooms(roomsToAssign);
				session.merge(address);
			}
		}.execute();
		log.info("Rooms with ids {} assigned to address with {}", roomIds, addressId);
		return Response.status(Status.OK).entity(Messages.SUCCESS_ROOMS_ASSIGNED_TO_ADDRESS).build();
	}

	@POST
	@Path("{id}/utilities")
	@Produces(MediaType.TEXT_PLAIN)
	@JsonArrayRequestFilter
	public Response assignUtilitiesToAddress(@PathParam("id") final int addressId, final List<Short> utilityIds) {
		log.debug("Received request to assign utilities with ids: {} to address with id: {}", utilityIds, addressId);

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check utilities exists inside db
				List<Utility> utilitiesToAssign = new ArrayList<Utility>();
				for (Short utilityId : utilityIds) {
					utilitiesToAssign.add(UtilityResource.getUtilityNotNull(session, utilityId));
				}
				// check address exists inside db
				Address address = AddressResource.getAddressNotNull(session, addressId);
				// assign utilities
				address.setUtilities(utilitiesToAssign);
				session.merge(address);
			}
		}.execute();
		log.info("Utilities with ids {} assigned to address with {}", utilityIds, addressId);
		return Response.status(Status.OK).entity(Messages.SUCCESS_UTILITIES_ASSIGNED_TO_ADDRESS).build();
	}

	public static void checkUserIsOwnerForAddress(int userId, Address address) {
		if (!isUserOwnerForAddress(userId, address)) {
			log.warn("User with id: {} is not owner for address with id: {}", userId, address.getId());
			throw new WebApplicationException(
					Response.status(Status.FORBIDDEN).entity(Messages.WARNING_ADDRESS_AUTHORIZATION_FAILED).build());
		}
	}

	public static void checkUserIsAssignedToAddress(int userId, Address address) {
		if (!isUserAssignedToAddress(userId, address)) {
			log.warn("User with id: {} is not assigned to address with id: {}", userId, address.getId());
			throw new WebApplicationException(
					Response.status(Status.FORBIDDEN).entity(Messages.WARNING_ADDRESS_AUTHORIZATION_FAILED).build());
		}
	}

	public static boolean isUserOwnerForAddress(int userId, Address address) {
		for (Person person : address.getOwners()) {
			if (person.getId() == userId) {
				return true;
			}
		}
		return false;
	}

	public static boolean isUserAssignedToAddress(int userId, Address address) {
		for (AddressPerson addrPerson : address.getAddressesPersons()) {
			if (addrPerson.getPerson().getId() == userId) {
				return true;
			}
		}
		return false;
	}

	public static void makeUserOwnerForAddress(Person user, Address address) {
		for (AddressPerson addrPerson : address.getAddressesPersons()) {
			if (addrPerson.getPerson().getId() == user.getId()) {
				addrPerson.setOwner(true);
				return;
			}
		}
		AddressPerson addrPers = new AddressPerson(address, user, true);
		address.getAddressesPersons().add(addrPers);
	}

	public static boolean removeUserOwnerFromAddress(Person user, Address address) {
		for (AddressPerson addrPerson : address.getAddressesPersons()) {
			if (addrPerson.getPerson().getId() == user.getId()) {
				addrPerson.setOwner(false);
				return true;
			}
		}
		return false;
	}

	public static void removeUserFromAddress(int userId, Address address) {
		Iterator<AddressPerson> iterator = address.getAddressesPersons().iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getPerson().getId() == userId) {
				log.debug("Found person to unassign from address");
				iterator.remove();
			}
		}
	}

	public static Address getAddressNotNull(Session session, int addressId) {
		Address address = (Address) session.get(Address.class, addressId);
		if (address == null) {
			log.warn("Address with id {} doesn't exist.", addressId);
			throw new WebApplicationException(
					Response.status(Status.NOT_FOUND).entity(Messages.WARNING_ADDRESS_DOES_NOT_EXIST).build());
		}
		return address;
	}
}
