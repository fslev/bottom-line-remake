package ro.bucharest.youth.bottom_line_mw.ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.HttpRequestPacket;
import org.glassfish.grizzly.websockets.DataFrame;
import org.glassfish.grizzly.websockets.ProtocolHandler;
import org.glassfish.grizzly.websockets.WebSocket;
import org.glassfish.grizzly.websockets.WebSocketApplication;
import org.glassfish.grizzly.websockets.WebSocketListener;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ro.bucharest.youth.bottom_line_mw.ws.msg.ClientRegistrationMsg;
import ro.bucharest.youth.bottom_line_mw.ws.msg.OnlineTenantsMsg;
import ro.bucharest.youth.bottom_line_mw.ws.msg.OnlineUsersMsg;
import ro.bucharest.youth.bottom_line_mw.ws.msg.SimpleMessage;
import ro.bucharest.youth.bottom_line_mw.ws.msg.TenantsRegistrationMsg;

public class ChatServerEndpoint extends WebSocketApplication {
	private static final Logger log = LogManager.getLogger(ChatServerEndpoint.class);

	private static Map<Integer, WebSocket> clients = new HashMap<>();
	private static Map<Integer, List<Integer>> tenants = new HashMap<>();

	@Override
	public void onConnect(WebSocket socket) {
		log.info("Connected websocket client");
	};

	@Override
	public void onMessage(WebSocket socket, String text) {

		log.debug("Got message: {}", text);
		String msgType = getMsgTypeFromJson(text);

		switch (msgType) {
		case "CLIENT_REGISTRATION":

			ClientRegistrationMsg clientReg = mapMessage(text, ClientRegistrationMsg.class);
			registerClient(clientReg, socket);
			OnlineUsersMsg onlineUsersMsg = new OnlineUsersMsg(clients.size());
			broadCastToAll(onlineUsersMsg);
			return;

		case "TENANTS_REGISTRATION":

			TenantsRegistrationMsg tenantsRegMsg = mapMessage(text, TenantsRegistrationMsg.class);
			registerTenants(tenantsRegMsg);
			OnlineTenantsMsg onlineTenantsMsg = new OnlineTenantsMsg();
			List<Integer> onlineTenants = getCommonOnlineTenants(tenantsRegMsg.getUserId());
			onlineTenantsMsg.setTenants(onlineTenants);
			broadCastToOnlineTenants(onlineTenants, onlineTenantsMsg);
			return;

		case "MESSAGE":
			SimpleMessage msg = mapMessage(text, SimpleMessage.class);
			socket.send(forwardMsg(msg) ? "OK" : "NOT_OK");
			return;
		}
		socket.send("I don't know what to do with your msg!");
	}

	private static String getMsgTypeFromJson(String msg) {
		return msg.split("(\"type\":)|,")[1].replaceAll("\"", "");
	}

	private static <T> T mapMessage(String msg, Class<T> classType) {
		try {
			return new ObjectMapper(new JsonFactory()).readValue(msg, classType);
		} catch (IOException e) {
			log.error(e);
			return null;
		}
	}

	@Override
	public void onClose(WebSocket socket, DataFrame frame) {
		log.info("Closed client websocket connection");
		Integer removedUser = removeClient(socket);
		super.onClose(socket, frame);
		OnlineUsersMsg onlineUsersMsg = new OnlineUsersMsg(clients.size());
		broadCastToAll(onlineUsersMsg);
		OnlineTenantsMsg onlineTenantsMsg = new OnlineTenantsMsg();
		List<Integer> onlineTenants = getCommonOnlineTenants(removedUser);
		onlineTenantsMsg.setTenants(onlineTenants);
		broadCastToOnlineTenants(onlineTenants, onlineTenantsMsg);
	}

	@Override
	public WebSocket createSocket(ProtocolHandler handler, HttpRequestPacket requestPacket,
			WebSocketListener... listeners) {
		return super.createSocket(handler, requestPacket, listeners);
	}

	private void broadCastToAll(Object msg) {
		log.debug("Broadcast message to all");
		for (Entry<Integer, WebSocket> entry : clients.entrySet()) {
			entry.getValue().send(toJsonString(msg));
		}
	}

	private void broadCastToOnlineTenants(List<Integer> tenants, OnlineTenantsMsg msg) {
		log.debug("Broadcast message to all online tenants");
		for (Integer user : tenants) {
			WebSocket userWs = clients.get(user);
			if (userWs != null) {
				userWs.send(toJsonString(msg));
			}
		}
	}

	private static List<Integer> getCommonOnlineTenants(Integer user) {
		List<Integer> onlineTenants = new ArrayList<>();
		List<Integer> allTenants = tenants.get(user);
		for (Entry<Integer, WebSocket> entry : clients.entrySet()) {
			Integer onlineUser = entry.getKey();
			if (allTenants.contains(onlineUser) && tenants.get(onlineUser).contains(user)) {
				onlineTenants.add(onlineUser);
			}
		}
		return onlineTenants;
	}

	private Integer removeClient(WebSocket socket) {
		Iterator<Entry<Integer, WebSocket>> iter = clients.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<Integer, WebSocket> entry = iter.next();
			if (entry.getValue().equals(socket)) {
				iter.remove();
				return entry.getKey();
			}
		}
		return null;
	}

	private void registerClient(ClientRegistrationMsg registration, WebSocket socket) {
		clients.put(registration.getId(), socket);
		log.debug("Tracking client {}", registration.getId());
	}

	private void registerTenants(TenantsRegistrationMsg msg) {
		tenants.put(msg.getUserId(), msg.getTenants());
		log.debug("Added tenants {} for client {}", msg.getTenants(), msg.getUserId());
	}

	private boolean forwardMsg(SimpleMessage msg) {
		WebSocket destUser = clients.get(msg.getDestUserId());
		if (destUser != null) {
			destUser.send(toJsonString(msg));
			log.debug("Sent msg from {} to {} ", msg.getSrcUserId(), msg.getDestUserId());
			return true;
		} else {
			log.warn("Client {} is not online", msg.getDestUserId());
		}
		return false;
	}

	private static String toJsonString(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			log.error("Cannot map to json string.");
			return null;
		}
	}
}