package ro.bucharest.youth.bottom_line_mw.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "address_invitation")
@XmlRootElement
public class AddressInvitation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, insertable = false)
	public int id;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "address_id")
	private Address address;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "src_person_id")
	private Person srcPerson;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "dest_person_id")
	private Person destPerson;

	@Column(name = "accepted")
	public boolean accepted;

	@Column(name = "active")
	public boolean active = true;

	public AddressInvitation() {

	}

	public AddressInvitation(Address address, Person srcPerson, Person destPerson) {
		this.address = address;
		this.srcPerson = srcPerson;
		this.destPerson = destPerson;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Person getSrcPerson() {
		return srcPerson;
	}

	public void setSrcPerson(Person srcPerson) {
		this.srcPerson = srcPerson;
	}

	public Person getDestPerson() {
		return destPerson;
	}

	public void setDestPerson(Person destPerson) {
		this.destPerson = destPerson;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
