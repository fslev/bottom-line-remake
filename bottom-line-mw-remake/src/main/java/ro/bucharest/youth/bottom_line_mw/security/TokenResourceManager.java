package ro.bucharest.youth.bottom_line_mw.security;

import javax.naming.AuthenticationException;
import javax.ws.rs.container.ContainerRequestContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.server.ContainerRequest;

import ro.bucharest.youth.bottom_line_mw.App;

public class TokenResourceManager {

	private static final Logger log = LogManager.getLogger(TokenResourceManager.class);

	private ContainerRequestContext context;

	public TokenResourceManager(ContainerRequestContext context) {
		this.context = context;
	}

	public Token extractToken() throws AuthenticationException {

		log.debug("Extract token for authorization");
		String authenticationTokenId = context.getHeaderString(ContainerRequest.AUTHORIZATION);

		if (authenticationTokenId == null) {
			log.debug("Authorization failed");
			throw new AuthenticationException("Authentication token is required");
		}

		Token token = App.tokenManager.getToken(authenticationTokenId);

		if (token == null) {
			log.debug("Authorization failed");
			throw new AuthenticationException("Token is invalid or expired");
		}
		return token;
	}

	public void removeToken() throws AuthenticationException {
		log.debug("Remove token");
		String authenticationTokenId = context.getHeaderString(ContainerRequest.AUTHORIZATION);

		if (authenticationTokenId == null) {
			log.debug("Authorization failed");
			throw new AuthenticationException("Authentication token is required");
		}

		Token token = App.tokenManager.getToken(authenticationTokenId);
		if (token == null) {
			log.debug("Authorization failed");
			throw new AuthenticationException("Token is invalid or expired");
		}

		App.tokenManager.removeToken(token);
	}
}
