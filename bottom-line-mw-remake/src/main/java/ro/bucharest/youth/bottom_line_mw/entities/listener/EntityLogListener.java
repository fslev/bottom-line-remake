package ro.bucharest.youth.bottom_line_mw.entities.listener;

import javax.persistence.PostPersist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EntityLogListener<T> {

	private static final Logger log = LogManager.getLogger(EntityLogListener.class);

	@PostPersist
	public void logPersist(T entity) {
		// maybe using it in future
	}
}
