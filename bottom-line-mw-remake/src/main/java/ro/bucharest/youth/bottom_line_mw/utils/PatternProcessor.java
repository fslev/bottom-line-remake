package ro.bucharest.youth.bottom_line_mw.utils;

import java.util.ArrayList;
import java.util.List;

public class PatternProcessor {

	private static String[] conjuctiveOperators = { " or ", " and " };
	private static String[] operators = { ">=", "<=", "!=", "=", ">", "<" };
	private List<Criteria> criteriaList = new ArrayList<Criteria>();

	public List<Criteria> process(String expression) throws InvalidStringCriteria {
		if (expression == null)
			return null;
		split(new Criteria(expression));
		return criteriaList;
	}

	// "type= COLD or type=HOT and room =Bucatarie and date > 142";
	private void split(Criteria criteria) throws InvalidStringCriteria {

		for (String conj : conjuctiveOperators) {
			String[] splinters = criteria.expression.split(conj);
			if (splinters.length > 1) {
				for (int i = 0; i < splinters.length; i++) {
					Criteria subCriteria = new Criteria(splinters[i]);
					if (i == 0 && criteria.conjuction != null) {
						subCriteria.conjuction = criteria.conjuction;
					}
					if (i > 0) {
						subCriteria.conjuction = conj.trim();
					}
					split(subCriteria);
				}
				return;
			}
		}

		for (String op : operators) {
			String[] splinters = criteria.expression.split(op);
			if (splinters.length == 2) {
				criteria.operator = op;
				criteria.key = splinters[0].trim();
				criteria.value = splinters[1].trim();
				// criteria.expression = "";
				criteriaList.add(criteria);
				return;
			} else if (splinters.length > 2) {
				throw new InvalidStringCriteria("Same operator found multiple times in the same key-value condition");
			}
		}
	}

	public static void main(String[] args) throws InvalidStringCriteria {
		String exp = "type>= COLD or type!=HOT and and date<= 432-432-1 and type=HOT and room =Bucatarie and date > 142 or a!=b";
		PatternProcessor pp = new PatternProcessor();
		System.out.println(pp.process(exp));

		List<Criteria> criteriaList = pp.process(exp);

		StringBuilder query = new StringBuilder(
				"from WaterConsumption wc where (:addressId is null or wc.address.id = :addressId) and ");
		for (Criteria criteria : criteriaList) {
			if (criteria.conjuction != null)
				query.append(criteria.conjuction).append(" ");
			query.append(criteria.key).append(" ");
			query.append(criteria.operator).append(" ");
			query.append(":").append(criteria.key).append(" ");
		}

		System.out.println(query);
	}

	public static class Criteria {
		String expression;
		public String key;
		public String value;
		public String operator;
		public String conjuction;

		public Criteria(String expression) {
			this.expression = expression;
		}

		@Override
		public String toString() {
			return "\nCriteria [\n key= " + key + ",\n value= " + value + ",\n operator= " + operator
					+ ",\n conjuction= " + conjuction + "\n]";
		}
	}

	public static class InvalidStringCriteria extends Exception {
		public InvalidStringCriteria(String message) {
			super(message);
		}
	}

}
