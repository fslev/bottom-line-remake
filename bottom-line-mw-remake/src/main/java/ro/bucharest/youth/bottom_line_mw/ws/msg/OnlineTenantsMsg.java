package ro.bucharest.youth.bottom_line_mw.ws.msg;

import java.util.List;

public class OnlineTenantsMsg {
	private Type type = Type.ONLINE_TENANTS;
	private List<Integer> tenants;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<Integer> getTenants() {
		return tenants;
	}

	public void setTenants(List<Integer> tenants) {
		this.tenants = tenants;
	}
}
