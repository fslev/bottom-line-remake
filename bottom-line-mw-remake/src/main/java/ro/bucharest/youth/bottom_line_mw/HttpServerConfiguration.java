package ro.bucharest.youth.bottom_line_mw;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainerProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class HttpServerConfiguration {

	public static HttpServer createHttpServer() {
		HttpServer server = new HttpServer();
		// Add network listener
		NetworkListener listener = new NetworkListener("default", AppProperties.getHttpHost(),
				AppProperties.getHttpPort());
		server.addListener(listener);
		// Add http container
		GrizzlyHttpContainer handler = new GrizzlyHttpContainerProvider().createContainer(GrizzlyHttpContainer.class,
				configureResources());
		server.getServerConfiguration().addHttpHandler(handler, "/api");
		// Add static http handler
		StaticHttpHandler webAppHandler = new StaticHttpHandler("src/main/webapp/");
		webAppHandler.setFileCacheEnabled(false);
		server.getServerConfiguration().addHttpHandler(webAppHandler);
		return server;
	}

	private static ResourceConfig configureResources() {
		ResourceConfig resConfig = new ResourceConfig()
				.packages("ro.bucharest.youth.bottom_line_mw.http.api",
						"ro.bucharest.youth.bottom_line_mw.http.api.filters")
				.register(RolesAllowedDynamicFeature.class);
		return resConfig;
	}
}
