package ro.bucharest.youth.bottom_line_mw.http.api.filters;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.ws.rs.NameBinding;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.message.internal.ReaderWriter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@NameBinding
@Retention(RetentionPolicy.RUNTIME)
@interface JsonValueRequestFilter {
}

@Provider
@JsonValueRequestFilter
public class ResourceJsonRequestFilter implements ContainerRequestFilter {

	protected byte[] entity;

	@Override
	public void filter(ContainerRequestContext context) {

		try {
			if (!context.hasEntity()) {
				abortWithResponseStatus(context, "Request has no entity !", Status.NOT_ACCEPTABLE);
				return;
			}
			this.entity = readBytes(context.getEntityStream());
			if (!isEntityValidJSON()) {
				abortWithResponseStatus(context, getJSONErrorMessage(), Status.BAD_REQUEST);
				return;
			}
		} catch (Exception e) {
			abortWithResponseStatus(context, stackTraceAsString(e), Status.INTERNAL_SERVER_ERROR);
			return;
		}
		// anew entity input stream after consuming it
		finally {
			if (this.entity != null)
				context.setEntityStream(new ByteArrayInputStream(this.entity));
		}
	}

	protected String getJSONErrorMessage() {
		return "Invalid JSON value!";
	}

	protected boolean isEntityValidJSON() {
		try {
			new ObjectMapper().readValue(this.entity, JsonNode.class);
		} catch (JsonParseException e) {
			return false;
		} catch (JsonMappingException e) {
			return false;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return true;
	}

	protected void abortWithResponseStatus(ContainerRequestContext context, Object message,
			Status status) {
		context.abortWith(Response.status(status).type(MediaType.TEXT_PLAIN_TYPE).entity(message)
				.build());
	}

	private static byte[] readBytes(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		if (in.available() > 0) {
			ReaderWriter.writeTo(in, out);
			return out.toByteArray();
		}
		return null;
	}

	private static String stackTraceAsString(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
}
