package ro.bucharest.youth.bottom_line_mw.ws.msg;

public class OnlineUsersMsg {
	private Type type = Type.ONLINE_USERS;
	private int count;

	public OnlineUsersMsg(int count) {
		this.count = count;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
