package ro.bucharest.youth.bottom_line_mw;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.AddressInvitation;
import ro.bucharest.youth.bottom_line_mw.entities.AddressPerson;
import ro.bucharest.youth.bottom_line_mw.entities.Debt;
import ro.bucharest.youth.bottom_line_mw.entities.EnergyConsumption;
import ro.bucharest.youth.bottom_line_mw.entities.HeatConsumption;
import ro.bucharest.youth.bottom_line_mw.entities.Person;
import ro.bucharest.youth.bottom_line_mw.entities.RoomType;
import ro.bucharest.youth.bottom_line_mw.entities.Utility;
import ro.bucharest.youth.bottom_line_mw.entities.UtilityDebtCalculus;
import ro.bucharest.youth.bottom_line_mw.entities.UtilityExpense;
import ro.bucharest.youth.bottom_line_mw.entities.UtilityExpensePayment;
import ro.bucharest.youth.bottom_line_mw.entities.WaterConsumption;
import ro.bucharest.youth.bottom_line_mw.entities.audit.UtilityDebtAudit;

public class HibernateConfiguration {

	public static SessionFactory buildSessionFactory() {
		Configuration config = configureHibernate();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(config.getProperties());
		return config.buildSessionFactory(builder.build());
	}

	private static Configuration configureHibernate() {
		Configuration cfg = new Configuration().addAnnotatedClass(RoomType.class).addAnnotatedClass(Address.class)
				.addAnnotatedClass(WaterConsumption.class).addAnnotatedClass(EnergyConsumption.class)
				.addAnnotatedClass(HeatConsumption.class).addAnnotatedClass(Utility.class)
				.addAnnotatedClass(UtilityExpense.class).addAnnotatedClass(Person.class).addAnnotatedClass(Debt.class)
				.addAnnotatedClass(UtilityDebtCalculus.class).addAnnotatedClass(UtilityExpensePayment.class)
				.addAnnotatedClass(UtilityDebtAudit.class).addAnnotatedClass(AddressPerson.class)
				.addAnnotatedClass(AddressInvitation.class);
		return cfg;
	}
}
