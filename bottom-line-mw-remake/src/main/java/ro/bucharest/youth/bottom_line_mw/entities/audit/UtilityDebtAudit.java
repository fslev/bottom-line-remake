package ro.bucharest.youth.bottom_line_mw.entities.audit;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Historize utilities payment debt
 * 
 * @author raf
 *
 */
@Entity(name = "utilities_debt_audit")
public class UtilityDebtAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, insertable = false)
	private int id;

	@Column(name = "amount", nullable = false)
	private double amount;

	@Column(name = "formula", length = 2048, nullable = false)
	private String formula;

	@Column(name = "comment", length = 300)
	private String comment;

	@Column(name = "debtor_name", length = 50, nullable = false)
	private String debtorName;

	@Column(name = "utility_name", length = 50, nullable = false)
	private String utilityName;

	@Temporal(TemporalType.DATE)
	@Column(name = "date", nullable = false)
	private Date date;

	public UtilityDebtAudit() {

	}

	public UtilityDebtAudit(String debtorName, String utilityName, String formula, Double amount) {
		this.debtorName = debtorName;
		this.utilityName = utilityName;
		this.formula = formula;
		this.amount = amount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDebtorName() {
		return debtorName;
	}

	public void setDebtorName(String debtorName) {
		this.debtorName = debtorName;
	}

	public String getUtilityName() {
		return utilityName;
	}

	public void setUtilityName(String utilityName) {
		this.utilityName = utilityName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}
}
