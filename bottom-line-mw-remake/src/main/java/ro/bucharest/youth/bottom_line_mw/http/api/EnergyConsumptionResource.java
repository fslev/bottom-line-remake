package ro.bucharest.youth.bottom_line_mw.http.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import ro.bucharest.youth.bottom_line_mw.App;
import ro.bucharest.youth.bottom_line_mw.entities.Address;
import ro.bucharest.youth.bottom_line_mw.entities.EnergyConsumption;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.security.LoggedUserResourceManager;
import ro.bucharest.youth.bottom_line_mw.utils.HibernateTransaction;
import ro.bucharest.youth.bottom_line_mw.utils.Messages;
import ro.bucharest.youth.bottom_line_mw.utils.PatternProcessor.InvalidStringCriteria;
import ro.bucharest.youth.bottom_line_mw.utils.QueryBuilder;

@Path("econsumption")
@AuthorizationRequestFilter
public class EnergyConsumptionResource {

	private static final Logger log = LogManager.getLogger(EnergyConsumptionResource.class);

	@Context
	SecurityContext secContext;

	@Path("/address/{addressId}")
	@POST
	@JsonObjectRequestFilter
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Adds a energy consumption instance inside db
	 */
	public Response addEnergyConsumption(final EnergyConsumption econsumption,
			@PathParam("addressId") final int addressId) {
		log.debug("Received request to add energy consumption: {}", econsumption);

		validateFields(econsumption);
		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// check address exists
				Address address = AddressResource.getAddressNotNull(session, addressId);
				AddressResource.checkUserIsAssignedToAddress(userId, address);

				// check energy consumption exists inside db
				EnergyConsumption result = (EnergyConsumption) session
						.createQuery("from EnergyConsumption ec where ec.date = :date and ec.address.id = :addressId")
						.setParameter("addressId", addressId).setParameter("date", econsumption.getDate())
						.uniqueResult();
				if (result != null) {
					log.warn("Energy consumption already exists inside database: {}", econsumption);
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity("Energy consumption already exists").build());
				}

				// If consumption is not defined then get previous index
				// measurement for same address and type and calculate it
				if (econsumption.getConsumption() == null) {
					EnergyConsumption prevResult = (EnergyConsumption) session
							.createQuery(
									"from EnergyConsumption ec where ec.address.id = :address_id and ec.date < :date order by ec.date desc")
							.setParameter("date", econsumption.getDate()).setParameter("address_id", addressId)
							.setMaxResults(1).uniqueResult();
					econsumption.setConsumption(
							prevResult != null ? econsumption.getIndex() - prevResult.getIndex() : null);
				}

				econsumption.setAddress(address);
				session.persist(econsumption);
			}
		}.execute();
		log.debug("Energy consumption entry persisted");

		return Response.status(Status.OK).entity("Energy consumption successfully persisted").build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEConsumptionList(@QueryParam(value = "addressId") final int addressId,
			@QueryParam(value = "offset") final Integer offset, @QueryParam(value = "limit") final Integer limit,
			@QueryParam(value = "pattern") final String pattern) {

		log.debug("Get all energy consumption for addressId {} and offset {} and limit {} and pattern {}", addressId,
				offset, limit, pattern);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		final QueryBuilder queryBuilder = new QueryBuilder(pattern);
		queryBuilder.acceptField("date", "date").acceptField("index", "index_val").acceptField("consumption",
				"consumption");

		List<EnergyConsumption> list = new HibernateTransaction(
				App.sessionFactory).new WithValue<List<EnergyConsumption>>() {
			@Override
			public List<EnergyConsumption> run(Session session) {

				Address address = AddressResource.getAddressNotNull(session, addressId);
				AddressResource.checkUserIsAssignedToAddress(userId, address);

				StringBuilder queryString = new StringBuilder("from EnergyConsumption ec where ec.address.id = ?");

				final String queryPattern;
				try {
					queryPattern = queryBuilder.build(queryString).toString();
				} catch (InvalidStringCriteria e) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
				}

				log.debug("RUNNING special query: {}", queryPattern);
				Query query = session.createQuery(queryPattern + " order by ec.date desc").setInteger(0, addressId);
				for (int i = 0; i < queryBuilder.getCriteriaList().size(); i++) {
					query.setString(i + 1, queryBuilder.getCriteriaList().get(i).value);
				}

				if (limit != null)
					query.setMaxResults(limit);
				if (offset != null)
					query.setFirstResult(offset);

				return query.list();
			}
		}.execute();

		// Get query count for pagination
		long count = new HibernateTransaction(App.sessionFactory).new WithValue<Long>() {
			@Override
			public Long run(Session session) {

				StringBuilder queryString = new StringBuilder(
						"select count(ec) from EnergyConsumption ec where ec.address.id = ?");

				final String queryPattern;
				try {
					queryPattern = queryBuilder.build(queryString).toString();
				} catch (InvalidStringCriteria e) {
					throw new WebApplicationException(
							Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build());
				}

				log.debug("RUNNING special query: {}", queryPattern);
				Query query = session.createQuery(queryPattern).setInteger(0, addressId);
				for (int i = 0; i < queryBuilder.getCriteriaList().size(); i++) {
					query.setString(i + 1, queryBuilder.getCriteriaList().get(i).value);
				}
				return (Long) query.uniqueResult();
			}
		}.execute();

		return Response.status(Status.OK).entity(list).header("count", count).build();
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	/**
	 * Removes EnergyConsumption
	 */
	public Response delete(@PathParam("id") final int eConsId) {
		log.debug("Remove energy consumption with id: {}", eConsId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				EnergyConsumption eCons = getEConsumptionNotNull(session, eConsId);
				AddressResource.checkUserIsAssignedToAddress(userId, eCons.getAddress());
				session.delete(eCons);
			}
		}.execute();

		return Response.status(Status.OK).entity("Energy consumption successfully removed").build();
	}

	@PUT
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@JsonObjectRequestFilter
	public Response update(@PathParam("id") final int eConsId, final EnergyConsumption eCons) {
		log.debug("Update energy consumption with id {} ", eConsId);

		final int userId = new LoggedUserResourceManager(secContext).getUserId();

		new HibernateTransaction(App.sessionFactory).new NoValue() {
			@Override
			public void run(Session session) {
				// get energy consumption from database
				EnergyConsumption srcECons = getEConsumptionNotNull(session, eConsId);
				AddressResource.checkUserIsAssignedToAddress(userId, srcECons.getAddress());
				// update energy consumption
				srcECons.setDate(eCons.getDate());
				srcECons.setIndex(eCons.getIndex());
				if (eCons.getConsumption() == null) {
					EnergyConsumption result = (EnergyConsumption) session
							.createQuery(
									"from EnergyConsumption ec where ec.address.id = :address_id and ec.date < :date order by ec.date desc")
							.setParameter("date", eCons.getDate())
							.setParameter("address_id", srcECons.getAddress().getId()).setMaxResults(1).uniqueResult();
					srcECons.setConsumption(result != null ? eCons.getIndex() - result.getIndex() : null);
				} else {
					srcECons.setConsumption(eCons.getConsumption());
				}
				srcECons.setDescription(eCons.getDescription());
				session.merge(srcECons);
			}
		}.execute();

		return Response.status(Status.OK).entity("Energy consumption successfully removed").build();
	}

	public static EnergyConsumption getEConsumptionNotNull(Session session, int eConsumptionId) {
		EnergyConsumption econsumption = (EnergyConsumption) session.get(EnergyConsumption.class, eConsumptionId);
		if (econsumption == null) {
			log.warn("Energy consumption with id {} doesn't exist.", econsumption);
			throw new WebApplicationException(
					Response.status(Status.NOT_FOUND).entity("Energy consumption does not exist").build());
		}
		return econsumption;
	}

	public static void validateFields(EnergyConsumption econsumption) {
		if (econsumption.getDate() == null || econsumption.getIndex() == null) {
			throw new WebApplicationException(
					Response.status(Status.BAD_REQUEST).entity(Messages.WARNING_INVALID_FIELDS).build());
		}
	}
}
