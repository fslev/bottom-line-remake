package ro.bucharest.youth.bottom_line_mw.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "energy_consumption")
public class EnergyConsumption extends AbstractUtilitiesConsumption {

	public EnergyConsumption() {
	};
}
