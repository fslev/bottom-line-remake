package ro.bucharest.youth.bottom_line_mw;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.websockets.WebSocketAddOn;
import org.glassfish.grizzly.websockets.WebSocketEngine;
import org.hibernate.SessionFactory;

import ro.bucharest.youth.bottom_line_mw.security.TokenManager;
import ro.bucharest.youth.bottom_line_mw.ws.ChatServerEndpoint;

public class App {

	static {
		System.setProperty("java.util.logging.manager", "org.apache.logging.log4j.jul.LogManager");
	}

	private static final Logger log = LogManager.getLogger(App.class);

	public static SessionFactory sessionFactory = HibernateConfiguration.buildSessionFactory();
	public static HttpServer httpServer = HttpServerConfiguration.createHttpServer();
	public static TokenManager tokenManager = new TokenManager();

	public static void main(String[] args) {
		enableWebSockets();
		startHttpServer();
		addShutdownHooks();
		log.info("--- Calories in Master Crumble Crunchy Coconut Clusters ---");
		sleepForever();
	}

	private static void startHttpServer() {
		try {
			httpServer.start();
		} catch (IOException e) {
			throw new RuntimeException("Can't start HTTP server", e);
		}
	}

	private static void enableWebSockets() {
		final WebSocketAddOn addon = new WebSocketAddOn();
		for (NetworkListener listener : httpServer.getListeners()) {
			listener.registerAddOn(addon);
		}
		WebSocketEngine.getEngine().register(AppProperties.getWsContext(), AppProperties.getWsPath(),
				new ChatServerEndpoint());
		log.info("Web socket was added");
	}

	private static void addShutdownHooks() {
		addHibernateShutdownHooks();
		addHttpServerShutdownHooks();
	}

	private static void addHibernateShutdownHooks() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (sessionFactory != null)
					sessionFactory.close();
				log.info("Closed hibernate session factory");
			}
		});
	}

	private static void addHttpServerShutdownHooks() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (httpServer != null)
					httpServer.shutdown();
				log.info("Closed HTTP server");
			}
		});
	}

	private static void sleepForever() {
		try {
			// nothing lasts forever
			Thread.sleep(Long.MAX_VALUE);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
}
