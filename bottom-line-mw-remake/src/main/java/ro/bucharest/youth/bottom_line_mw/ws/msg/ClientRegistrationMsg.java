package ro.bucharest.youth.bottom_line_mw.ws.msg;

public class ClientRegistrationMsg {
	private Type type;
	private int id;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
