package ro.bucharest.youth.bottom_line_mw.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordCypher {

	public static String encrypt(String text) {
		try {
			return new String(MessageDigest.getInstance("MD5").digest(text.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
}
