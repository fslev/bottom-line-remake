package ro.bucharest.youth.bottom_line_mw.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "address_person", uniqueConstraints = @UniqueConstraint(columnNames = { "address_id",
		"person_id" }))
public class AddressPerson {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, insertable = false)
	public int id;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "address_id")
	private Address address;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "person_id")
	private Person person;

	@Column(name = "is_owner", nullable = false)
	private boolean owner;

	public AddressPerson() {
	}

	public AddressPerson(Address address, Person person) {
		this.address = address;
		this.person = person;
	}

	public AddressPerson(Address address, Person person, boolean owner) {
		this.address = address;
		this.person = person;
		this.owner = owner;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean owner) {
		this.owner = owner;
	}

}
