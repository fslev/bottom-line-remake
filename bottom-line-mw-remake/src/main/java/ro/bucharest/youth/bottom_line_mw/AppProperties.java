package ro.bucharest.youth.bottom_line_mw;

import java.io.IOException;
import java.util.Properties;

public class AppProperties {

	private static Properties appProps;

	static {
		loadApplicationProperties();
	}

	private static void loadApplicationProperties() {
		appProps = new Properties();
		try {
			appProps.load(AppProperties.class.getClassLoader().getResourceAsStream("application.properties"));
		} catch (IOException e) {
			throw new RuntimeException("Can't load properties", e);
		}
	}

	public static String getHttpHost() {
		return appProps.getProperty("http.host").trim();
	}

	public static Integer getHttpPort() {
		return Integer.parseInt(appProps.getProperty("http.port"));
	}

	public static String getWsContext() {
		return appProps.getProperty("ws.context").trim();
	}

	public static String getWsPath() {
		return appProps.getProperty("ws.path").trim();
	}

	public static String getSmtpHost() {
		return appProps.getProperty("smtp.host").trim();
	}

	public static String getSmtpPort() {
		return appProps.getProperty("smtp.port").trim();
	}

	public static String getSmtpUser() {
		return appProps.getProperty("smtp.user").trim();
	}

	public static String getSmtpPwd() {
		return appProps.getProperty("smtp.pwd").trim();
	}
}