package ro.bucharest.youth.bottom_line_mw.http.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ro.bucharest.youth.bottom_line_mw.http.api.annotations.AuthorizationRequestFilter;
import ro.bucharest.youth.bottom_line_mw.http.api.annotations.JsonObjectRequestFilter;
import ro.bucharest.youth.bottom_line_mw.utils.MailSenderBuilder;

@Path("mail")
@AuthorizationRequestFilter
public class MailResource {

	private static final Logger log = LogManager.getLogger(MailResource.class);

	@POST
	@JsonObjectRequestFilter
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response sendEmail(final MailMessage mailMsg) {
		log.debug("Received request to send email");
		MailSenderBuilder builder = new MailSenderBuilder();
		for (String recip : mailMsg.getRecipients()) {
			builder.recipient(recip);
		}
		builder.from(mailMsg.getFrom()).subject(mailMsg.getSubject());
		if (builder.build().send(mailMsg.getMsg())) {
			return Response.status(Status.OK).entity("Message was succsessfully sent to recipients").build();
		} else {
			return Response.status(Status.BAD_REQUEST).entity("Cannot send email").build();
		}
	}
}
