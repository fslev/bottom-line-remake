package ro.bucharest.youth.bottom_line_mw.utils;

public class Messages {
	public static final String SUCCESS_PERSONS_ASSIGNED_TO_ADDRESS = "Persons successfully assigned to address";
	public static final String SUCCESS_ROOMS_ASSIGNED_TO_ADDRESS = "Rooms successfully assigned to address";
	public static final String SUCCESS_UTILITIES_ASSIGNED_TO_ADDRESS = "Utilities successfully assigned to address";
	public static final String SUCCESS_PERSONS_UNASSIGNED_FROM_ADDRESS = "Persons successfully unassigned from address";

	public static final String SUCCESS_ROOM_UPDATED = "Room successfully updated";
	public static final String SUCCESS_ADDRESS_UPDATED = "Address successfully updated";
	public static final String SUCCESS_PERSON_UPDATED = "Person successfully updated";
	public static final String SUCCESS_UTILITY_UPDATED = "Utility successfully updated";
	public static final String SUCCESS_WCONSUMPTION_UPDATED = "Water consumption successfully updated";

	public static final String SUCCESS_ROOM_REMOVED = "Room removed";
	public static final String SUCCESS_ADDRES_REMOVED = "Address removed";
	public static final String SUCCESS_PERSON_REMOVED = "Person removed";
	public static final String SUCCESS_UTILITY_REMOVED = "Utility removed";
	public static final String SUCCESS_WCONSUMPTION_REMOVED = "Water consumption removed";

	public static final String SUCCESS_ADDRESS_INVITATION_ACCEPTED = "Address invitation accepted";

	public static final String SUCCESS_WCONSUMPTION_PERSIST = "Water consumption entry successfully persisted";

	public static final String WARNING_UTILITY_DOES_NOT_EXIST = "Utility does not exist";
	public static final String WARNING_PERSON_DOES_NOT_EXIST = "Person does not exist";
	public static final String WARNING_ROOM_DOES_NOT_EXIST = "Room does not exist";
	public static final String WARNING_ADDRESS_DOES_NOT_EXIST = "Address does not exist";
	public static final String WARNING_WCONSUMPTION_DOES_NOT_EXIST = "Water consumption does not exist";
	public static final String WARNING_ADDRESS_INVITATION_DOES_NOT_EXIST = "Address invitation does not exist";

	public static final String WARNING_UTILITY_ALREADY_EXISTS = "Utility already exists!";
	public static final String WARNING_ROOM_ALREADY_EXISTS = "Room already exists!";
	public static final String WARNING_PERSON_ALREADY_EXISTS = "Person already exists!";
	public static final String WARNING_ADDRESS_ALREADY_EXISTS = "Address already exists!";
	public static final String WARNING_WCONSUMPTION_ALREADY_EXISTS = "Water consumption entry already exists!";

	public static final String WARNING_MISSING_NAME = "Name is missing";
	public static final String WARNING_MISSING_USERNAME = "Username is missing";
	public static final String WARNING_MISSING_PASSWORD = "Password is missing";
	public static final String WARNING_INVALID_EMAIL = "Email is missing or is invalid";

	public static final String WARNING_AUTHENTICATION_FAILED = "Authentication failed!";
	public static final String WARNING_USER_HAS_NO_RIGHTS = "Insufficient rights!";
	public static final String WARNING_PRINCIPAL_NOT_FOUND = "No principal found";
	public static final String WARNING_USER_ALREADY_OWNER_FOR_ADDRESS = "User is already owner for address";
	public static final String WARNING_INVALID_FIELDS = "Fields are not all set";

	public static final String WARNING_ADDRESS_AUTHORIZATION_FAILED = "User has no rights for address";
}
