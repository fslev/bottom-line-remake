


-----------------------------------------------
--------- How to build FE ------------
sudo apt-get install nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node

sudo apt-get install npm

sudo npm install gulp -g
sudo npm install bower -g

npm install && bower install
gulp



-----------------------------------------------
------------ Authentication -------------------

1.) Login
POST: http://192.168.0.102:8180/api/auth/login
Body:
{
    "username":"fslevoaca",
    "password":"md5(smth)"
    
}

returns a detailed json about authenticated user

2.) Get current user information
GET: http://localhost:8180/api/auth/user
HEADER: Authorization

returns a detailed json about current authenticated user



----------------- RESOURCES -------------------
More, you can find inside the package: ro.bucharest.youth.bottom_line_mw.http.api
-----------------------------------------------
------------ Addresses -------------------

1.) Get all addresses
GET http://localhost:8180/api/address
HEADER: Authorization = MTpmc2xldm9hY2E=

2.) Add address
POST: http://localhost:8180/api/address
{
"country":"Romania",
"city":"Bucuresti",
"Street":"Trestiana nr.9, Bloc 11, Ap.58"
}

3.) Add owner to address
PUT: http://localhost:8180/api/address/{addressId}/owner/{ownerId}

4.) Get owners for address
GET: http://192.168.0.102:8180/api/address/{addressId}/owners



-----------------------------------------------
------------ Rooms -------------------

1.) Add a room for an address:
POST: http://localhost:8180/api/room/address/{addressId}
BODY: {"name":"kitchen"}